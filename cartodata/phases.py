from enum import Enum

from cartodata.pipeline.clustering import get_executor_clus
from cartodata.pipeline.neighbors import get_executor_nbr
from cartodata.pipeline.projectionnd import get_executor_nD
from cartodata.pipeline.projection2d import get_executor_2D


class PhaseBase():

    @classmethod
    def name(cls):
        return f"{cls.INDEX}_{cls.SHORT}"

    @classmethod
    def long_name(cls):
        return f"{cls.INDEX}_{cls.NAME}"

    @classmethod
    def prefix(cls, param, extra=""):
        if isinstance(param, str):
            return f"{cls.name()}__{extra}{param}"
        elif isinstance(param, dict):
            params = {}
            for key in param.keys():
                params[f"{cls.name()}__{extra}{key}"] = param[key]
            return params

    @classmethod
    def remove_prefix(cls, name, extra=""):
        name = name.replace(f"{cls.name()}__{extra}", "")
        return name

    @classmethod
    def get_key(cls, phase_params):
        return phase_params.get("key", cls.DEFAULT)

    @classmethod
    def get_executor(cls, executor_params):
        phase_params = executor_params.get(cls.NAME, {})
        key_executor = cls.get_key(phase_params)
        executor = cls.EXECUTOR_FACTORY(key_executor)
        kwargs_nD = executor.filter_kwargs(cls, phase_params)
        return executor(**kwargs_nD)


class PhaseProjectionND(PhaseBase):

    INDEX = 2
    NAME = "projection_nD"
    SHORT = "nD"
    DEFAULT = "lsa"
    EXECUTOR_FACTORY = get_executor_nD


class PhaseProjection2D(PhaseBase):

    INDEX = 3
    NAME = "projection_2D"
    SHORT = "2D"
    DEFAULT = "umap"
    EXECUTOR_FACTORY = get_executor_2D


class PhaseClustering(PhaseBase):

    INDEX = 4
    NAME = "clustering"
    SHORT = "clus"
    DEFAULT = "hdbscan"
    EXECUTOR_FACTORY = get_executor_clus


class PhaseNeighboring(PhaseBase):
    INDEX = 5
    NAME = "neighboring"
    SHORT = "nbr"
    DEFAULT = "all"
    EXECUTOR_FACTORY = get_executor_nbr


class PhasePost(PhaseBase):
    INDEX = 6
    NAME = "post"
    SHORT = "pst"
    DEFAULT = ""


class Phases(Enum):

    Pre = None
    Entity = None
    ProjectionND = PhaseProjectionND
    Projection2D = PhaseProjection2D
    Clustering = PhaseClustering
    Neighboring = PhaseNeighboring
    Post = PhasePost
