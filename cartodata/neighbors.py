import logging
import os
from pathlib import Path
from gzip import GzipFile

import nmslib
import numpy as np

from cartodata.logs import timeit

logger = logging.getLogger(__name__)

NEIGHBORS_FILENAME_FORMAT = 'nearest_{}_for_{}.npy.gz'


def get_neighbors_path(neighbors_nature, nature, dump_dir):
    dump_dir = Path(dump_dir)
    return dump_dir / NEIGHBORS_FILENAME_FORMAT.format(
        neighbors_nature, nature
    )


@timeit
def get_neighbors_indirect(indirect_matrix, natural_space_matrix, matrices,
                           dump_dir=None, neighbors_nature=None,
                           natures=None, n_neighbors=10, best_among=100,
                           space="l2"):

    nms = nmslib.init(method='hnsw', space=space)
    nms.addDataPointBatch(indirect_matrix.transpose())
    nms.createIndex(print_progress=True)

    neighbors_list = []
    for idx, matrix in enumerate(matrices):
        indirect_neighbors = nms.knnQueryBatch(matrix.T, k=best_among,
                                               num_threads=4)
        neighbor_ids = get_best_indirect_neighbors(indirect_neighbors,
                                                   natural_space_matrix,
                                                   n_neighbors)

        if dump_dir is not None:
            with GzipFile(get_neighbors_path(neighbors_nature,
                                             natures[idx], dump_dir),
                          'w') as file:
                np.save(file, neighbor_ids)
        else:
            neighbors_list.append(neighbor_ids)

    return neighbors_list


@timeit
def get_neighbors(neighbors_matrix, neighbors_scores, matrices, power_score=0,
                  dump_dir=None, neighbors_nature=None,
                  natures=None, n_neighbors=10, best_among=100,
                  space="l2", num_threads=4):
    """
    Compute the nearest neighbors from the entities represented by the
    neighbors_matrix for all the matrices in the matrices array. For each
    matrix, if power_score at same index in the power_scores array is 0, the
    closest n_neighbors are computed. If power_score > 0, the best_among
    closest neighbors are computed and weighted by their score before
    only keeping the n_neighbors best.

    If dump_dir is provided, the neighbor matrices will be saved to disk in
    the dump_dir with filename as NEIGHBORS_FILENAME_FORMAT. This is
    recommended to limit memory usage.

    :param neighbors_matrix: the entities to compute get neighbors from
    :param neighbors_scores: the scores for the entities in neighbors_matrix
    :param matrices: all the matrices of the entities to get the neighbors for
    :param power_score: the power score to weight the distances
    :param dump_dir: the directory to store neighbors matrices
    :param natures: a list of natures of same length as matrices used to name
    the matrices when they are saved on disk
    :param neighbors_nature: the neighbors' nature used to name the matrices
    when they are saved on disk
    :param n_neighbors: the number of neighbors to keep
    for each entity
    :param best_among: the sample of neighbors to select
    the best neighbors from
    :param space: l2 or cosinesim. l2 better for umap,
    should be normalized for nD
    :return:
    """
    nms = nmslib.init(method='hnsw', space=space)
    nms.addDataPointBatch(neighbors_matrix.transpose())
    nms.createIndex(print_progress=True)

    if dump_dir is not None:
        nms.saveIndex(
            os.path.join(dump_dir, f"{neighbors_nature}.index"), save_data=True
        )

    logger.debug("Starting calculating neighbors.")
    neighbors_list = []
    for idx, matrix in enumerate(matrices):
        logger.debug(f"Calculating for {neighbors_nature}.")
        if power_score == 0:
            neighbors = nms.knnQueryBatch(matrix.T, k=n_neighbors,
                                          num_threads=num_threads)
            neighbor_ids = np.vstack([ids for ids, _ in neighbors]).T
        else:
            neighbors = nms.knnQueryBatch(matrix.T, k=best_among,
                                          num_threads=num_threads)
            neighbor_ids = get_best_neighbors_by_score(
                neighbors, neighbors_scores, n_neighbors, power_score
            )

        if dump_dir is not None:
            logger.debug(f"Calculating for {neighbors_nature}_{natures[idx]}")

            with GzipFile(get_neighbors_path(
                    neighbors_nature, natures[idx], dump_dir), 'w') as file:
                np.save(file, neighbor_ids)

                logger.debug(f"Wrote to {file}.")
        neighbors_list.append(neighbor_ids)

    logger.debug("Finished calculating neighbors.")

    return neighbors_list


def get_best_neighbors_by_score(neighbors, neighbors_scores, n_neighbors,
                                power_score=0.5):
    """
    Given a list of all nearest neighbors for all data points, select among
    those nearest neighbors the n_neighbors best based on
    (distance / (score ** power_score)).

    :param neighbors: a list of n_samples tuples (ids, distances) of nearest
    neighbors
    :param neighbors_scores: a pandas.Series of scores for all the possible
    neighbor ids
    :param n_neighbors: how many neighbors to select for each data point
    :param power_score: parameter to update the distances
    :return:
    """
    neighbor_ids = []
    for ids, distances in neighbors:
        scores = neighbors_scores.iloc[ids]
        scores = scores ** power_score
        ndistances = distances / scores
        sorted_ids = ids[np.argsort(ndistances)[:n_neighbors]]
        neighbor_ids.append(sorted_ids)

    return np.vstack(neighbor_ids).T


def get_best_indirect_neighbors(indirect_neighbors, natural_space_matrix,
                                n_neighbors):
    """
    Given a list of indirect neighbors with their distances, chooses the best
    direct neighbors that are associated with the indirect neighbors.

    :param indirect_neighbors:
    :param natural_space_matrix:
    :param n_neighbors:
    :return:
    """
    neighbor_ids = []
    for ids, distances in indirect_neighbors:
        neighbors = natural_space_matrix[ids]
        inv_dists = 0.9 - distances
        inv_dists = np.where(inv_dists > 0, inv_dists, 0)
        scores = inv_dists * neighbors
        sorted_ids = np.argsort(scores)[::-1][:n_neighbors]
        neighbor_ids.append(sorted_ids)

    return np.vstack(neighbor_ids).T


@timeit
def get_neighbors_from_index(matrix, neighbors_nature, dump_dir,
                             n_neighbors=10, space="l2"):
    """
    Load an NMSLib index from disk and use it to get the nearest neighbors of
    type neighbors_nature for the given points
    in matrix
    :param matrix:
    :param neighbors_nature:
    :param dump_dir:
    :param n_neighbors:
    :return:
    """
    index = nmslib.init(method='hnsw', space=space)
    index.loadIndex(
        os.path.join(dump_dir, f"{neighbors_nature}.index"), load_data=False
    )
    neighbors = index.knnQueryBatch(matrix, k=n_neighbors)
    return neighbors
