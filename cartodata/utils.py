from hashlib import blake2b
import numpy as np
import pandas as pd


def digest(data, digest_size=8):
    h = blake2b(digest_size=digest_size)
    h.update(data.encode())
    return h.hexdigest()


def hstack_transpose_matrices(matrices):
    """Horizontally stacks each matrix in the matrices list and gets transpose
    of the array. Returns also column count of each matrix in a list.

    Parameters
    ----------
    matrices: list of numpy.ndarray
        a list of arrays that have same row counts, but may differ in column
    counts

    Returns:
    numpy.ndarray, list of int

    transposed array and list of integers that contains row count for each
    matrix in the matrices to be used to decompose the transposed matrix
    """
    global_matrix = np.asarray(np.hstack(matrices).T)
    row_counts = [matrix.shape[1] for matrix in matrices]

    return global_matrix, row_counts


def decompose_transpose_matrix(global_matrix, row_counts):
    """ Decomposes the ``global_matrix`` according to the specifed
    ``row_counts`` and transposes the obtained matrix.

    Parameters
    ----------
    global_matrix: numpy.ndarray
        matrix that contains multiple arrays
    row_counts: list
        list of integers that contains the number of rows in each array in the
    ``global_matrix``

    Returns
    -------
    list of numpy.ndarray
    """
    start = 0
    matrices = []
    for row_count in row_counts:
        matrix = np.asarray(global_matrix[start:start + row_count].T)
        matrices.append(matrix)
        start += row_count

    return matrices


def make_relation(from_df, to_df):
    """Creates a mapping between ``from_df`` to ``to_df`` for common indices in
    both dataframes.

    For each dataframe a new dataframe is generated with the index of the
    dataframe and values as integers starting from 0 to number of elements
    in the dataframe. The mapping is created from the integer value of each
    row in ``from_df`` to the integer value of corresponding row in ``to_df``
    for common indexes in dataframes.

    Parameters
    ----------
    from_df: pandas.DataFrame
        the dataframe whose integer values are used as key for the mapping
    to_df: pandas.DataFrame
        the dataframe whose integer values are used as value for mapping

    Returns
    -------
    dict
        a mapping from ``from_df`` to ``to_df``
    """
    # create a new dataframe with index from from_df, and values as
    # integers starting from 0 to the length of from_df
    left = pd.DataFrame(data=np.arange(len(from_df)), index=from_df.index)

    # create a new dataframe with index from to_df, and values as integers
    # starting from 0 to the length of to_df
    right = pd.DataFrame(data=np.arange(len(to_df)), index=to_df.index)

    # merge left and right dataframes on the intersection of keys of both
    # dataframes preserving the order of left keys
    merge = left.merge(right, how="inner",
                       left_index=True, right_index=True)

    return dict(merge.values)


def get_sub_matrices(scores_s1, scores_s2, matrices, values=True):
    """
    Creates a dictionary of mapping of the common entities in  scores_s1 and
    scores_s2. The keys the dictionary are the indices of the entities from
    scores_s1 and the values are the indices of the entities from scores_s2.

    Using this dictionary, it takes the rows corresponding to the entities
    specified by the values in the dictionary from the matrices and returns the
    list of matrices.
    """
    matrices_sub = []
    for i in range(len(matrices)):
        relations_i = make_relation(scores_s1[i], scores_s2[i])
        if values:
            indices_i = list(relations_i.values())
        else:
            indices_i = list(relations_i.keys())
        matrix_i = matrices[i]
        matrix_s2_sub_s1 = np.take(matrix_i, indices_i, axis=1)
        matrices_sub.append(matrix_s2_sub_s1)

    return matrices_sub
