"""
Code for projecting text into high-dimensional vectors,
or high-dimensional vectors into lower-dimensional vectors.
"""
import logging
import os

import numpy as np
import scipy.sparse as scs
from gensim.matutils import Sparse2Corpus, corpus2dense
from gensim.models import LdaModel, doc2vec
from gensim import utils
from sklearn.utils.extmath import randomized_svd
from umap import UMAP

from cartodata.logs import timeit
from cartodata.operations import save_matrix, normalize_l2  # noqa

try:  # Try to load adapters and torch, not always installed with cartolabe
    from transformers import AutoTokenizer, AutoModel
    from adapters import AutoAdapterModel
    import torch
    from tqdm.autonotebook import tqdm
    HAS_TRANSFORMERS = True
except ImportError:  # adapters not installed
    HAS_TRANSFORMERS = False


logger = logging.getLogger(__name__)


# ------------------------------------------------------------
# LLMs code
#
# To add: https://huggingface.co/sentence-transformers/all-MiniLM-L6-v2


def torch_device():
    """
    Returns the best device for Torch,
    either 'cuda' if a GPU is available of 'cpu' otherwise.
    """
    if torch.cuda.is_available():
        return "cuda"
        # device_count = torch.cuda.device_count()
        # for i in range(device_count):
        #     device_name = torch.cuda.get_device_name(i)
        #     print(f"GPU {i}: {device_name}")
        # return "cuda"
    # print("CUDA is not available. Using CPU.")
    return "cpu"


def iterate_in_batches(lst, batch_size):
    "Iterate over the specified list with the given batch size."
    for i in tqdm(range(0, len(lst), batch_size), "Processing batches"):
        yield lst[i:i + batch_size]


BERT_PROJECTIONS = {
    "specter2": {
        "name": "specter2",
        "tokenizer": 'allenai/specter2_aug2023refresh_base',
        "pretrained": 'allenai/specter2_aug2023refresh_base',
        "adapter": 'allenai/specter2_aug2023refresh',
        "type": "bert"
    },
    "scincl": {
        "name": "scincl",
        "tokenizer": 'malteos/scincl',
        "pretrained": 'malteos/scincl',
        "type": "bert"
    },
    # See https://www.sbert.net/docs/pretrained_models.html
    "all-MiniLM-L6-v2": {
        "name": "all-MiniLM-L6-v2",
        "tokenizer": 'sentence-transformers/all-MiniLM-L6-v2',
        "pretrained": 'sentence-transformers/all-MiniLM-L6-v2',
        "type": "sbert"
    },
    "all-mpnet-base-v2": {
        "name": "all-mpnet-base-v2",
        "tokenizer": 'sentence-transformers/all-mpnet-base-v2',
        "pretrained": 'sentence-transformers/all-mpnet-base-v2',
        "type": "sbert"
    },
    "scibert": {
        "name": "scibert",
        "tokenizer": 'allenai/scibert_scivocab_uncased',
        "pretrained": 'allenai/scibert_scivocab_uncased',
        "type": "bert"
    }
}


@timeit
def bert_projection(matrices, vocab_df, family="specter2", normalize=True,
                    pt_device=None, batch_size=10, max_length=256):
    """
    Apply a Bert projection method, Specter2 by default.
    :param matrices: a list of matrices to project
    :param vocab_db: the list of text to index
    :param family: the Bert pretrained family, currently
      "specter2" (default), or "scincl".
    :param pt_device: the Torch device to use, or None to compute it
    :param normalize: True (default) if the returned matrices are normalized
    :returns: a list of matrices projected in the latent space.

    Also applies an l2 normalization to each feature of the projected matrices.
    """
    params = BERT_PROJECTIONS[family]
    if params.get("type") == "sbert":
        return sbert_projection(
            matrices, vocab_df,
            family=family,
            normalize=normalize,
            pt_device=pt_device
        )
    if pt_device is None:
        pt_device = torch_device()
    print("Using torch device:", pt_device)
    tokenizer = AutoTokenizer.from_pretrained(params["tokenizer"])
    #    'allenai/specter2_aug2023refresh_base'
    model = AutoAdapterModel.from_pretrained(params["pretrained"])
    #    'allenai/specter2_aug2023refresh_base'
    if "adapter" in params:
        model.load_adapter(
            params["adapter"],
            # 'allenai/specter2_aug2023refresh',
            source="hf",
            set_active=True
        )
    model.to(pt_device)
    model.eval()
    collect_embeddings = torch.Tensor([])
    i = 0
    for texts in iterate_in_batches(vocab_df, batch_size):
        # preprocess the input
        inputs = tokenizer(list(texts), padding=True, truncation=True,
                           return_tensors="pt", return_token_type_ids=False,
                           max_length=max_length).to(pt_device)
        i = i + 1
        with torch.no_grad():
            output = model(**inputs)
            # take the first token in the batch as the embedding
            embeddings = output.last_hidden_state[:, 0, :].to("cpu")
            collect_embeddings = torch.cat((collect_embeddings, embeddings),
                                           dim=0)

    # collect_embeddings.shape = (len(vocab_df), 768)
    # print(collect_embeddings.shape)
    doc_embeddings = np.asanyarray(collect_embeddings).T
    projected_matrices = []
    for matrix in matrices:
        try:
            proj = matrix_dot(np.transpose(doc_embeddings), matrix)
            projected_matrices.append(proj)
        except TypeError:
            projected_matrices.append(doc_embeddings)

    if normalize:
        projected_matrices = list(map(normalize_l2, projected_matrices))
    return projected_matrices


def mean_pooling(model_output, attention_mask):
    "Take the attention mask into account for correct averaging"
    # First element of model_output contains all token embeddings
    token_embeddings = model_output[0]
    input_mask_expanded = attention_mask\
        .unsqueeze(-1)\
        .expand(token_embeddings.size())\
        .float()
    sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
    sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
    return sum_embeddings / sum_mask


@timeit
def sbert_projection(matrices, vocab_df,
                     family="all-MiniLM-L6-v2",
                     normalize=True,
                     pt_device=None, batch_size=10):
    """
    Apply a Sentence Bert projection method, "all-MiniLM-L6-v2" by default.
    :param matrices: a list of matrices to project
    :param vocab_db: the list of text to index
    :param family: the Bert pretrained family, currently
      "all-MiniLM-L6-v2" (default).
    :param pt_device: the Torch device to use, or None to compute it
    :param normalize: True (default) if the returned matrices are normalized
    :returns: a list of matrices projected in the latent space.

    Also applies an l2 normalization to each feature of the projected matrices.
    """
    params = BERT_PROJECTIONS[family]
    if params.get("type") != "sbert":
        raise ValueError(f"Family of model {family} is not a Sentence Bert")
    if pt_device is None:
        pt_device = torch_device()
    tokenizer = AutoTokenizer.from_pretrained(params["tokenizer"])
    model = AutoModel.from_pretrained(params["pretrained"])
    if "adapter" in params:
        model.load_adapter(
            params["adapter"],
            source="hf",
            set_active=True
        )
    model.to(pt_device)
    model.eval()
    collect_embeddings = torch.Tensor([])
    i = 0
    for texts in iterate_in_batches(vocab_df, batch_size):
        # preprocess the input
        inputs = tokenizer(list(texts), padding=True, truncation=True,
                           return_tensors="pt", return_token_type_ids=False
                           ).to(pt_device)
        i = i + 1
        with torch.no_grad():
            output = model(**inputs)
            # take the first token in the batch as the embedding
            embeddings = mean_pooling(output,
                                      inputs['attention_mask'])
        collect_embeddings = torch.cat((collect_embeddings,
                                        embeddings.to("cpu")),
                                       dim=0)

    # collect_embeddings.shape = (len(vocab_df), 768)
    # print(collect_embeddings.shape)
    doc_embeddings = np.asanyarray(collect_embeddings).T
    projected_matrices = []
    for matrix in matrices:
        try:
            proj = matrix_dot(np.transpose(doc_embeddings), matrix)
            projected_matrices.append(proj)
        except TypeError:
            projected_matrices.append(doc_embeddings)

    if normalize:
        projected_matrices = list(map(normalize_l2, projected_matrices))
    return projected_matrices

# End LLMs
# --------------------------------------------------------------------------


def lsa_get_randomized_svd(nb_dims, canon_table, randomized_svd_n_iter=5):

    U, sigma, VT = randomized_svd(canon_table, n_components=nb_dims,
                                  n_iter=randomized_svd_n_iter,
                                  random_state=None)
    return U, sigma, VT


def matrix_dot(U, matrix, max_size=1000):
    if matrix.shape[1] < max_size:
        return np.asarray(np.transpose(U).dot(matrix.todense()))

    # matrix size is too big, making dot product separately on submatrices
    ns = int(matrix.shape[1] / max_size)
    totspace = None
    for i in range(0, ns + 1):
        start = i * max_size
        end = (i + 1) * max_size
        if i == ns:
            end = matrix.shape[1]

        ext = matrix[:, start:end].todense()
        sext = np.asarray(np.transpose(U).dot(ext))
        if i == 0:
            totspace = sext
        else:
            totspace = np.concatenate([totspace, sext], axis=1)

    return totspace


@timeit
def lsa_projection(nb_dims, canon_table, matrices, randomized_svd_n_iter=5,
                   max_size=1000, normalize=True, random_state=None,
                   dump_dir=None):

    U, sigma, VT = randomized_svd(canon_table, n_components=nb_dims,
                                  n_iter=randomized_svd_n_iter,
                                  random_state=random_state)

    if dump_dir is not None:
        if not os.path.exists(dump_dir):
            os.makedirs(dump_dir)
        save_matrix(VT, os.path.join(dump_dir, 'lsa_components'))

    projected_matrices = []
    for matrix in matrices:
        if type(matrix) == scs.dia_matrix:
            # assume identity matrix
            proj = np.asarray(np.transpose(U))
            projected_matrices.append(proj)
        else:
            proj = matrix_dot(U, matrix, max_size=max_size)
            projected_matrices.append(proj)

    if normalize:
        projected_matrices = list(map(normalize_l2, projected_matrices))

    return projected_matrices


@timeit
def lda_projection(nb_dims, canon_table_idx, matrices, update_every=0,
                   passes=20, normalize=True):

    corpus = Sparse2Corpus(
        sparse=matrices[canon_table_idx], documents_columns=False
    )
    model = LdaModel(
        corpus, num_topics=nb_dims, update_every=update_every, passes=passes
    )

    U = corpus2dense(model[corpus], nb_dims)

    projected_matrices = []
    for matrix in matrices:
        if type(matrix) == scs.dia_matrix:
            projected_matrices.append(U)
        else:
            proj = matrix_dot(np.transpose(U), matrix)
            projected_matrices.append(proj)

    if normalize:
        projected_matrices = list(map(normalize_l2, projected_matrices))

    return projected_matrices


@timeit
def doc2vec_projection(nb_dims, canon_table_idx, matrices, vocab_df, workers=3,
                       epochs=5, callbacks=(), batch_words=10000,
                       trim_rule=None, alpha=0.025, window=5, seed=1, hs=0,
                       negative=5, ns_exponent=0.75, cbow_mean=1,
                       min_alpha=0.0001, compute_loss=False, dm_mean=None,
                       dm=1, dbow_words=0, dm_concat=0, dm_tag_count=1,
                       normalize=True):
    """
    Apply the `doc2vec_projection` method. Takes at least four arguments:
    :param nb_dims: the number of dimensions wanted
    :param canon_table_idx: matrix index of documents/words frequency, unused
    :param matrices: a list of matrices to project
    :param vocab_db: the list of text to index
    :returns: a list of matrices projected in the latent space.

    Additionally, it is possible to add doc2vec parameters with the same
    syntax as the gensim doc2vec function.

    Also applies an l2 normalization to each feature of the projected matrices.
    """
    corpusvoc = [utils.simple_preprocess(row) for row in vocab_df]
    tagcorpusvoc = [
        doc2vec.TaggedDocument(row, [i]) for i, row in enumerate(corpusvoc)
    ]

    model = doc2vec.Doc2Vec(vector_size=nb_dims, workers=workers,
                            epochs=epochs, callbacks=callbacks,
                            batch_words=batch_words, trim_rule=trim_rule,
                            alpha=alpha, window=window, seed=seed, hs=hs,
                            negative=negative, ns_exponent=ns_exponent,
                            cbow_mean=cbow_mean, min_alpha=min_alpha,
                            compute_loss=compute_loss, dm_mean=dm_mean, dm=dm,
                            dbow_words=dbow_words, dm_concat=dm_concat,
                            dm_tag_count=dm_tag_count)
    model.build_vocab(tagcorpusvoc)
    model.train(tagcorpusvoc, total_examples=model.corpus_count,
                epochs=model.epochs)

    Ulist = [model.infer_vector(row) for row in corpusvoc]
    U = np.asanyarray(Ulist).T

    projected_matrices = []
    for matrix in matrices:
        try:
            proj = matrix_dot(np.transpose(U), matrix)
            projected_matrices.append(proj)
        except TypeError:
            projected_matrices.append(U)

    if normalize:
        projected_matrices = list(map(normalize_l2, projected_matrices))

    return projected_matrices


@timeit
def umap_projection(
        matrices,
        n_neighbors=15,
        min_dist=0.1,
        n_components=2,
        random_state=None,
        metric="euclidean",
        init="random"):
    """
    Apply the UMAP fit_transform algorithm on all the matrices, reducing the
    number of features of each matrix to n_components.
    This works only if the combined number of documents for all matrices
    is ~ < 2million. Otherwise, use the indirect_umap_projection method.

    :param matrices:
    :param n_neighbors:
    :param min_dist:
    :param n_components
    :return:
    """
    shapes = [matrix.shape for matrix in matrices]
    global_matrix = np.asarray(np.hstack(matrices).T)

    reducer = UMAP(
        n_neighbors=n_neighbors,
        min_dist=min_dist,
        n_components=n_components,
        init=init,
        random_state=random_state,
        metric=metric
    )
    proj_umap = reducer.fit_transform(global_matrix)

    min_coords = np.amin(proj_umap, axis=0)
    max_coords = np.amax(proj_umap, axis=0)
    logger.debug(
        f"UMAP projection done. Max coords: {max_coords}. "
        f"Min Coords: {min_coords}."
    )

    start = 0
    projected_matrices = []
    for shape in shapes:
        matrix = np.asarray(proj_umap[start:start + shape[1]].T)
        projected_matrices.append(matrix)
        start += shape[1]

    return projected_matrices


@timeit
def guided_umap_projection(projected_matrice, matrices, n_neighbors=15,
                           min_dist=0.1, max_size=2000000, model=None):
    """
    Apply the UMAP fit algorithm to the projected_matrice. Then transform
    all the matrices using the fitted model to extrapolate the vectors in 2
    dimension.

    :param projected_matrice:
    :param matrices:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    model = fit_transform_guided(projected_matrice, n_neighbors, min_dist,
                                 max_size, model=model)

    logger.debug(
        f"UMAP: Reference matrix projected to size {projected_matrice.shape}"
    )

    projected_matrices = []
    if len(matrices) > 0:
        for matrix in matrices:
            projected_matrix = umap_transform(model, matrix, max_size)
            projected_matrices.append(projected_matrix.T)
            logger.debug(
                f"UMAP: Matrix projected to size {projected_matrix.shape}"
            )

    # Check min and max coordinates
    stack = np.hstack(projected_matrices)
    min_coords = np.amin(stack, axis=1)
    max_coords = np.amax(stack, axis=1)
    logger.debug(
        f"UMAP projection done. Max coords: {max_coords}. "
        f"Min Coords: {min_coords}."
    )

    return projected_matrices


def fit_transform_guided(projected_matrice, n_neighbors, min_dist, max_size,
                         model=None):
    """
    Fit a UMAP model with a random sample of max_size documents from the
    matrix. Then use this model to transform the remaining documents,
    combining the results into a transformed matrix that is returned.

    :param matrix:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    # Create a random permutation

    # Fit model on random mask data
    logger.debug(
        "UMAP: Fitting model with sample matrix of size "
        f"{projected_matrice.shape}"
    )
    if model is None:
        model = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')

    model.fit(projected_matrice.T)

    # Model's embedding is equal to transformed data for the train data
    # Transform data for remaining documents
    return model


@timeit
def indirect_umap_projection(matrices, n_neighbors=15, min_dist=0.1,
                             max_size=2000000):
    """
    Apply the UMAP fit algorithm to the first matrix, with a limited random
    sample of max_size documents. Then transform all the matrices using the
    fitted model to extrapolate the vectors in 2 dimension.

    :param matrices:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    fit_matrix = matrices[0]
    if fit_matrix.shape[1] > max_size:
        model, model_matrix = fit_transform_big_matrix(fit_matrix, n_neighbors,
                                                       min_dist, max_size)
    else:
        logger.debug(
            f"UMAP: Fitting model with full matrix of size {fit_matrix.shape}"
        )
        model = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')
        model.fit(fit_matrix.T)

        model_matrix = model.embedding_

    logger.debug(
        f"UMAP: Reference matrix projected to size {model_matrix.shape}"
    )

    projected_matrices = [model_matrix.T]
    if len(matrices) > 1:
        for matrix in matrices[1:]:
            projected_matrix = umap_transform(model, matrix, max_size)
            projected_matrices.append(projected_matrix.T)
            logger.debug(
                f"UMAP: Matrix projected to size {projected_matrix.shape}"
            )

    # Check min and max coordinates
    stack = np.hstack(projected_matrices)
    min_coords = np.amin(stack, axis=1)
    max_coords = np.amax(stack, axis=1)
    logger.debug(f"UMAP projection done. Max coords: {max_coords}. "
                 f"Min Coords: {min_coords}.")

    return projected_matrices


def fit_transform_big_matrix(matrix, n_neighbors, min_dist, max_size):
    """
    Fit a UMAP model with a random sample of max_size documents from the
    matrix. Then use this model to transform the remaining documents,
    combining the results into a transformed matrix that is returned.

    :param matrix:
    :param n_neighbors:
    :param min_dist:
    :param max_size:
    :return:
    """
    # Create a random permutation
    perm = np.random.permutation(matrix.shape[1])
    fit_data = matrix[:, perm[:max_size]]

    # Fit model on random mask data
    logger.debug(
        f"UMAP: Fitting model with sample matrix of size {fit_data.shape}"
    )
    model = UMAP(n_neighbors=n_neighbors, min_dist=min_dist, init='random')
    model.fit(fit_data.T)

    # Model's embedding is equal to transformed data for the train data
    fit_transformed = model.embedding_
    # Transform data for remaining documents
    other_transformed = umap_transform(
        model, matrix[:, perm[max_size:]], max_size
    )

    # Merge arrays
    merged = np.vstack((fit_transformed, other_transformed))
    # Rearrange in correct order
    res = merged[invert_permutation(perm), :]
    return model, res


def umap_transform(model, matrix, max_size, max_tries=5):
    """
    Transform a matrix with a given UMAP model, either straight away if the
    matrix has less than max_size documents, or by splitting it in sub
    matrices if it is too big. UMAP does not work well with class imbalances.
    To avoid that, we randomly permute the matrix, to try to randomly
    distribute classes across all submatrices.

    :param max_tries:
    :param model:
    :param matrix:
    :param max_size:
    :return:
    """
    logger.debug(f"UMAP: Transforming matrix of size {matrix.shape}")
    if matrix.shape[1] > max_size:
        tries = 0
        while tries < max_tries:
            try:
                projected_matrix = split_transform(model, matrix, max_size)
                tries = max_tries
            except ValueError as e:
                logger.debug(
                    'UMAP: Transformation of submatrices failed due to invalid'
                    'permutation.'
                )
                tries += 1
                if tries < max_tries:
                    logger.debug('UMAP: Retrying transformation...')
                else:
                    raise e
    else:
        projected_matrix = model.transform(matrix.T)

    return projected_matrix


def split_transform(model, matrix, max_size):
    nb_seg = int(matrix.shape[1] / max_size) + 1
    seg_size = int(matrix.shape[1] / nb_seg)
    perm = np.random.permutation(matrix.shape[1])
    projected_matrix = None
    for i in range(nb_seg):
        start = i * seg_size
        end = matrix.shape[1] if i == nb_seg - 1 else start + seg_size

        logger.debug(
            f"UMAP: Transforming sub matrix with cols from {start} to {end}"
        )
        proj = model.transform(matrix[:, perm[start:end]].T)
        if i == 0:
            projected_matrix = proj
        else:
            projected_matrix = np.concatenate([projected_matrix, proj], axis=0)

    return projected_matrix[invert_permutation(perm), :]


def invert_permutation(p):
    """
    The argument p is assumed to be some permutation of 0, 1, ..., len(p)-1.
    Returns an array s, where s[i] gives the index of i in p.
    """
    s = np.empty(p.size, p.dtype)
    s[p] = np.arange(p.size)
    return s
