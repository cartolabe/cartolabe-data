import logging
import time

import falcon

from cartodata.api import settings
from cartodata.api.configurations.hal import HalConfiguration
from cartodata.api.resources import PositionsResource, NeighborsResource
from cartodata.api.stores import ModelStore

logger = logging.getLogger(__name__)


def create_api():
    store = ModelStore(settings.DUMP_DIR)
    confs = {
        'hal': HalConfiguration,
        'inria': HalConfiguration
    }
    return get_app(store, confs)


def get_app(store, confs):
    app = falcon.App()

    positions = PositionsResource(store, confs)
    neighbors = NeighborsResource(store, confs)
    app.add_route('/{key}/position', positions)
    app.add_route('/{key}/neighbors', neighbors)
    return app


class TimeLogMiddleware(object):
    def process_request(self, req, resp):
        """Process the request before routing it.
        """
        start = time.time()
        req.context.start_time = start

    def process_resource(self, req, resp, resource, params):
        """Process the request after routing.

        """

    def process_response(self, req, resp, resource, req_succeeded):
        """Post-processing of the response (after routing).
        """
        t = ''
        if hasattr(req.context, 'start_time'):
            end = time.time()
            start = req.context.start_time
            total = end - start
            if total < 1:
                t = '%2.2f ms' % (total * 1000)
            else:
                t = '%2.2f s' % total
        logger.info('"%s %s" %s %s' % (req.method, req.relative_uri,
                                       resp.status, t))
