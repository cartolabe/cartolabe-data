import importlib
import os

ENVIRONMENT_VARIABLE = 'CARTODATA_CONFIG'


class LazySettings:
    _wrapped = None

    def _setup(self, name=None):
        settings_module = os.environ.get(ENVIRONMENT_VARIABLE)
        if not settings_module:
            raise ModuleNotFoundError(
                "Could not intialize settings. You must define the "
                f"environment variable {ENVIRONMENT_VARIABLE}.")

        self._wrapped = Settings(settings_module)

    def __getattr__(self, name):
        """Return the value of a setting and cache it in self.__dict__."""
        if self._wrapped is None:
            self._setup(name)
        val = getattr(self._wrapped, name)
        self.__dict__[name] = val
        return val

    def __setattr__(self, name, value):
        """
        Set the value of setting. Clear all cached values if _wrapped changes
        (@override_settings does this) or clear single values when set.
        """
        if name == '_wrapped':
            self.__dict__.clear()
            self.__dict__['_wrapped'] = value
        else:
            self.__dict__.pop(name, None)
            if self._wrapped is None:
                self._setup()
            setattr(self._wrapped, name, value)

    @property
    def configured(self):
        """Return True if the settings have already been configured."""
        return self._wrapped is not None

    def __repr__(self):
        if self._wrapped is None:
            return '<LazySettings [Unevaluated]>'
        return '<LazySettings "%(settings_module)s">' % {
            'settings_module': self._wrapped.SETTINGS_MODULE,
        }


class Settings():
    def __init__(self, settings_module):
        self.SETTINGS_MODULE = settings_module
        mod = importlib.import_module(settings_module)
        for setting in dir(mod):
            if setting.isupper():
                setting_value = getattr(mod, setting)
                setattr(self, setting, setting_value)

    def __repr__(self):
        return '<%(cls)s "%(settings_module)s">' % {
            'cls': self.__class__.__name__,
            'settings_module': self.SETTINGS_MODULE,
        }


settings = LazySettings()
