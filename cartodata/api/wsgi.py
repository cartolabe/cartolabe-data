"""
WSGI config for cartodata project.

It exposes the WSGI callable as a module-level variable named ``application``.

"""

import os

from cartodata.api.api import create_api

os.environ.setdefault('CARTODATA_CONFIG', 'cartodata.api.settings')
application = create_api()
