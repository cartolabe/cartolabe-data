from cartodata.api.configurations.configurations import APIConfiguration
from cartodata.loading import transform_new_docs
import numpy as np

from cartodata.neighbors import get_best_neighbors_by_score


class HalConfiguration(APIConfiguration):

    def neighbors(self, vector, nature, n_neighbors=10):
        power_scores = {'authors': 0.5, 'words': 0.5}
        power_score = power_scores.get(nature, 0)
        index = self.store.get_model(self.dataset_key, nature, 'index')

        if power_score == 0:
            neighbors = get_neighbors_from_index(vector, index, n_neighbors)
            neighbor_ids = np.vstack([ids for ids, _ in neighbors]).T
        else:
            neighbors = get_neighbors_from_index(vector, index, 100)
            scores = self.store.get_model(self.dataset_key, nature, 'scores')
            neighbor_ids = get_best_neighbors_by_score(
                neighbors, scores, n_neighbors, power_score
            )
        return neighbor_ids.ravel().tolist()

    def project(self, query):
        # Tokenize
        words_score = self.store.get_model(self.dataset_key, 'words', 'scores')
        articles_umap = self.store.get_model(
            self.dataset_key, 'articles', 'umap')
        n = articles_umap.shape[1]
        token_vector = transform_new_docs(
            words_score, [query], 4, n_samples_init=n
        )
        row, _ = token_vector.nonzero()
        if len(row) < 1:
            return None, None

        # Project to latent space
        lsa_components = self.store.get_model(
            self.dataset_key, 'lsa', 'components'
        )
        latent = token_vector.todense().dot(lsa_components.T)

        # Project to 2D using neighbors

        articles_index = self.store.get_model(
            self.dataset_key, 'articles', 'index'
        )
        projection = project_to_2d_neighbors(
            latent, articles_umap, articles_index
        )

        return latent, projection


def project_to_2d_neighbors(latent, articles_umap, articles_index):
    """
    Project token vector to 2D using nearest neighbors barycentric projection
    :param latent:
    :param articles_umap:
    :param dump_dir:
    :return:
    """
    neighbors = get_neighbors_from_index(latent, articles_index, 5)
    barys = []
    for ids, distances in neighbors:
        weights = (1 - distances)
        weights = weights / weights.sum()
        bary = weights.dot(articles_umap[:, ids].T)
        barys.append(bary)
    return np.array(barys)


def get_neighbors_from_index(matrix, index, n_neighbors=10):
    return index.knnQueryBatch(matrix, k=n_neighbors)
