import gzip
import hashlib
import logging
import math
import os

import numpy as np
import pandas as pd
import nmslib

from cartodata.operations import load_scores

logger = logging.getLogger(__name__)


class ModelStore(object):
    """
    This is an In-Memory object store used to manage the models required by
    the API.
    """

    def __init__(self, dump_dir):
        self.dump_dir = dump_dir
        self.store = {}
        self.size = 0

    def get_model(self, dataset_key: str, nature: str, suffix: str):
        object_id = self._get_object_id(dataset_key, nature, suffix)
        if object_id in self.store:
            logger.info(
                '%r Retrieving %s_%s with id %s from model store.' % (
                    self, nature, suffix, object_id
                )
            )
            return self.store.get(object_id)

        if suffix == 'scores':
            value = self._get_score_vector(dataset_key, nature)
            size = value.nbytes
        elif suffix == 'index':
            value = self._get_nn_index(dataset_key, nature)
            size = os.path.getsize(
                os.path.join(self.dump_dir, dataset_key, nature + '.index')
            )
        else:
            value = self._get_matrix(dataset_key, nature, suffix)
            size = value.nbytes
        self.store[object_id] = value
        self.size += size
        logger.info(
            '%r %s_%s saved into model store with id %s.' % (
                self, nature, suffix, object_id
            )
        )
        return value

    def _get_matrix(self, dataset_key: str, nature: str, suffix: str):
        filename = os.path.join(self.dump_dir, dataset_key,
                                nature + '_' + suffix)
        with gzip.GzipFile(filename + '.npy.gz', 'r') as file:
            matrix = np.load(file)
        return matrix

    def _get_score_vector(self, dataset_key: str, nature: str):
        dump_dir = os.path.join(self.dump_dir, dataset_key)
        score = load_scores([nature], dump_dir)[0]
        return score

    def _get_nn_index(self, dataset_key: str, nature: str):
        dump_dir = os.path.join(self.dump_dir, dataset_key)
        index = nmslib.init(method='hnsw', space='cosinesimil')
        index.loadIndex(
            os.path.join(dump_dir, f"{nature}.index"), load_data=False
        )
        return index

    def _get_object_id(self, dataset_key: str, nature: str, suffix: str):
        # create md5 hash of 'dataset_key + nature + suffix'
        md5hash = hashlib.md5(
            str.encode(f"{dataset_key}_{nature}_{suffix}")
        ).hexdigest()
        return md5hash

    def __repr__(self):
        return '<ModelStore(%s)>' % convert_size(self.size)


def load_scores_without_index(nature, dump_dir):
    filename = os.path.join(dump_dir, nature + '_scores')
    with gzip.GzipFile(filename + '.npy.gz', 'r') as file:
        values = np.load(file)
    score = pd.Series(values)
    return score


def convert_size(size_bytes):
    if size_bytes == 0:
        return '0B'
    size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB')
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return '%s %s' % (s, size_name[i])
