import glob
import logging
import os

import pandas as pd

from cartodata.clustering import create_kmeans_clusters
from cartodata.neighbors import get_neighbors, get_neighbors_indirect
from cartodata.operations import (
    normalize_l2, dump_matrices, dump_scores, load_matrices_from_dumps
)
from cartodata.projection import (
    lsa_projection, umap_projection, lda_projection, doc2vec_projection,
    indirect_umap_projection
)

logger = logging.getLogger(__name__)


def do_projection(natures, matrices, words_mat, num_dims, dump_dir,
                  n_neighbors=15, min_dist=0.1, max_size=0, force=True):

    if all_matrices_exist(natures, 'lsa', dump_dir) and not force:
        logger.info(
            f"LSA matrices for {', '.join(natures)} already exist. Skipping "
            "LSA."
        )
        lsa_matrices = load_matrices_from_dumps(natures, 'lsa', dump_dir)
    else:
        logger.info('Starting LSA projection')
        lsa_matrices = lsa_projection(num_dims, words_mat, matrices, dump_dir)
        lsa_matrices = list(map(normalize_l2, lsa_matrices))
        dump_matrices(natures, lsa_matrices, 'lsa', dump_dir)

    if all_matrices_exist(natures, 'umap', dump_dir) and not force:
        logger.info(
            f"UMAP matrices for {', '.join(natures)} already exist. Skipping "
            "UMAP."
        )
        umap_matrices = load_matrices_from_dumps(natures, 'umap', dump_dir)
    else:
        logger.info('Starting UMAP projection')
        if max_size > 0:
            umap_matrices = indirect_umap_projection(
                lsa_matrices, n_neighbors, min_dist, max_size
            )
        else:
            umap_matrices = umap_projection(
                lsa_matrices, n_neighbors, min_dist
            )
        dump_matrices(natures, umap_matrices, 'umap', dump_dir)

    return lsa_matrices, umap_matrices


def do_lda_projection(natures, matrices, words_tab_idx, num_dims, dump_dir):

    logger.info('Starting LDA projection')
    projected_matrices = lda_projection(
        num_dims, words_tab_idx, matrices, update_every=1, passes=1
    )
    normalized_matrices = list(map(normalize_l2, projected_matrices))

    dump_matrices(natures, normalized_matrices, 'lda', dump_dir)

    logger.info('Starting UMAP projection')
    umap_matrices = umap_projection(normalized_matrices)

    dump_matrices(natures, umap_matrices, 'umap', dump_dir)

    return normalized_matrices, umap_matrices


def do_doc2vec_projection(natures, matrices, words_tab_idx, textdf, num_dims,
                          dump_dir):

    logger.info('Starting LDA projection')
    projected_matrices = doc2vec_projection(
        num_dims, words_tab_idx, matrices, textdf
    )
    normalized_matrices = list(map(normalize_l2, projected_matrices))

    dump_matrices(natures, normalized_matrices, 'lda', dump_dir)

    logger.info('Starting UMAP projection')
    umap_matrices = umap_projection(normalized_matrices)

    dump_matrices(natures, umap_matrices, 'umap', dump_dir)

    return normalized_matrices, umap_matrices


def get_all_neighbors(matrices, scores, power_scores, dump_dir, natures,
                      n_neighbors=10):

    logger.info('Starting neighbors computing.')
    for idx in range(len(matrices)):
        get_neighbors(matrices[idx], scores[idx], matrices,
                      power_score=power_scores[idx], dump_dir=dump_dir,
                      neighbors_nature=natures[idx], natures=natures,
                      n_neighbors=n_neighbors)


def get_all_neighbors_indirect(lsa_matrices, scores, matrices, indirects,
                               dump_dir, natures):
    logger.info('Starting neighbors computing.')
    for idx in range(len(lsa_matrices)):
        if indirects[idx]:
            get_neighbors_indirect(lsa_matrices[0], matrices[idx],
                                   lsa_matrices, dump_dir, natures[idx],
                                   natures)
        else:
            get_neighbors(lsa_matrices[idx], scores[idx], lsa_matrices,
                          dump_dir=dump_dir, neighbors_nature=natures[idx],
                          natures=natures)


def create_clusters(natures, articles_umap, words_umap, words_tab,
                    words_scores, words_lsa, dump_dir, base_factor=3):

    cluster_labels = []
    clus_lsa = []
    clus_umap = []
    clus_scores = []

    for idx, nature in enumerate(natures):
        n_clusters = 8 * (base_factor ** idx)
        logger.info(
            f"Starting clustering for {nature}. Nb clusters = {n_clusters}."
        )

        c_lsa, c_umap, c_scores, c_knn = create_kmeans_clusters(
            n_clusters, articles_umap, words_umap, words_tab, words_scores,
            cluster_labels, words_lsa
        )

        clus_lsa.append(c_lsa)
        clus_umap.append(c_umap)
        clus_scores.append(c_scores)

    dump_scores(natures, clus_scores, dump_dir)
    dump_matrices(natures, clus_lsa, 'lsa', dump_dir)
    dump_matrices(natures, clus_umap, 'umap', dump_dir)

    return clus_lsa, clus_umap, clus_scores


def all_matrices_exist(natures, suffix, dirname):
    all = True
    for nature in natures:
        if not glob.glob(os.path.join(dirname, nature + '_' + suffix + '.*')):
            all = False

    return all


def get_data_from_csv_dump(filename, index_col=0):
    df = pd.read_csv(filename, index_col=index_col)
    return df
