import logging

import pandas as pd
from sklearn.feature_extraction import text as sktxt

from cartodata.exporting import Exporter
from cartodata.loading import (
    load_text_column, load_identity_column, load_comma_separated_column
)
from cartodata.operations import (
    normalize_tfidf, filter_min_score, load_matrices_from_dumps, load_scores,
    normalize_l2
)
from cartodata.projection import lsa_projection, indirect_umap_projection
from cartodata.workflows.common import (
    dump_scores, create_clusters, get_all_neighbors, dump_matrices,
    all_matrices_exist
)

logger = logging.getLogger(__name__)


def software_workflow(dump_dir):
    df = pd.read_csv('datas/software_clean.csv')
    df['description'] = df['description'].str.replace('\\n', ' ', regex=True)

    do_workflow(df, 25, 0.1, 200000, 3000000, 300, dump_dir)


def do_workflow(df, min_df, max_df, max_words, vocab_sample, num_dims,
                dump_dir):
    natures = ['softwares', 'tags', 'words', 'hl_clusters', 'ml_clusters',
               'll_clusters', 'vll_clusters']

    load_matrices(natures[:3], df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, 50, force=False)

    matrices = load_matrices_from_dumps(natures[:3], 'mat', dump_dir)
    do_projection(natures[:3], matrices, num_dims, dump_dir, force=False)

    umap_matrices = load_matrices_from_dumps(natures[:3], 'umap', dump_dir)
    lsa_matrices = load_matrices_from_dumps(natures[:3], 'lsa', dump_dir)
    scores = load_scores(natures[:3], dump_dir)

    clus_lsa, clus_pos, clus_scores = create_clusters(
        natures[3:], umap_matrices[0], umap_matrices[2], matrices[2],
        scores[2], lsa_matrices[2], dump_dir, base_factor=3
    )
    umap_matrices.extend(clus_pos)
    scores.extend(clus_scores)

    power_scores = [0, 0, 0]
    get_all_neighbors(
        lsa_matrices, scores, power_scores, dump_dir, natures[:3]
    )

    export(dump_dir, df, natures, natures[:3])


def load_matrices(natures, df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, filt_min_score, force=False):

    if all_matrices_exist(natures, 'mat', dump_dir) and not force:
        logger.info(
            f"Matrices for {', '.join(natures)} already exist. Skipping "
            "creation."
        )
        return

    logger.info('Starting data loading')
    softwares_mat, softwares_scores = load_identity_column(df, 'name')
    tags_mat, tags_scores = load_comma_separated_column(df, 'tags')

    logger.info('Starting words loading')
    with open('datas/stopwords.txt', 'r') as stop_file:
        stopwords = sktxt.ENGLISH_STOP_WORDS.union(
            set(stop_file.read().splitlines())
        )

    df['text'] = df['name'].astype(str) + '.' + \
        df['description'].astype(str) + '.' + df['tags'].fillna('')
    words_mat, words_scores = load_text_column(
        df['text'], 4, min_df, max_df, vocab_sample, max_words,
        stopwords=stopwords
    )
    words_mat = normalize_tfidf(words_mat)

    matrices = [softwares_mat, tags_mat, words_mat]
    scores = [softwares_scores, tags_scores, words_scores]

    # filter tags min score = 50
    filt_tags, filt_tag_scores = filter_min_score(
        matrices[1], scores[1], filt_min_score
    )
    matrices[1] = filt_tags
    scores[1] = filt_tag_scores

    dump_scores(natures, scores, dump_dir)
    dump_matrices(natures, matrices, 'mat', dump_dir)


def do_projection(natures, matrices, num_dims, dump_dir, force=False):
    if all_matrices_exist(natures, 'lsa', dump_dir) and not force:
        logger.info(
            f"LSA matrices for {', '.join(natures)} already exist. Skipping "
            "LSA."
        )
        lsa_matrices = load_matrices_from_dumps(natures, 'lsa', dump_dir)
    else:
        logger.info('Starting LSA projection')
        lsa_matrices = lsa_projection(num_dims, matrices[2], matrices)
        lsa_matrices = list(map(normalize_l2, lsa_matrices))
        dump_matrices(natures, lsa_matrices, 'lsa', dump_dir)

    if all_matrices_exist(natures, 'umap', dump_dir) and not force:
        logger.info(
            f"UMAP matrices for {', '.join(natures)} already exist. Skipping "
            "UMAP."
        )
    else:
        logger.info('Starting UMAP projection')
        umap_matrices = indirect_umap_projection(
            lsa_matrices, n_neighbors=5, min_dist=0.1, max_size=1500000
        )
        dump_matrices(natures, umap_matrices, 'umap', dump_dir)


def export(dump_dir, df, natures, neighbor_natures):
    exporter = Exporter(dump_dir, natures, neighbor_natures)
    exporter.add_metadata_values('softwares', df[['url']])
    exporter.export_to_feather()
