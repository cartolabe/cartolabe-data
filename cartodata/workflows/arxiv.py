import logging

import pandas as pd
from sklearn.feature_extraction import text as sktxt

from cartodata.exporting import Exporter
from cartodata.loading import (
    load_identity_column, load_comma_separated_column, load_text_column
)
from cartodata.operations import (
    normalize_tfidf, filter_min_score, dump_scores, dump_matrices,
    load_matrices_from_dumps, normalize_l2, load_scores
)
from cartodata.projection import lsa_projection, umap_projection
from cartodata.workflows.common import (
    all_matrices_exist, create_clusters, get_all_neighbors
)

logger = logging.getLogger(__name__)


def arxiv_workflow(dump_dir):
    df = load_df()
    do_workflow(df, 25, 0.1, 100000, 350000, 300, dump_dir, 4, 10)
    # load_workflow(dump_dir)


def load_df():
    logger.info('Reading csv...')
    df = pd.read_csv('datas/arXiv2-30-11-2019.csv.bz2')
    # df = df.sample(n=50000)
    return df


def load_workflow(dump_dir, df):
    natures = ['articles', 'authors', 'words', 'subjects', 'hl_clusters',
               'ml_clusters', 'll_clusters', 'vll_clusters']

    export(dump_dir, df, natures, natures[:4])


def do_workflow(df, min_df, max_df, max_words, vocab_sample, num_dims,
                dump_dir, filt_min_score=4, n_neighbors=10):

    natures = ['articles', 'authors', 'words', 'subjects', 'hl_clusters',
               'ml_clusters', 'll_clusters', 'vll_clusters']

    load_matrices(natures[:4], df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, filt_min_score, force=False)

    matrices = load_matrices_from_dumps(natures[:4], 'mat', dump_dir)
    do_projection(natures[:4], matrices, num_dims, dump_dir, force=False)

    umap_matrices = load_matrices_from_dumps(natures[:4], 'umap', dump_dir)
    lsa_matrices = load_matrices_from_dumps(natures[:4], 'lsa', dump_dir)
    scores = load_scores(natures[:4], dump_dir)

    clus_lsa, clus_pos, clus_scores = create_clusters(
        natures[4:], umap_matrices[0], umap_matrices[2], matrices[2],
        scores[2], lsa_matrices[2], dump_dir, base_factor=3
    )
    umap_matrices.extend(clus_pos)
    scores.extend(clus_scores)

    power_scores = [0, 0.5, 0.5, 0]
    get_all_neighbors(
        lsa_matrices, scores, power_scores, dump_dir, natures[:4], n_neighbors
    )

    export(dump_dir, df, natures, natures[:4])


def load_matrices(natures, df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, filt_min_score, force=False):

    if all_matrices_exist(natures, 'mat', dump_dir) and not force:
        logger.info(
            f"Matrices for {', '.join(natures)} already exist. Skipping "
            "creation."
        )
        return

    logger.info('Starting data loading')
    # Create term-document matrices for the entites we need
    articles_tab, articles_scores = load_identity_column(df, 'title')
    authors_tab, authors_scores = load_comma_separated_column(df, 'authors',
                                                              comma=';')
    subjects_tab, subjects_scores = load_comma_separated_column(df, 'subject',
                                                                comma=';')

    logger.info('Starting words loading')
    with open('datas/stopwords.txt', 'r') as stop_file:
        stopwords = sktxt.ENGLISH_STOP_WORDS.union(
            set(stop_file.read().splitlines())
        )

    df['text'] = df['abstract'] + '.' + df['subject'].astype(str)
    words_tab, words_scores = load_text_column(df['text'], 4, min_df, max_df,
                                               vocab_sample, max_words,
                                               stopwords=stopwords)

    # Normalise words_tab
    words_tab = normalize_tfidf(words_tab)

    matrices = [articles_tab, authors_tab, words_tab, subjects_tab]
    scores = [articles_scores, authors_scores, words_scores, subjects_scores]

    # filter authors with min score = 4
    filt_authors, filt_auth_scores = filter_min_score(matrices[1], scores[1],
                                                      filt_min_score)
    matrices[1] = filt_authors
    scores[1] = filt_auth_scores

    filt_subjs, filt_subj_scores = filter_min_score(matrices[3], scores[3],
                                                    filt_min_score)
    matrices[3] = filt_subjs
    scores[3] = filt_subj_scores

    dump_scores(natures, scores, dump_dir)
    dump_matrices(natures, matrices, 'mat', dump_dir)


def do_projection(natures, matrices, num_dims, dump_dir, force=False):
    if all_matrices_exist(natures, 'lsa', dump_dir) and not force:
        logger.info(
            f"LSA matrices for {', '.join(natures)} already exist. Skipping "
            "LSA."
        )
        lsa_matrices = load_matrices_from_dumps(natures, 'lsa', dump_dir)
    else:
        logger.info('Starting LSA projection')
        lsa_matrices = lsa_projection(num_dims, matrices[2], matrices)
        lsa_matrices = list(map(normalize_l2, lsa_matrices))
        dump_matrices(natures, lsa_matrices, 'lsa', dump_dir)

    if all_matrices_exist(natures, 'umap', dump_dir) and not force:
        logger.info(
            f"UMAP matrices for {', '.join(natures)} already exist. Skipping "
            "UMAP."
        )
    else:
        logger.info('Starting UMAP projection')
        umap_matrices = umap_projection(lsa_matrices, n_neighbors=15,
                                        min_dist=0.1)
        dump_matrices(natures, umap_matrices, 'umap', dump_dir)


def export(dump_dir, df, natures, neighbor_natures):
    df['year'] = df['date'].astype(str).str[0:4].fillna('')
    df['url'] = df['id'].fillna('')

    exporter = Exporter(dump_dir, natures, neighbor_natures)
    exporter.add_reference('articles', 'subjects')
    exporter.add_reference('authors', 'subjects')

    exporter.add_metadata_values('articles', df[['year', 'url']])
    exporter.export_to_feather()
