import pandas as pd


class ExportNature():

    yaml_tag = u'!ExportNature'

    def __init__(self, key, refs=None, merge_metadata=None, add_metadata=None):
        self.key = key
        self.refs = refs
        self.merge_metadata = merge_metadata
        self.add_metadata = add_metadata

    def update_exporter(self, pipeline):
        if self.refs is not None:
            for ref in self.refs:
                pipeline.exporter.add_reference(self.key, ref)

        if self.merge_metadata is not None:
            for item in self.merge_metadata:
                pipeline.exporter.merge_metadata_values(
                    self.key, item["columns"], item["as_column"]
                )

        if self.add_metadata is not None:
            self.add_metadata_values(pipeline)

    def add_metadata_values(self, pipeline):
        dfs = []
        for item in self.add_metadata:
            dfs.append(item.process(pipeline))

        pipeline.exporter.add_metadata_values(self.key, pd.concat(dfs, axis=1))


class MetadataColumn():

    yaml_tag = u'!MetadataColumn'

    def __init__(self, column, as_column, func=None):
        self.column = column
        self.as_column = as_column
        self.func = {lambda x: eval(func)} if func is not None else None

    def process(self, pipeline):
        df = pipeline.data

        temp = df[self.column]

        if self.func is not None:
            temp = temp.apply(self.func)

        temp.columns = [self.as_column]

        return pd.DataFrame(temp)


class EntityMetadataMapColumn(MetadataColumn):

    yaml_tag = u'!EntityMetadataMapColumn'

    def __init__(self, entity, column, as_column):
        self.entity = entity

        super().__init__(column, as_column, func=None)

    def process(self, pipeline):
        df = pipeline.data

        matrix_dir = pipeline.working_dir
        matrix = pipeline.dataset.load_matrices([self.entity], matrix_dir)[0]
        rows, cols = matrix.T.nonzero()
        col = 0
        row = 0
        new_column = []
        for idx in range(len(rows)):
            if rows[idx] == row:
                continue
            new_column.append(','.join(str(y) for y in set(
                df[self.column].iloc[cols[col:idx]].values)))
            row = rows[idx]
            col = idx
        new_column.append(','.join(str(y) for y in set(
            df[self.column].iloc[cols[col:-1]].values)))

        return pd.DataFrame(new_column, columns=[self.as_column])
