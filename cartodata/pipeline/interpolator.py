import numpy as np
from sklearn.neighbors import KNeighborsRegressor

from cartodata.pipeline.base import BaseProjection
from cartodata.utils import (
    hstack_transpose_matrices, decompose_transpose_matrix
)


class KNeighborsInterpolator(BaseProjection):

    def __init__(self, key="kneighbors_int", metric="euclidean",
                 n_neighbors=2):
        super().__init__(key)

        self.metric = metric
        self.n_neighbors = n_neighbors

        self.regressor = KNeighborsRegressor(metric=metric, n_neighbors=2)

        self._add_to_params(["metric", "n_neighbors"])

    def fit(self, X_train, y_train):
        X_train_T, row_counts = hstack_transpose_matrices(X_train)
        y_train_T = np.hstack(y_train).T

        self.regressor.fit(X_train_T, y_train_T)

    def predict(self, X):
        X_T, row_counts = hstack_transpose_matrices(X)
        embedding = self.regressor.predict(X_T)

        matrices_2D = decompose_transpose_matrix(embedding, row_counts)

        return matrices_2D
