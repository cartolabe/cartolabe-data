import logging
from pathlib import Path

from cartodata.model_selection.experiment import BaseExperiment
from cartodata.pipeline.clustering import get_clustering
from cartodata.pipeline.columns import Columns
from cartodata.pipeline.common import get_pipeline
from cartodata.pipeline.loader import load_pipeline
from cartodata.pipeline.neighbors import get_neighboring
from cartodata.phases import (
    PhaseProjectionND, PhaseProjection2D, PhaseClustering
)
from cartodata.pipeline.projection2d import get_projection_2d
from cartodata.pipeline.projectionnd import get_projection_nd

logger = logging.getLogger(__name__)

PARAM_LIST_DATASET = [
    "filename",
    "fileurl",
    "version",
    "prev_version"
]


class PipelineExperiment(BaseExperiment):

    def __init__(self, dataset_name, dataset_version, conf_dir, input_dir,
                 nature, source, top_dir, selector, score_list=None,
                 **score_params):
        super().__init__(nature, source, top_dir, selector, score_list=None,
                         **score_params)
        self.dataset_name = dataset_name
        self.dataset_version = dataset_version
        self.top_dir = top_dir
        self.conf_dir = conf_dir
        self.input_dir = input_dir

    def _load_dataset(self, params):
        """
        Gets dataset name from next_params and loads the `dataset_name.yaml`
        file in conf_dir. Updates parameters of the dataset columns with the
        values specified in next_params.

        For a column type, a column parameter in the specified in `params` will
        update the parameter value in all columns of the same type. For
        example, if `filter_min_score` field exists in the `params`, it will
        update `filter_min_score` value for all columns of type Column.CS. It
        is also possible to update only certain column's value by specifying
        `nature_filter_min_score`. It will update the `filter_min_score` value
        only for the column with `nature` and type Columns.CS.

        For example `authors_filter_min_score` will update only for the columns
        whose nature is `authors` and type is `Columns.CS`.

        For each column type, the field that can be updated by params are as
        follows:

        Columns.CS type
        ---------------
        - `filter_min_score`

        Columns.CORPUS
        --------------
        -  `min_df`
        - `max_df`
        - `nb_grams`
        - `max_features`
        - `min_word_length`
        - `vocab_sample`

        Columns.FILTER
        --------------
        - `filter_value`

        Parameters
        ----------
        params: dict
            set of parameters to be used for configuring dataset columns.

        Returns
        -------
        cartodata.pipeline.datasets.Dataset
        """
        dataset = load_pipeline(self.dataset_name, self.conf_dir,
                                False).dataset
        dataset.version = self.dataset_version

        for column in dataset.columns:
            if column.type == Columns.CS:
                self._set_column_value(params, column, "filter_min_score")
            elif column.type == Columns.CORPUS:
                self._set_column_value(params, column, "min_df")
                self._set_column_value(params, column, "max_df")
                self._set_column_value(params, column, "vocab_sample")
                self._set_column_value(params, column, "max_features")
                self._set_column_value(params, column, "min_word_length")
                self._set_column_value(params, column, "nb_grams")
            elif column.type == Columns.FILTER:
                self._set_column_value(params, column, "filter_value")

        dataset_params = self._load_kwargs(params, PARAM_LIST_DATASET)

        fileurl = dataset_params.get("fileurl", None)
        filename = dataset_params.get("filename", None)

        if fileurl is not None:
            dataset.fileurl = fileurl
        if filename is not None:
            dataset.filename = filename

        return dataset

    def _set_column_value(self, params, column, field):
        """Checks if the `nature_field` exists in the specified `params`. If
        it does not exist, checks if  `field` exists in the `params`. If
        a value is found, updates the column's field value with value.
        """
        field_value = params.get(
                f"{column.nature}__{field}", None
        )
        if field_value is None:
            field_value = params.get(field, None)
        if field_value != "None" and field_value is not None:
            setattr(column, field, field_value)

    def _load_kwargs(self, next_params, param_list):
        kwargs = {}
        for param in param_list:
            value = next_params.get(param, None)
            if value is not None and value != 'None':
                kwargs[param] = value
        return kwargs

    def _init_pipeline(self, dataset, next_params):
        robustseed = next_params["robustseed"]

        key_nD, kwargs_nD = PhaseProjectionND.get_key_params(next_params)
        projection_nd = get_projection_nd(key_nD, **kwargs_nD)

        key_2D, kwargs_2D = PhaseProjection2D.get_key_params(next_params)
        projection_2d = get_projection_2d(key_2D, **kwargs_2D)
        aligned = "aligned" in key_2D

        key_clustering, kwargs_clustering = PhaseClustering.get_key_params(
            next_params
        )
        clustering = get_clustering(key_clustering, **kwargs_clustering)

        neighboring = get_neighboring(15, [0, 0.5, 0.5, 0, 0])

        dataset_params = self._load_kwargs(next_params, PARAM_LIST_DATASET)
        prev_version = dataset_params.get("prev_version", None)
        if prev_version is not None:
            dataset.slice_count = 1

        pipeline = get_pipeline(dataset,
                                top_dir=self.top_dir,
                                input_dir=self.input_dir,
                                projection_nd=projection_nd,
                                projection_2d=projection_2d,
                                clustering=clustering,
                                neighboring=neighboring,
                                hierarchical_dirs=True,
                                optimisation_seed=robustseed,
                                aligned=aligned)

        if prev_version is not None:
            prev_dir = Path(str(pipeline.working_dir).replace(
                pipeline.dataset.version, prev_version
            )
                            )

            if not prev_dir.exists():
                pipeline = None

        return pipeline

    def run_steps(self, next_params):
        dataset = self._load_dataset(next_params)
        pipeline = self._init_pipeline(dataset, next_params)
        pipeline_params = pipeline.params
        pipeline_params.update(next_params)
        next_params = pipeline_params

        if pipeline.is_aligned:
            last_slice_index = pipeline.dataset.slice_count - 1
            pipeline.set_current_slice(last_slice_index)

        dataset_dir = pipeline.working_dir
        key_nD = pipeline.projection_nd.key
        dir_nD = pipeline.get_nD_dir()
        key_2D = pipeline.projection_2d.key
        dir_2D = pipeline.get_2D_dir()
        words_index = pipeline.dataset.corpus_index
        clus_dir = pipeline.get_clus_dir()

        if pipeline.is_aligned:
            pipeline.working_dir = pipeline.main_working_dir

        self.set_next_params(next_params)

        # generate entity matrices
        pipeline.generate_entity_matrices()

        # run n-dimensional projection and save scores
        pipeline.do_projection_nD()
        self.add_nD_scores(dataset_dir, dir_nD)

        # run 2-dimensional projection and save plots and scores
        pipeline.do_projection_2D()
        plots = pipeline.save_2D_plots()

        if pipeline.is_aligned:
            plots = plots[last_slice_index]
        _, fig = plots
        self.add_2D_scores(
            pipeline.natures, dataset_dir, key_nD, dir_nD,
            key_2D, dir_2D, [fig]
        )

        # run clustering and save scores
        pipeline.create_clusters()
        plots = pipeline.save_plots(save_2D=False)
        if pipeline.is_aligned:
            plots = plots[last_slice_index]
        self.add_clustering_scores(
            pipeline.natures, pipeline.clustering.natures,
            dataset_dir, key_nD, dir_nD, key_2D, dir_2D, words_index,
            clus_dir, plots
        )

        # add final score
        self.add_final_score(clus_dir)

        self.finish_iteration(clus_dir)
