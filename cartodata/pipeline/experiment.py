import logging
from pathlib import Path

from cartodata.model_selection.experiment import (
    BaseExperiment, PARAM_LIST_DATASET
)
from cartodata.pipeline.common import get_pipeline
from cartodata.phases import (
    PhaseProjectionND, PhaseProjection2D, PhaseClustering, PhaseNeighboring,
    PhasePost
)

logger = logging.getLogger(__name__)


class PipelineExperiment(BaseExperiment):

    def _init_pipeline(self, dataset, next_params):
        robustseed = next_params["robustseed"]

        projection_nD = PhaseProjectionND.get_executor(next_params)
        projection_2D = PhaseProjection2D.get_executor(next_params)
        aligned = projection_2D.is_aligned
        clustering = PhaseClustering.get_executor(next_params)

        neighboring = PhaseNeighboring.get_executor(next_params)

        dataset_params = self._load_kwargs(next_params, PARAM_LIST_DATASET)
        prev_version = dataset_params.get("prev_version", None)
        if prev_version is not None:
            dataset.slice_count = 1

        pipeline = get_pipeline(dataset,
                                top_dir=self.top_dir,
                                input_dir=self.input_dir,
                                projection_nd=projection_nD,
                                projection_2d=projection_2D,
                                clustering=clustering,
                                neighboring=neighboring,
                                hierarchical_dirs=True,
                                optimisation_seed=robustseed,
                                aligned=aligned)

        if prev_version is not None:
            prev_dir = Path(str(pipeline.working_dir).replace(
                pipeline.dataset.version, prev_version
            )
                            )

            if not prev_dir.exists():
                pipeline = None

        return pipeline

    def run_steps(self, next_params):
        dataset = self._load_dataset(next_params)
        pipeline = self._init_pipeline(dataset, next_params)

        if pipeline.is_aligned:
            last_slice_index = pipeline.dataset.slice_count - 1
            pipeline.set_current_slice(last_slice_index)

        dir_mat = pipeline.working_dir
        key_nD = pipeline.projection_nd.key
        dir_nD = pipeline.get_nD_dir()
        key_2D = pipeline.projection_2d.key
        dir_2D = pipeline.get_2D_dir()
        words_index = pipeline.dataset.corpus_index
        dir_clus = pipeline.get_clus_dir()

        if pipeline.is_aligned:
            pipeline.working_dir = pipeline.main_working_dir

        # generate entity matrices
        pipeline.generate_entity_matrices()

        # run n-dimensional projection and save scores
        pipeline.do_projection_nD()
        phase_result = self.add_nD_scores(
            key_nD, dir_mat, dir_nD, pipeline.projection_nd.params_values
        )

        self.result.append(phase_result)
        self.result.persist_scores(dir_nD)
        logger.debug(
            f"{PhaseProjectionND.long_name()} scores:\n{phase_result.print()}"
        )
        logger.info("Finished running nD scoring.")

        # run 2-dimensional projection and save plots and scores
        pipeline.do_projection_2D()
        plots = pipeline.save_2D_plots()

        if pipeline.is_aligned:
            plots = plots[last_slice_index]
        _, fig = plots
        phase_result = self.add_2D_scores(
            pipeline.natures, dir_mat, key_nD, key_2D, dir_nD,
            dir_2D, [fig], pipeline.projection_2d.params_values
        )

        self.result.append(phase_result)
        self.result.persist_scores(dir_2D)
        logger.debug(
            f"{PhaseProjection2D.long_name()} scores:\n{phase_result.print()}"
        )
        logger.info("Finished running 2D scoring.")

        # run clustering and save scores
        pipeline.do_clustering()
        plots = pipeline.save_plots(save_2D=False)
        if pipeline.is_aligned:
            plots = plots[last_slice_index]
        phase_result = self.add_clustering_scores(
            pipeline.natures, pipeline.clustering.natures, key_nD, key_2D,
            dir_mat, dir_nD, dir_2D, dir_clus, words_index, plots,
            pipeline.clustering.params_values
        )
        self.result.append(phase_result)
        self.result.persist_scores(dir_clus)
        logger.debug(
            f"{PhaseClustering.long_name()} scores:\n{phase_result.print()}"
        )
        logger.info("Finished running clustering scoring.")

        # add final score
        phase_result = self.add_post_scores(dir_clus)
        self.result.append(phase_result)
        self.result.persist_scores(dir_clus)
        logger.debug(
            f"{PhasePost.long_name()} scores:\n{phase_result.print()}"
        )
        logger.info("Finished running post scoring.")

        self.finish_iteration(next_params, dir_clus)
