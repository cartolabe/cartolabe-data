import logging
from pathlib import Path
from yaml import YAMLObject

from cartodata.operations import (
    dump_matrices, load_matrices_from_dumps, load_scores, dump_scores,
    load_labels, dump_labels
)

logger = logging.getLogger(__name__)


class BaseEntity():

    def __init__(self):
        self._all_params = []
        self._phase = None

    @property
    def phase(self):
        return self._phase

    @property
    def params(self):
        """Returns the parameter values of this estimator as string
        contatenated with ``_`` character.

        Returns
        -------
        str
        """
        def get(object, attribute):
            value = getattr(object, attribute)
            if isinstance(value, list):
                return "_".join(str(item) for item in value)
            return str(value)

        return "_".join(
            get(self, item) for item in self._all_params
        ).replace(" ", "")

    @property
    def params_values(self):
        """Returns all parameter name-parameter value pairs as a dictionary.

        Returns
        -------
        dict
        """
        return dict([(item, getattr(self, item)) for item in self._all_params])

    def _add_to_params(self, params):
        """Adds each parameter name-parameter value in the ``params`` to list
        of all parameters of the estimator.

        Parameters
        ----------
        params: tuple or list of tuples
            Parameter name - parameter value pairs as tuple. Or a list of
        tuples for multiple parameters.
        """
        if isinstance(params, str):
            self._all_params.append(params)
        elif isinstance(params, list):
            self._all_params.extend(params)
        else:
            raise ValueError("Please specify a list or a string!")


class BaseEstimator(BaseEntity, YAMLObject):

    def __init__(self, key):
        """
        Parameters
        ----------
        key: str
            Type of the estimator.
        """
        super().__init__()
        self.key = key
        self._add_to_params("key")

    @classmethod
    def filter_kwargs(cls, phase, next_params):
        kwargs = {}
        for param in cls.ALL_PARAMS:
            value = next_params.get(phase.prefix(param), None)
            if value is None:
                value = next_params.get(param, None)
            if value is not None and value != 'None':
                kwargs[param] = value
        return kwargs

    def execute(self, matrices, dataset, dump_dir):
        raise NotImplementedError()


class BaseProjection(BaseEstimator):

    def __init__(self, key):
        super().__init__(key)

    def matrices_exist(self, natures, working_dir):
        """Checks if entity matrices are already generated for this projection
        type.

        Parameters
        ----------
        natures: list
            list of natures to search for corresponding matrix files
        working_dir: str, Path
            the directory to look for the matrices

        Returns
        -------
            bool
        True if the entity matrices for this projection is already generated;
        False otherwise.

        raises
        ------
            Exception
            if the specified ``working_dir`` is not valid or ``natures`` is
        ``None`` or empty.
        """
        if natures is None or len(natures) == 0:
            raise ValueError("No entity natures specified!")

        try:
            working_dir = Path(working_dir)
            for nature in natures:
                entity_files = working_dir.rglob(
                    f"{nature}_{self.key}.*"
                )
                if len(list(entity_files)) == 0:
                    logger.info(
                        f"Entity matrix for nature {nature} does not exist for"
                        f" {self.key}."
                    )
                    return False

            logger.info(
                f"Matrices of type {self.key} for {', '.join(natures)}"
                " already exist."
            )
            return True
        except Exception as ex:
            logger.error(f"Specified directory {working_dir} is not valid.")
            raise ex

    def dump_matrices(self, natures, matrices, working_dir, key=None):
        if natures is None or len(natures) == 0:
            raise ValueError("No entity natures specified!")
        if key is None:
            key = self.key
        logger.info(f'Saving matrices for {key} ...')

        if isinstance(working_dir, Path) or isinstance(working_dir, str):
            dump_matrices(natures, matrices, key, working_dir)

        else:
            for i, dir_i in enumerate(working_dir):
                dump_matrices(natures, matrices[i], key, dir_i)

    def load_matrices(self, natures, working_dir, key=None):
        """Looks for the matrices for the specified ``natures`` in the
        ``working_dir`` and loads them to a list.

        Parameters
        ----------
        natures: list
            the list of natures to load
        working_dir: str, Path, list
            the directory where the matrices are stored, or the list of
        directories where the matrices are stored
        key: str
            the key for the matrices. If empty string, uses the key for this
        current object

        Returns
        -------
        list
            the list of matrices for the specified ``natures``, or list of list
        of matrices for the specifed ``natures``; if ``working_dir`` is
        specified as a list of directories
        raises
        ------
            Exception
            if the specified ``natures`` is ``None`` or empty.
        """
        if key is None:
            key = self.key
        if natures is None or len(natures) == 0:
            raise ValueError("No entity natures specified!")

        logger.info(f'Loading {key} matrices from dumps...')

        if isinstance(working_dir, Path) or isinstance(working_dir, str):
            return load_matrices_from_dumps(natures, key, working_dir)

        matrices_2D_all = []
        for dir_sub in working_dir:
            matrices_2D_all.append(
                load_matrices_from_dumps(natures, key, dir_sub)
            )
        return matrices_2D_all

    def load_scores(self, natures, working_dir, suffix=""):
        if isinstance(working_dir, Path) or isinstance(working_dir, str):
            return load_scores(natures, working_dir, suffix)

        scores_all = []
        for dir_sub in working_dir:
            scores_all.append(load_scores(natures, dir_sub, suffix))
        return scores_all

    def dump_scores(self, natures, scores, working_dir, suffix=""):
        dump_scores(natures, scores, working_dir, suffix)

    def load_labels(self, natures, working_dir):
        return load_labels(natures, working_dir)

    def dump_labels(self, natures, labels, working_dir):
        dump_labels(natures, labels, working_dir)
