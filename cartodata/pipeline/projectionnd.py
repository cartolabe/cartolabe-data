import logging

from cartodata.pipeline.base import BaseProjection
from cartodata.projection import (
    lsa_projection, lda_projection, doc2vec_projection, bert_projection
)

logger = logging.getLogger(__name__)


def get_executor_nD(key):
    if key == "lsa":
        return LSAProjection
    elif key == "lda":
        return LDAProjection
    elif key == "doc2vec":
        return Doc2VecProjection
    elif key == "bert":
        return BertProjection


class ProjectionND(BaseProjection):
    """Base class for n-dimensional projection.

    N-dimensional projection is used to create topic models for the specified
    documents. It creates an n-dimensional vector for each document.

    Attributes
    ----------
    key: str
       The key that is used when saving n-dimensional vectors generated from
    the documents specified; refers to the projection method used.
    num_dims: int
       The number of dimensions of the vector for each document corresponding
    to the number of terms to extract from each document.
    normalize: bool
       Applies l2-norm to the resulting projection matrices.

    Methods
    -------

    execute(matrices, dataset, dump_dir)
        Executes n-dimensional projection on the specified matrices.

    """

    def __init__(self, key, num_dims, normalize):
        """
        Parameters
        ----------
        key: str
           The key that is used when saving n-dimensional vectors generated
        from the documents specified; refers to the projection method used.
        num_dims: int
           The number of dimensions of the vector for each document
        corresponding to the number of terms to extract from each document.
        normalize: bool
           Applies l2-norm to the resulting projection matrices.

        """
        super().__init__(key)
        self.num_dims = num_dims
        self.normalize = normalize

        self._add_to_params(["num_dims", "normalize"])

    def load_execute(self, dataset, dir_mat, dir_nD, dump=True,
                     force=False, return_mat=False):
        """Loads the entity matrices and executes n-dimensional projection.
        If ``force`` is False, checks if the matrices are already
        generated. Loads and returns them if ``return_mat`` is True.

        Parameters
        ----------
        dataset: cartodata.pipeline.Dataset
            the dataset object from which the entity matrices are generated
        dir_mat: str, pathlib.Path
            the path where the entity matrices are saved
        dir_nD: str, pathlib.Path
            the path where the generated matrices should be saved
        dump: bool
            boolean value to indicate if the generated matrices should be saved
        under ``dir_nD``
        force: bool
            boolean value to indicate if the matrices should be regenerated or
        loaded from existing.
        return_mat: bool
            boolean value to indicate if the projected matrices should be
        returned from the method

        Returns
        -------
        list of matrices of shape (num_dims, entity_count) if ``return_mat`` is
        True; None otherwise

        """
        natures = dataset.natures

        # if not force to regenerate matrices and the matrices already exist
        if not force and self.matrices_exist(natures, dir_nD):
            logger.info(
                f"{self.key} matrices already exist, will not regenerate."
            )
            if return_mat:
                return self.load_matrices(natures, dir_nD)
            else:
                return None

        # if the matrices do not already exist or force=True
        matrices = dataset.load_matrices(natures, dir_mat)

        return self.execute(matrices, dataset, dir_nD, dump)

    def execute(self, matrices, dataset, dir_nD=None, dump=False):
        """Executes n-dimensional projection on the specified matrices.

        Parameters
        ----------
        matrices: list
            list of entity matrices
        dataset: cartodata.pipeline.Dataset
            the dataset object from which the entity matrices are generated
        dir_nD: str, pathlib.Path
            the path where the generated nD-matrices should be saved
        dump: bool
            boolean value to indicate if the generated matrices should be saved
        under ``dir_nD``

        Returns
        -------
        list of matrices of shape (num_dims, entity_count)

        """
        logger.info(f"Starting {self.key} projection...")

        matrices_nD = self._execute(matrices, dataset, dir_nD)

        if dump:
            self.dump_matrices(dataset.natures, matrices_nD, dir_nD)

        logger.info(f'Finished {self.key} projection')

        return matrices_nD


class LSAProjection(ProjectionND):
    """An n-dimensional projection class that uses Latent Semantice Analysis
    (LSA).

    N-dimensional projection is used to create topic models for the specified
    documents. It creates an n-dimensional vector for each document.

    Attributes
    ----------
    key: str
        The key that is used when saving n-dimensional vectors generated
    from the documents specified; refers to the projection method used.
    num_dims: int
        The number of dimensions of the vector for each document corresponding
    to the number of terms to extract from each document.
    normalize: bool
        Applies l2-norm to the resulting projection matrices.

    Methods
    -------
    load_execute(self, dataset, dir_mat, dir_nD, dump, force, return_mat)
        Loads the entity matrices and executes n-dimensional projection on the
    entity matrices.

    execute(matrices, dataset, dir_nD, dump)
        Executes n-dimensional projection on the specified matrices.
    """

    yaml_tag = u'!LSAProjection'

    ALL_PARAMS = {
        "num_dims",
        "randomized_svd_n_iter",
        "max_size",
        "random_state",
        "normalize"
    }

    def __init__(self, num_dims=200, randomized_svd_n_iter=5, max_size=1000,
                 random_state=None, normalize=True):
        """
        Parameters
        ----------
        num_dims: int, default=200
           The number of dimensions of the vector for each document
        corresponding to the number of terms to extract from each document.
        randomized_svd_n_iter: int, default=5
        max_size: int, default=1000
        random_state: int, default=None
        normalize: bool, default=True
           Applies l2-norm to the resulting projection matrices.

        """
        super().__init__("lsa", num_dims, normalize)

        self.randomized_svd_n_iter = randomized_svd_n_iter
        self.max_size = max_size
        self.random_state = random_state

    def _execute(self, matrices, dataset, dir_nD=None):
        words_index = dataset.corpus_index
        return lsa_projection(
            self.num_dims, matrices[words_index], matrices,
            randomized_svd_n_iter=self.randomized_svd_n_iter,
            max_size=self.max_size, normalize=self.normalize,
            random_state=self.random_state, dump_dir=dir_nD
        )


class LDAProjection(ProjectionND):
    """An n-dimensional projection class that uses Latent Dirichlet Allocation
    (LDA).

    N-dimensional projection is used to create topic models for the specified
    documents. It creates an n-dimensional vector for each document.

    Attributes
    ----------
    key: str
        The key that is used when saving n-dimensional vectors generated
    from the documents specified; refers to the projection method used.
    num_dims: int
       The number of dimensions of the vector for each document corresponding
    to the number of terms to extract from each document.
    update_every: int
       Number of documents to be iterated through for each update. Set to 0 for
    batch learning, > 1 for online iterative learning.
    passes: int
       Number of passes through the corpus during training.
    normalize: bool
        Applies l2-norm to the resulting projection matrices.

    Methods
    -------
    load_execute(self, dataset, dir_mat, dir_nD, dump, force, return_mat)
        Loads the entity matrices and executes n-dimensional projection on the
    entity matrices.

    execute(matrices, dataset, dir_nD, dump)
        Executes n-dimensional projection on the specified matrices.
    """

    yaml_tag = u'!LDAProjection'

    ALL_PARAMS = {
        "num_dims",
        "update_every",
        "passes",
        "normalize"
    }

    def __init__(self, num_dims=200, update_every=0, passes=20,
                 normalize=True):
        """
        Parameters
        ----------
        num_dims: int
           The number of dimensions of the vector for each document
        corresponding to the number of terms to extract from each document.
        update_every: int, default=0
           Number of documents to be iterated through for each update. Set to
        0 for batch learning, > 1 for online iterative learning.
        passes: int, default=20
           Number of passes through the corpus during training.
        normalize: bool, default=True
           Applies l2-norm to the resulting projection matrices.

        """
        super().__init__("lda", num_dims, normalize)

        self.update_every = update_every
        self.passes = passes

        self._add_to_params(["update_every", "passes"])

    def _execute(self, matrices, dataset, dir_nD=None):
        words_index = dataset.corpus_index
        return lda_projection(
            self.num_dims, words_index, matrices,
            update_every=self.update_every, passes=self.passes,
            normalize=self.normalize
        )


class Doc2VecProjection(ProjectionND):
    """An n-dimensional projection class that uses Doc2vec.

    N-dimensional projection is used to create topic models for the specified
    documents. It creates an n-dimensional vector for each document.

    Attributes
    ----------
    key: str
       The key that is used when saving n-dimensional vectors generated from
    the documents specified.
    num_dims: int
       The number of dimensions of the vector for each document corresponding
    to the number of terms to extract from each document.
    workers: int
        Use these many worker threads to train the model (=faster training
    with multicore machines).
    epochs: int
        Number of iterations (epochs) over the corpus. Defaults to 10 for
    Doc2Vec.
    batch_words: int
    trim_rule: function
        Vocabulary trimming rule, specifies whether certain words should remain
    in the vocabulary, be trimmed away, or handled using the default (discard
    if word count < min_count). Can be None (min_count will be used, look to
    keep_vocab_item()), or a callable that accepts parameters (word, count,
    min_count) and returns either gensim.utils.RULE_DISCARD,
    gensim.utils.RULE_KEEP or gensim.utils.RULE_DEFAULT. The rule, if given, is
    only used to prune vocabulary during current method call and is not stored
    as part of the model.
    alpha: float
        The initial learning rate.
    window: int
        The maximum distance between the current and predicted word within a
    sentence.
    seed: int
        Seed for the random number generator. Initial vectors for each word are
    seeded with a hash of the concatenation of word + str(seed).
    hs: {1,0}
        If 1, hierarchical softmax will be used for model training. If set to
    0, and negative is non-zero, negative sampling will be used.
    negative: int
        If > 0, negative sampling will be used, the int for negative specifies
    how many “noise words” should be drawn (usually between 5-20). If set to 0,
    no negative sampling is used.
    ns_exponent: float
        The exponent used to shape the negative sampling distribution. A value
    of 1.0 samples exactly in proportion to the frequencies, 0.0 samples all
    words equally, while a negative value samples low-frequency words more than
    high-frequency words. The popular default value of 0.75 was chosen by the
    original Word2Vec paper. More recently, in
    https://arxiv.org/abs/1804.04212, Caselles-Dupré, Lesaint, & Royo-Letelier
    suggest that other values may perform better for recommendation
    applications.
    cbow_mean: int
    min_alpha: float
        Learning rate will linearly drop to min_alpha as training progresses.
    compute_loss: bool
    dm_mean: {1,0}
        If 0, use the sum of the context word vectors. If 1, use the mean. Only
    applies when dm is used in non-concatenative mode.
    dm: {1,0}
        Defines the training algorithm. If dm=1, ‘distributed memory’ (PV-DM)
    is used. Otherwise, distributed bag of words (PV-DBOW) is employed.
    dbow_words: {1,0}
        If set to 1 trains word-vectors (in skip-gram fashion) simultaneous
    with DBOW doc-vector training; If 0, only trains doc-vectors (faster).
    dm_concat: {1,0}
        If 1, use concatenation of context vectors rather than sum/average;
    Note concatenation results in a much-larger model, as the input is no
    longer the size of one (sampled or arithmetically combined) word vector,
    but the size of the tag(s) and all words in the context strung together.
    dm_tag_count: int
        Expected constant number of document tags per document, when using
    dm_concat mode.
    normalize: bool
        Applies l2-norm to the resulting projection matrices.

    Methods
    -------
    load_execute(self, dataset, dir_mat, dir_nD, dump, force, return_mat)
        Loads the entity matrices and executes n-dimensional projection on the
    entity matrices.

    execute(matrices, dataset, dir_nD, dump)
        Executes n-dimensional projection on the specified matrices.
    """
    yaml_tag = u'!Doc2vecProjection'

    ALL_PARAMS = {
        "num_dims",
        "epochs",
        "batch_words",
        "alpha",
        "window",
        "min_alpha",
        "normalize",
    }

    def __init__(self, num_dims=200, workers=3, epochs=5, batch_words=10000,
                 trim_rule=None, alpha=0.025, window=5, seed=1, hs=0,
                 negative=5, ns_exponent=0.75, cbow_mean=1, min_alpha=0.0001,
                 compute_loss=False, dm_mean=None, dm=1, dbow_words=0,
                 dm_concat=0, dm_tag_count=1, normalize=True):
        """
        Parameters
        ----------
        num_dims: int
           The number of dimensions of the vector for each document
        corresponding to the number of terms to extract from each document.
        workers: int, default=3
            Use these many worker threads to train the model (=faster training
        with multicore machines).
        epochs: int, default=5
            Number of iterations (epochs) over the corpus. Defaults to 10 for
        Doc2Vec.
        batch_words: int, default=10000
        trim_rule: function, default=None
            Vocabulary trimming rule, specifies whether certain words should
        remain in the vocabulary, be trimmed away, or handled using the
        default (discard if word count < min_count). Can be None (min_count
        will be used, look to keep_vocab_item()), or a callable that accepts
        parameters (word, count, min_count) and returns either
        gensim.utils.RULE_DISCARD, gensim.utils.RULE_KEEP or
        gensim.utils.RULE_DEFAULT. The rule, if given, is only used to prune
        vocabulary during current method call and is not stored as part of the
        model.
        alpha: float, default=0.025
            The initial learning rate.
        window: int, default=5
            The maximum distance between the current and predicted word within
        a sentence.
        seed: int, default=1
            Seed for the random number generator. Initial vectors for each word
        are seeded with a hash of the concatenation of word + str(seed).
        hs: {1,0}, default=0
            If 1, hierarchical softmax will be used for model training. If set
        to 0, and negative is non-zero, negative sampling will be used.
        negative: int, default=5
            If > 0, negative sampling will be used, the int for negative
        specifies how many “noise words” should be drawn
        (usually between 5-20). If set to 0, no negative sampling is used.
        ns_exponent: float, default=0.75
            The exponent used to shape the negative sampling distribution. A
        value of 1.0 samples exactly in proportion to the frequencies, 0.0
        samples all words equally, while a negative value samples
        low-frequency words more than high-frequency words. The popular
        default value of 0.75 was chosen by the original Word2Vec paper. More
        recently, in https://arxiv.org/abs/1804.04212, Caselles-Dupré, Lesaint,
        & Royo-Letelier suggest that other values may perform better for
        recommendation applications.
        cbow_mean: int, default=1
        min_alpha: float, default=0.0001
            Learning rate will linearly drop to min_alpha as training
        progresses.
        compute_loss: bool, default=False
        dm_mean: {1,0}, default=None
            If 0, use the sum of the context word vectors. If 1, use the mean.
        Only applies when dm is used in non-concatenative mode.
        dm: {1,0}, default=1
            Defines the training algorithm. If dm=1, ‘distributed memory’
        (PV-DM) is used. Otherwise, distributed bag of words (PV-DBOW) is
        employed.
        dbow_words: {1,0}, defalt=0
            If set to 1 trains word-vectors (in skip-gram fashion) simultaneous
        with DBOW doc-vector training; If 0, only trains doc-vectors (faster).
        dm_concat: {1,0}, default=0
            If 1, use concatenation of context vectors rather than sum/average;
        Note concatenation results in a much-larger model, as the input is no
        longer the size of one (sampled or arithmetically combined) word
        vector, but the size of the tag(s) and all words in the context strung
        together. dm_tag_count: int, default=1
            Expected constant number of document tags per document, when using
        dm_concat mode.
        normalize: bool, default=True
           Applies l2-norm to the resulting projection matrices.

        """
        super().__init__("doc2vec", num_dims, normalize)

        self.workers = workers
        self.epochs = epochs
        self.batch_words = batch_words
        self.trim_rule = trim_rule
        self.alpha = alpha
        self.window = window
        self.seed = seed
        self.hs = hs
        self.negative = negative
        self.ns_exponent = ns_exponent
        self.cbow_mean = cbow_mean
        self.min_alpha = min_alpha
        self.compute_loss = compute_loss
        self.dm_mean = dm_mean
        self.dm = dm
        self.dbow_words = dbow_words
        self.dm_concat = dm_concat
        self.dm_tag_count = dm_tag_count

        # TODO complete this
        # self._add_to_params([("epochs", epochs)])

    def _execute(self, matrices, dataset, dir_nD=None):
        words_index = dataset.corpus_index
        vocab_df = dataset.corpus

        return doc2vec_projection(
            self.num_dims, words_index, matrices, vocab_df,
            workers=self.workers, epochs=self.epochs, callbacks=(),
            batch_words=self.batch_words, trim_rule=self.trim_rule,
            alpha=self.alpha, window=self.window, seed=self.seed, hs=self.hs,
            min_alpha=self.min_alpha, compute_loss=self.compute_loss,
            dm_mean=self.dm_mean, dm=self.dm, dbow_words=self.dbow_words,
            dm_concat=self.dm_concat, dm_tag_count=self.dm_tag_count,
            normalize=self.normalize
        )


class BertProjection(ProjectionND):

    ALL_PARAMS = {
        "family",
        "batch_size",
        "max_length",
        "pt_device"
    }

    def __init__(self, family="specter2", batch_size=10, max_length=256,
                 pt_device=None, normalize=True):
        super().__init__("bert", 768, normalize)
        self.family = family
        self.batch_size = batch_size
        self.pt_device = pt_device
        self.max_length = max_length

        self._add_to_params(["family", "max_length"])

    def _execute(self, matrices, dataset, dir_nD=None):
        corpus = dataset.corpus

        return bert_projection(
            matrices, corpus, self.family, self.normalize, self.pt_device,
            self.batch_size, self.max_length
        )
