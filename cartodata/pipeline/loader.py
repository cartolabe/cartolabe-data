from pathlib import Path

from yaml import SafeLoader, load
from yaml.nodes import MappingNode

from cartodata.pipeline.columns import (
    IdentityColumn, CSColumn, FilterColumn, CorpusColumn, TfidfCorpusColumn
)
from cartodata.pipeline.common import (
    Pipeline, AlignedPipeline, GuidedAlignedPipeline
)
from cartodata.pipeline.clustering import KMeansClustering, HDBSCANClustering
from cartodata.pipeline.datasets import CSVDataset, CSVSliceDataset
from cartodata.pipeline.exporting import (
    ExportNature, MetadataColumn, EntityMetadataMapColumn
)
from cartodata.pipeline.neighbors import AllNeighbors
from cartodata.pipeline.projectionnd import (
    LSAProjection, LDAProjection,  Doc2VecProjection, BertProjection
)
from cartodata.pipeline.projection2d import (
    UMAPProjection, GuidedUMAPProjection, IndirectUMAPProjection,
    AlignedUMAPProjection, TSNEProjection, AlignedGuidedUMAPProjection,
    AlignedKNeighborsProjection,
)


def _get_dataset_dir(dataset, conf_dir, aligned=False):
    dataset_dir = Path(f"{conf_dir}/{dataset}")

    if aligned:
        dataset_dir = dataset_dir / "aligned"

    return dataset_dir


def load_dataset(dataset, conf_dir, aligned=False):
    dataset_dir = _get_dataset_dir(dataset, conf_dir, aligned)
    filename = dataset_dir / "dataset.yaml"

    assert filename.exists(), (
        "Please specify a valid dataset file."
        f"{filename} does not exist."
    )

    with open(filename) as f:
        dataset = load(f, Loader=_get_yaml_loader())

    return dataset


def load_pipeline(dataset, conf_dir, aligned=False):
    dataset_dir = _get_dataset_dir(dataset, conf_dir, aligned)

    filename = dataset_dir / "pipeline.yaml"
    assert filename.exists(), (
        "Please specify a valid dataset file."
        f"{filename} does not exist."
    )

    with open(filename) as f:
        pipeline = load(f, Loader=_get_yaml_loader())

    dataset_obj = load_dataset(dataset, conf_dir, aligned)
    pipeline.update_dataset(dataset_obj)

    return pipeline


def _get_yaml_loader():
    """Add constructors to PyYAML loader."""
    loader = SafeLoader
    # pipelines
    loader.add_constructor("!Pipeline", _pipeline_const)
    loader.add_constructor("!AlignedPipeline", _aligned_pipeline_const)
    loader.add_constructor("!GuidedAlignedPipeline",
                           _guided_aligned_pipeline_const)

    # nd_projections
    loader.add_constructor("!LSAProjection", _lsa_const)
    loader.add_constructor("!LDAProjection", _lda_const)
    loader.add_constructor("!Doc2VecProjection", _doc2vec_const)
    loader.add_constructor("!BertProjection", _bert_const)

    # 2d_projections
    loader.add_constructor("!UMAPProjection", _umap_const)
    loader.add_constructor("!GuidedUMAPProjection", _guided_umap_const)
    loader.add_constructor("!IndirectUMAPProjection", _indirect_umap_const)
    loader.add_constructor("!AlignedUMAPProjection", _aligned_umap_const)
    loader.add_constructor("!TSNEProjection", _tsne_const)
    loader.add_constructor("!AlignedGuidedUMAPProjection",
                           _aligned_guided_umap_const)
    loader.add_constructor("!GuidedKNeighborsProjection",
                           _aligned_kneigh_const)

    # columns
    loader.add_constructor("!IdentityColumn", _id_col_const)
    loader.add_constructor("!CSColumn", _cs_col_const)
    loader.add_constructor("!FilterColumn", _filter_col_const)
    loader.add_constructor("!CorpusColumn", _corpus_col_const)
    loader.add_constructor("!TfidfCorpusColumn", _tfidfcorpus_col_const)

    # dataset
    loader.add_constructor("!CSVDataset", _csv_dataset_const)
    loader.add_constructor("!CSVSliceDataset", _csv_slice_dataset_const)

    loader.add_constructor("!KMeansClustering", _kmeans_const)
    loader.add_constructor("!HDBSCANClustering", _hdbscan_const)
    loader.add_constructor("!AllNeighbors", _all_neighbors_const)

    # exporter
    loader.add_constructor("!ExportNature", _export_nature_const)
    loader.add_constructor("!MetadataColumn", _meta_col_const)
    loader.add_constructor("!EntityMetadataMapColumn", _meta_map_col_const)

    return loader


# pipelines
def _pipeline_const(loader: SafeLoader,
                    node: MappingNode) -> Pipeline:
    """Construct a workflow."""
    return Pipeline(**loader.construct_mapping(node, deep=True))


def _aligned_pipeline_const(loader: SafeLoader,
                            node: MappingNode) -> AlignedPipeline:
    """Construct a workflow."""
    return AlignedPipeline(**loader.construct_mapping(node, deep=True))


def _guided_aligned_pipeline_const(loader: SafeLoader,
                                   node: MappingNode) -> GuidedAlignedPipeline:
    """Construct a workflow."""
    return GuidedAlignedPipeline(**loader.construct_mapping(node, deep=True))


# nd_projections
def _lsa_const(loader: SafeLoader,
               node: MappingNode) -> LSAProjection:
    """Construct a workflow."""
    return LSAProjection(**loader.construct_mapping(node))


def _lda_const(loader: SafeLoader,
               node: MappingNode) -> LDAProjection:
    """Construct a workflow."""
    return LDAProjection(**loader.construct_mapping(node))


def _doc2vec_const(loader: SafeLoader,
                   node: MappingNode) -> Doc2VecProjection:
    """Construct a workflow."""
    return Doc2VecProjection(**loader.construct_mapping(node))


def _bert_const(loader: SafeLoader,
                node: MappingNode) -> BertProjection:
    """Construct a workflow."""
    return BertProjection(**loader.construct_mapping(node))


# 2d_projections
def _umap_const(loader: SafeLoader,
                node: MappingNode) -> UMAPProjection:
    """Construct a workflow."""
    return UMAPProjection(**loader.construct_mapping(node))


def _aligned_umap_const(loader: SafeLoader,
                        node: MappingNode) -> AlignedUMAPProjection:
    """Construct a workflow."""
    return AlignedUMAPProjection(**loader.construct_mapping(node))


def _guided_umap_const(loader: SafeLoader,
                       node: MappingNode) -> GuidedUMAPProjection:
    """Construct a workflow."""
    return GuidedUMAPProjection(**loader.construct_mapping(node))


def _indirect_umap_const(loader: SafeLoader,
                         node: MappingNode) -> IndirectUMAPProjection:
    """Construct a workflow."""
    return IndirectUMAPProjection(**loader.construct_mapping(node))


def _tsne_const(loader: SafeLoader,
                node: MappingNode) -> TSNEProjection:
    """Construct a workflow."""
    return TSNEProjection(**loader.construct_mapping(node))


def _aligned_guided_umap_const(
        loader: SafeLoader, node: MappingNode) -> AlignedGuidedUMAPProjection:
    """Construct a workflow."""
    return AlignedGuidedUMAPProjection(**loader.construct_mapping(node))


def _aligned_kneigh_const(loader: SafeLoader,
                          node: MappingNode) -> AlignedKNeighborsProjection:
    """Construct a workflow."""
    return AlignedKNeighborsProjection(**loader.construct_mapping(node))


# columns
def _id_col_const(loader: SafeLoader,
                  node: MappingNode) -> IdentityColumn:
    """Construct a workflow."""
    return IdentityColumn(**loader.construct_mapping(node, deep=True))


def _cs_col_const(loader: SafeLoader,
                  node: MappingNode) -> CSColumn:
    """Construct a workflow."""
    return CSColumn(**loader.construct_mapping(node, deep=True))


def _filter_col_const(loader: SafeLoader,
                      node: MappingNode) -> FilterColumn:
    """Construct a workflow."""
    return FilterColumn(**loader.construct_mapping(node, deep=True))


def _corpus_col_const(loader: SafeLoader,
                      node: MappingNode) -> CorpusColumn:
    """Construct a workflow."""
    return CorpusColumn(**loader.construct_mapping(node, deep=True))


def _tfidfcorpus_col_const(loader: SafeLoader,
                           node: MappingNode) -> TfidfCorpusColumn:
    """Construct a workflow."""
    return TfidfCorpusColumn(**loader.construct_mapping(node, deep=True))


# datasets
def _csv_dataset_const(loader: SafeLoader,
                       node: MappingNode) -> CSVDataset:
    """Construct a workflow."""
    return CSVDataset(**loader.construct_mapping(node, deep=True))


def _csv_slice_dataset_const(loader: SafeLoader,
                             node: MappingNode) -> CSVSliceDataset:
    """Construct a workflow."""
    return CSVSliceDataset(**loader.construct_mapping(node, deep=True))


def _kmeans_const(loader: SafeLoader,
                  node: MappingNode) -> KMeansClustering:
    """Construct a workflow."""
    return KMeansClustering(**loader.construct_mapping(node, deep=True))


def _hdbscan_const(loader: SafeLoader,
                   node: MappingNode) -> HDBSCANClustering:
    """Construct a workflow."""
    return HDBSCANClustering(**loader.construct_mapping(node, deep=True))


def _all_neighbors_const(loader: SafeLoader,
                         node: MappingNode) -> AllNeighbors:
    """Construct a workflow."""
    return AllNeighbors(**loader.construct_mapping(node, deep=True))


# exporter
def _export_nature_const(loader: SafeLoader,
                         node: MappingNode) -> ExportNature:
    """Construct a workflow."""
    return ExportNature(**loader.construct_mapping(node, deep=True))


def _meta_col_const(loader: SafeLoader,
                    node: MappingNode) -> MetadataColumn:
    """Construct a workflow."""
    return MetadataColumn(**loader.construct_mapping(node, deep=True))


def _meta_map_col_const(loader: SafeLoader,
                        node: MappingNode) -> EntityMetadataMapColumn:
    """Construct a workflow."""
    return EntityMetadataMapColumn(**loader.construct_mapping(node, deep=True))
