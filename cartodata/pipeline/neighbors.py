from cartodata.neighbors import get_neighbors, get_neighbors_path
from cartodata.pipeline.base import BaseProjection


def get_executor_nbr(key):
    if key == "all":
        return AllNeighbors
    else:
        raise NotImplementedError()


class AllNeighbors(BaseProjection):

    yaml_tag = u'!AllNeighbors'

    ALL_PARAMS = [
        "n_neighbors",
        "power_scores"
    ]

    def __init__(self, n_neighbors=10, power_scores=[0, 0.5, 0.5, 0, 0],
                 num_threads=8):
        super().__init__("neighbors")

        self.n_neighbors = n_neighbors
        self.power_scores = power_scores
        self.num_threads = num_threads

        self._add_to_params(AllNeighbors.ALL_PARAMS)

    def load_execute(self, natures, dir_mat, key_nD, dir_nD, dir_neighbors,
                     force=False):
        exists = True

        if not force:
            for neighbors_nature in natures:
                for nature in natures:
                    if not self.get_neighbors_path(
                            neighbors_nature, nature, dir_neighbors).exists():
                        exists = False

        if not exists or force:
            scores = self.load_scores(natures, dir_mat)
            matrices_nD = self.load_matrices(natures, dir_nD, key=key_nD)
            self.get_neighbors(natures, matrices_nD, scores, dir_neighbors)

    def get_neighbors(self, natures, matrices_nd, scores, dump_dir):
        for idx in range(len(matrices_nd)):
            get_neighbors(
                matrices_nd[idx], scores[idx], matrices_nd,
                power_score=self.power_scores[idx], dump_dir=dump_dir,
                neighbors_nature=natures[idx], natures=natures,
                n_neighbors=self.n_neighbors, num_threads=self.num_threads
            )

    def get_neighbors_path(self, neighbors_nature, nature, dump_dir):
        return get_neighbors_path(neighbors_nature, nature, dump_dir)
