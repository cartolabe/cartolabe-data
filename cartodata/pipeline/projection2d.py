import logging
from pathlib import Path
from pickle import dump, load

from numba.typed import List
import pandas as pd
from sklearn.manifold import TSNE
from umap import UMAP
from umap.aligned_umap import AlignedUMAP

from cartodata.pipeline.base import BaseProjection
from cartodata.pipeline.interpolator import KNeighborsInterpolator
from cartodata.projection import (
    indirect_umap_projection
)
from cartodata.utils import (
    hstack_transpose_matrices, decompose_transpose_matrix, make_relation,
    digest
)

logger = logging.getLogger(__name__)

INIT = "random"


def get_executor_2D(key):
    if key == "umap":
        return UMAPProjection
    elif key == "aligned_umap":
        return AlignedUMAPProjection
    elif key == "tsne":
        return TSNEProjection
    elif key == "guided_umap":
        return GuidedUMAPProjection
    elif key == "aligned_guided_umap":
        return AlignedGuidedUMAPProjection
    elif key == "aligned_kneighbors":
        return AlignedKNeighborsProjection


class Projection2D(BaseProjection):

    def __init__(self, key):
        super().__init__(key)

    @property
    def is_aligned(self):
        return False

    def digest(self, natures):
        initial = [self.params]
        initial.extend(natures)
        return digest("_".join(initial))

    def load_execute(self, natures, key_nD, dir_nD, dir_2D, dump=False,
                     force=False, return_mat=False):
        """Loads nD matrices and executes 2-dimensional projection.
        If ``force`` is False, checks if the matrices are already
        generated. Loads and returns them if ``return_mat`` is True.

        Parameters
        ----------
        natures: list of str
            the list of natures for the matrices to load
        key_nD: str
            the key for projection-nD
        dir_nD: str, pathlib.Path
            the path where the nD matrices are saved
        dir_2D: str, pathlib.Path
            the path where the generated matrices should be saved
        dump: bool
            boolean value to indicate if the generated matrices should be saved
        force: bool
            boolean value to indicate if the matrices should be regenerated or
        loaded from existing.
        return_mat: bool
            boolean value to indicate if the projected matrices should be
        returned from the method

        Returns
        -------
        list of matrices of shape (2, entity_count) if ``return_mat`` is
        True; None otherwise
        """

        # if not force to regenerate matrices and the matrices already exist
        if not force and self.matrices_exist(natures, dir_2D):
            logger.info(
                f"{self.key} matrices already exist, will not regenerate."
            )
            if return_mat:
                return self.load_matrices(natures, dir_2D)
            else:
                return None

        # if the matrices do not already exist or force=True
        matrices_nD = self.load_matrices(natures, dir_nD, key_nD)

        return self.execute(matrices_nD, dir_2D, natures=natures, dump=dump)

    def execute(self, matrices_nD, dir_2D=None, natures=None, dump=False):
        """Executes 2D-projection on the specifed ``matrices`` reducing the
        number of features of each matrix to 2.
        This works only if the combined number of documents for all matrices
        is ~ < 2million. Otherwise, use the indirect_umap_projection method.

        Parameters
        ----------
        matrices_nD: list of numpy.ndarray
            list of matrices of shape (num_dims, entity_count) to project to 2D
        dir_2D: str, Path
            the directory path where the generated 2D matrices should be saved
        natures: list of str
            the list of natures to be used for dump names
        dump: bool
            the boolean value to indicate if the matrices should be saved

        Returns
        -------
        list of numpy.ndarray
            list of matrices of shape (2, entity_count)
        """
        logger.info(f'Starting {self.key} projection...')

        matrices_2D = self.fit_transform(matrices_nD)

        if dump:
            assert natures is not None and dir_2D is not None, (
                "To save generated matrices natures and directory should be "
                "specifed!"
            )
            self.dump_matrices(natures, matrices_2D, dir_2D)

        logger.info(f'Finished {self.key} projection.')

        return matrices_2D

    def fit(self, matrices_nD, matrices_2D=None):
        matrices_nD_T, _ = hstack_transpose_matrices(matrices_nD)
        if matrices_2D is not None:
            matrices_2D_T, _ = hstack_transpose_matrices(matrices_2D)
        else:
            matrices_2D_T = None
        self.reducer.fit(matrices_nD_T, matrices_2D_T)

    def transform(self, matrices_nD):
        matrices_2D = []

        for matrix in matrices_nD:
            projected_matrix = self.reducer.transform(matrix.T)
            matrices_2D.append(projected_matrix.T)

        return matrices_2D

    def fit_transform(self, matrices_nD):
        global_matrix, row_counts = hstack_transpose_matrices(matrices_nD)

        embedding = self.reducer.fit_transform(global_matrix)

        matrices_2D = decompose_transpose_matrix(embedding, row_counts)

        return matrices_2D

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class UMAPProjection(Projection2D):

    yaml_tag = u'!UMAPProjection'

    ALL_PARAMS = [
        "n_neighbors",
        "min_dist",
        "metric",
        "init",
        "n_epochs",
        "learning_rate",
        "random_state"
    ]

    def __init__(self, key="umap", n_neighbors=15, min_dist=0.1,
                 metric="euclidean", n_epochs=None, learning_rate=1.0,
                 init=INIT, low_memory=True, n_jobs=-1, random_state=None,
                 target_metric='categorical', transform_mode='embedding',
                 verbose=False):
        super().__init__(key)

        self.reducer = UMAP(
            n_components=2, n_neighbors=n_neighbors, min_dist=min_dist,
            metric=metric, n_epochs=n_epochs, learning_rate=learning_rate,
            init=init, low_memory=low_memory, n_jobs=n_jobs,
            random_state=random_state, target_metric=target_metric,
            transform_seed=random_state, transform_mode=transform_mode,
            verbose=verbose
        )

        if isinstance(init, list):
            init = "list"

        self.n_neighbors = n_neighbors
        self.min_dist = min_dist
        self.metric = metric
        self.init = init
        self.learning_rate = learning_rate
        self.n_epochs = n_epochs
        self.random_state = random_state

        # if other parameters are to be used, this should be updated
        self._add_to_params(["metric",
                             "n_neighbors",
                             "min_dist",
                             "init",
                             "learning_rate",
                             "n_epochs",
                             "random_state"])


class IndirectUMAPProjection(UMAPProjection):

    yaml_tag = u'!IndirectUMAPProjection'

    ALL_PARAMS = [
        "n_neighbors",
        "min_dist",
        "metric",
        "init",
        "n_epochs",
        "learning_rate",
        "random_state",
        "max_size"
    ]

    def __init__(self, key="indirect_umap", n_neighbors=15, min_dist=0.1,
                 metric="euclidean", n_epochs=None, learning_rate=1.0,
                 init=INIT, low_memory=True, n_jobs=-1, random_state=None,
                 target_metric='categorical', transform_mode='embedding',
                 verbose=False, max_size=1500000):
        super().__init__(
            key=key, n_neighbors=n_neighbors, min_dist=min_dist,
            metric=metric, n_epochs=n_epochs, learning_rate=learning_rate,
            init=init, low_memory=low_memory, n_jobs=n_jobs,
            random_state=random_state, target_metric=target_metric,
            transform_mode=transform_mode, verbose=verbose
        )
        self.max_size = max_size

        self._add_to_params("max_size")

    def fit_transform(self, matrices):
        # TODO this should use reducer of this object
        return indirect_umap_projection(
            matrices, self.n_neighbors, self.min_dist, self.max_size
        )


class GuidedUMAPProjection(UMAPProjection):

    yaml_tag = u'!GuidedUMAPProjection'

    ALL_PARAMS = [
        "n_neighbors",
        "min_dist",
        "metric",
        "init",
        "n_epochs",
        "learning_rate",
        "random_state",
        "max_size",
        "keepsize"
    ]

    def __init__(self, key="guided_umap", n_neighbors=15, min_dist=0.1,
                 metric="euclidean", n_epochs=None, learning_rate=1.0,
                 init=INIT, low_memory=True, n_jobs=-1, random_state=None,
                 target_metric='categorical', transform_mode='embedding',
                 verbose=False, max_size=1500000, keepsize=60000):
        super().__init__(
            key=key, n_neighbors=n_neighbors, min_dist=min_dist,
            metric=metric, n_epochs=n_epochs, learning_rate=learning_rate,
            init=init, low_memory=low_memory, n_jobs=n_jobs,
            random_state=random_state, target_metric=target_metric,
            transform_mode=transform_mode, verbose=verbose
        )
        self.max_size = max_size
        self.keepsize = keepsize

        self._add_to_params(["max_size", "keepsize"])

    def fit(self, matrices_nD, matrices_2D=None):
        """Fits only with the first matrix of ``matrices_nD`` and
        ``matrices_2D`` which is assumed to be the main entity.

        Parameters
        ----------
        matrices_nD: list of numpy.ndarray
            list of matrices of shape (num_dims, entity_count)
        matrices_2D: list of numpy.ndarray
            list of matrices of shape (2, entity_count)
        """
        matrices_nD_T, _ = hstack_transpose_matrices([matrices_nD[0]])
        if matrices_2D is not None:
            matrices_2D_T, _ = hstack_transpose_matrices([matrices_2D[0]])
        else:
            matrices_2D_T = None
        self.reducer.fit(matrices_nD_T, matrices_2D_T)

    def fit_transform(self, matrices_nD):
        """Fits the model with the first matrix of the specified
        ``matrices_nD`` and transforms all matrices of ``matrices_nD``.

        Parameters
        ----------
        matrices_nD: list of numpy.ndarray
            list of matrices of shape (num_dims, entity_count) to project to 2D

        Returns
        -------
        list of numpy.ndarray
            list of matrices of shape (2, entity_count)
        """
        self.fit(matrices_nD)
        return self.transform(matrices_nD)


class TSNEProjection(Projection2D):

    yaml_tag = u'!TSNEProjection'

    ALL_PARAMS = [
        "metric",
        "perplexity",
        "early_exaggeration",
        "method",
        "init",
        "learning_rate",
        "angle",
        "random_state",
    ]

    def __init__(self, key="tsne", metric="euclidean", perplexity=30.0,
                 early_exaggeration=12.0, method="barnes_hut",
                 learning_rate=400.0, init=INIT, n_iter=1000, angle=0.5,
                 n_jobs=None, random_state=None, verbose=0):
        super().__init__(key)
        self.metric = metric
        self.perplexity = perplexity
        self.early_exaggeration = early_exaggeration
        self.method = method
        self.init = init
        self.learning_rate = learning_rate
        self.angle = angle
        self.random_state = random_state

        self.reducer = TSNE(metric=metric, perplexity=perplexity,
                            early_exaggeration=early_exaggeration,
                            method=method, learning_rate=learning_rate,
                            init=init, n_iter=n_iter, angle=angle,
                            n_jobs=n_jobs, random_state=random_state,
                            verbose=verbose)

        self._add_to_params(["metric",
                             "perplexity",
                             "early_exaggeration",
                             "method",
                             "learning_rate",
                             "init",
                             "angle",
                             "random_state"])


class AlignedUMAPProjection(Projection2D):

    ALL_PARAMS = [
        "n_neighbors",
        "min_dist",
        "metric",
        "init",
        "alignment_regularisation",
        "alignment_window_size",
        "n_epochs",
        "learning_rate",
        "random_state",
    ]

    def __init__(self, key="aligned_umap", n_neighbors=15, min_dist=0.1,
                 metric="euclidean", n_epochs=None, learning_rate=1.0,
                 init=INIT, low_memory=True, alignment_regularisation=1.0e-2,
                 alignment_window_size=3, random_state=None,
                 target_metric='categorical', verbose=False):
        super().__init__(key)

        if isinstance(init, list):
            init = "list"

        self.n_neighbors = n_neighbors
        self.min_dist = min_dist
        self.metric = metric
        self.init = init
        self.alignment_regularisation = alignment_regularisation
        self.alignment_window_size = alignment_window_size
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate
        self.random_state = random_state

        self.reducer = AlignedUMAP(
            n_components=2, n_neighbors=n_neighbors, min_dist=min_dist,
            metric=metric, n_epochs=n_epochs, learning_rate=learning_rate,
            init=init, low_memory=low_memory,
            alignment_regularisation=alignment_regularisation,
            alignment_window_size=alignment_window_size,
            random_state=random_state, target_metric=target_metric,
            transform_seed=random_state, verbose=verbose
        )

        self._add_to_params(["metric",
                             "n_neighbors",
                             "min_dist",
                             "init",
                             "alignment_regularisation",
                             "alignment_window_size",
                             "learning_rate",
                             "random_state"])

    @property
    def is_aligned(self):
        return True

    def load_reducer(self, reducer_dir, suffix):
        """Loads the model saved in ``reducer_dir`` with name
        reducer_<suffix>.pkl

        Parameters
        ----------
        reducer_dir: pathlib.Path
            the directory where the model is saved
        suffix: str
            the model name should be of the form ``reducer_<suffix>.pkl``
        """
        # From https://github.com/lmcinnes/umap/issues/672
        params_new = load(open(reducer_dir / f'reducer_{suffix}.pkl', 'rb'))
#        self.reducer.set_params(**params_new.get('umap_params'))

        for attr, value in params_new.get('umap_attributes').items():
            self.reducer.__setattr__(attr, value)
        self.reducer.__setattr__('embeddings_', List(
            params_new.get('umap_attributes').get('embeddings_')))

    def _adjust_params(self, list1, list2):
        if isinstance(list1, list):
            while len(list1) <= len(self.reducer.mappers_):
                list1.append(list1[-1])
                list2.append(list2[-1])

    def persist_reducer(self, reducer, reducer_dir, suffix):
        """Saves the model ``reducer`` to ``reducer_dir`` with name
        ``reducer_<suffix>.pkl``.

        Parameters
        ----------
        reducer:
            the model to be saved
        reducer_dir: pathlib.Path
            the directory where the model is saved
        suffix: str
            the model name should be of the form ``reducer_<suffix>.pkl``
        """
        # From https://github.com/lmcinnes/umap/issues/672
        params = reducer.get_params(deep=True)
        attributes_names = [attr for attr in reducer.__dir__(
        ) if attr not in params and attr[0] != '_']
        attributes = {key: reducer.__getattribute__(
            key) for key in attributes_names}
        attributes['embeddings_'] = list(reducer.embeddings_)

        for x in ['fit', 'fit_transform',
                  'update', 'get_params',
                  'set_params', "get_metadata_routing"]:
            del attributes[x]

        all_params = {
            'umap_params': params,
            'umap_attributes': {
                key: value for key, value in attributes.items()
            }
        }

        dump(all_params, open(reducer_dir / f'reducer_{suffix}.pkl', 'wb'))

    def load_execute(self, natures, dirs_mat, key_nD, dirs_nD, dirs_2D,
                     dir_reducer, current_version=None, prev_version=None,
                     dump=False, force=False, return_mat=False):
        """Loads nD matrices and executes 2-dimensional projection on all
        slices. If ``force`` is False, checks if the matrices are already
        generated. Loads and returns them if ``return_mat`` is True.

        Parameters
        ----------
        natures: list of str
            the list of natures for the matrices to load
        dirs_mat: list of str or pathlib.Path
            the list of directories where entity matrices and scores are
        stored for each slice. Scores for entities are necessary to create
        relations between consecutive slices.
        key_nD: str
            the key for projection-nD
        dirs_nD: list of str or pathlib.Path
            the list of directories where the nD matrices are saved for each
        slice
        dirs_2D: list of str or pathlib.Path
            the list of directories where the generated matrices should be
        saved, or are saved in the case where there is already a 2D projection
        executed
        dir_reducer: str, pathlib.Path
            the directory to save the reducer after 2D projection is executed
        current_version: str
            the version of the dataset processed
        prev_version: str
            the previous version of dataset processed with
        AlignedUMAPProjection, it will be used to find and load the reducer of
        the previous processing and update the reducer with the new matrices
        dump: bool
            boolean value to indicate if the generated matrices should be saved
        force: bool
            boolean value to indicate if the matrices should be regenerated or
        loaded from existing.
        return_mat: bool
            boolean value to indicate if the projected matrices should be
        returned from the method

        Returns
        -------
        list of list of matrices, list of scores
        a list that contains list of matrices of shape (2, entity_count) for
        each slice and the scores of entities for the last slice if
        ``return_mat`` is True; None otherwise
        """
        exist = True
        if not force:
            for dir_2D in dirs_2D:
                if not self.matrices_exist(natures, dir_2D):
                    exist = False
            if exist:
                logger.info(
                    f"{self.key} matrices already exist, will not regenerate."
                )
                digest = self.digest(natures)
                self.load_reducer(dir_reducer, digest)
                if return_mat:
                    matrices_2D_AS = self.load_matrices(natures, dirs_2D)
                    final_scores = self.load_scores(natures, dir_reducer,
                                                    digest)
                    return matrices_2D_AS, final_scores
                else:
                    return

        matrices_nD_AS = self.load_matrices(natures, dirs_nD, key=key_nD)
        scores_AS = self.load_scores(natures, dirs_mat)

        return self.execute(matrices_nD_AS, scores_AS, dir_reducer,
                            dirs_2D, natures, current_version=current_version,
                            prev_version=prev_version, dump=dump)

    def execute(self, matrices_nD_AS, scores_AS, dir_reducer, dirs_2D=None,
                natures=None, current_version=None, prev_version=None,
                dump=False):
        matrices_2D_AS = None
        digest = self.digest(natures)
        if prev_version is not None:
            # set previous version directory to load reducer from that
            # directory. For an update with existing
            # reducer prev_version should be set to None
            dir_prev = Path(str(dir_reducer).replace(current_version,
                                                     prev_version))
            scores_prev = self.load_scores(natures, dir_prev, digest)

            matrices_2D_AS = self.update(
                matrices_nD_AS, scores_AS, scores_prev, dir_prev,
                natures=natures
            )
        else:
            matrices_2D_AS = self.fit_transform(matrices_nD_AS, scores_AS)

        self.persist_reducer(self.reducer, dir_reducer, digest)
        scores_LS = scores_AS[-1]
        if dump:
            self.dump_matrices(natures, matrices_2D_AS, dirs_2D)
            self.dump_scores(natures, scores_LS, dir_reducer, digest)

        return matrices_2D_AS, scores_LS

    def fit(self, matrices_nD, matrices_2D=None):
        # cannot fit and transform separately for aligned UMAP
        raise NotImplementedError(
            "With AlignedUMAP, it is only possible to fit_transform"
        )

    def transform(self, matrices_nD):
        # cannot fit and transform separately for aligned UMAP
        raise NotImplementedError(
            "With AlignedUMAP, it is only possible to fit_transform"
        )

    def fit_transform(self, matrices_nD_AS, scores_AS):
        transposed_matrices, row_counts_all = self._transpose_matrices(
            matrices_nD_AS
        )
        relations = self._create_relations(scores_AS)

        self.reducer.fit_transform(
            transposed_matrices, relations=relations
        )

        return self._decompose_and_transpose_embeddings(
            self.reducer.embeddings_, row_counts_all
        )

    def update(self, matrices_nD_AS, scores_AS, scores_prev, dir_prev=None,
               natures=None):

        transposed_matrices_AS, row_counts_AS = self._transpose_matrices(
            matrices_nD_AS
        )
        scores = [scores_prev] + scores_AS
        relations_AS = self._create_relations(scores)

        if dir_prev is not None:
            digest = self.digest(natures)
            self.load_reducer(dir_prev, digest)
            self._adjust_params(self.reducer.n_neighbors, self.n_neighbors)
            self._adjust_params(self.reducer.min_dist, self.min_dist)

        for i in range(len(transposed_matrices_AS)):
            self.reducer.update(
                transposed_matrices_AS[i], relations=relations_AS[i]
            )

        return self._decompose_and_transpose_embeddings(
            [self.reducer.embeddings_[-1]], row_counts_AS
        )

    def _transpose_matrices(self, matrices_nD_AS):
        transposed_matrices_AS = []
        row_counts_AS = []

        for matrices in matrices_nD_AS:
            global_matrix, row_counts = hstack_transpose_matrices(
                matrices
            )
            transposed_matrices_AS.append(global_matrix)
            row_counts_AS.append(row_counts)

        return transposed_matrices_AS, row_counts_AS

    def _decompose_and_transpose_embeddings(self, embeddings, row_counts_AS):
        decomposed_embeddings = []

        for i in range(len(row_counts_AS)):
            row_counts = row_counts_AS[i]
            embedding = embeddings[i]
            matrices_i = decompose_transpose_matrix(embedding, row_counts)
            decomposed_embeddings.append(matrices_i)

        return decomposed_embeddings

    def _create_relations(self, scores_AS):
        concatenated_scores = []

        for scores in scores_AS:
            concatenated_scores.append(pd.concat(scores))

        relations = []

        for i in range(len(concatenated_scores) - 1):
            relation = make_relation(
                concatenated_scores[i], concatenated_scores[i+1]
            )

            relations.append(relation)

        return relations


class AlignedInterpolatedProjection(AlignedUMAPProjection):

    def load_execute(self, natures, dirs_mat, key_nD, dirs_nD, dirs_2D,
                     dir_reducer, current_version=None, prev_version=None,
                     dump=True, force=False, return_mat=False, dir_guided=None,
                     dir_guided_2D=None):

        if not force and dir_guided_2D is not None and (
                super().matrices_exist(natures, dir_guided_2D)
        ):
            logger.info(
                f"{self.key} matrices already exist, will not regenerate."
            )
            if return_mat:
                return super().load_matrices(natures, dir_guided_2D)
            else:
                return None

        matrices_nD_AS = self.load_matrices(natures, dirs_nD, key=key_nD)
        scores_AS = self.load_scores(natures, dirs_mat)

        return self.execute(matrices_nD_AS, scores_AS, dir_reducer,
                            dir_guided_2D, natures, dirs_2D,
                            current_version=current_version,
                            prev_version=prev_version, dump=dump)

    def _aligned_execute(self, matrices_nD_AS, scores_AS, dir_reducer,
                         dir_guided_2D, natures=None, dirs_2D=None,
                         current_version=None, prev_version=None, dump=False):
        # LSAE stands for last slice, all entities
        matrices_nD_LSAE = matrices_nD_AS[-1]

        # FE stands for "first entity"
        natures_FE = [natures[0]]
        exist = True
        if not self.matrices_exist(natures_FE, dirs_2D[-1]):
            exist = False
        if not exist:
            # ASFE stands for "all slices first entity"
            scores_ASFE = []
            # gets scores for first entity of each slice
            for i in range(len(scores_AS)):
                scores_ASFE.append([scores_AS[i][0]])

            matrices_nD_ASFE = []
            # get nD matrix for first entity of each slice
            for i in range(len(matrices_nD_AS)):
                matrices_nD_ASFE.append([matrices_nD_AS[i][0]])

            matrices_2D_ASFE = None
            digest = self.digest(natures)
            if prev_version is not None:
                # set previous version directory to load reducer from that
                # directory. For an update with existing
                # reducer prev_version should be set to None
                dir_prev = Path(str(dir_reducer).replace(current_version,
                                                         prev_version))
                scores_prev = super().load_scores(natures_FE, dir_prev, digest)

                matrices_2D_ASFE = self.update(
                    matrices_nD_ASFE, scores_ASFE, scores_prev, dir_prev,
                    natures=natures
                )
            else:
                matrices_2D_ASFE = self.fit_transform(
                    matrices_nD_ASFE, scores_ASFE
                )

            self.persist_reducer(self.reducer, dir_reducer, digest)
            if dump:
                self.dump_matrices(
                    natures_FE, matrices_2D_ASFE, dirs_2D
                )
                self.dump_scores(natures_FE, scores_AS[-1], dir_reducer,
                                 digest)

            matrices_2D_LSFE = matrices_2D_ASFE[-1]
        else:
            logger.info("matrices already exist, loading from existing")
            matrices_2D_LSFE = self.load_matrices(natures_FE, dirs_2D[-1])

        return matrices_nD_LSAE, matrices_2D_LSFE


class AlignedGuidedUMAPProjection(AlignedInterpolatedProjection):

    def __init__(self, key="aligned_guided_umap", n_neighbors=15, min_dist=0.1,
                 metric="euclidean", n_epochs=None, learning_rate=1.0,
                 init=INIT, low_memory=True, alignment_regularisation=1.0e-2,
                 alignment_window_size=3, random_state=None,
                 target_metric='categorical', verbose=False):

        super().__init__(
            n_neighbors=n_neighbors, min_dist=min_dist,
            metric=metric, n_epochs=n_epochs, learning_rate=learning_rate,
            init=init, low_memory=low_memory,
            alignment_regularisation=alignment_regularisation,
            alignment_window_size=alignment_window_size,
            random_state=random_state, target_metric=target_metric,
            verbose=verbose
        )
        self.key2 = key

    def execute(self, matrices_nD_AS, scores_AS, dir_reducer, dir_guided_2D,
                natures=None, dirs_2D=None, current_version=None,
                prev_version=None, dump=False):

        matrices_nD_LSAE, matrices_2D_LSFE = self._aligned_execute(
            matrices_nD_AS, scores_AS, dir_reducer, dir_guided_2D,
            natures=natures, dirs_2D=dirs_2D, current_version=current_version,
            prev_version=prev_version, dump=dump
        )
        init, _ = hstack_transpose_matrices(matrices_2D_LSFE)
        # -------------------------
        # use 2D projection array of last slice for first
        # entity as initial points
        projection_2D = GuidedUMAPProjection(
            n_neighbors=15, min_dist=0.1, init=init
        )

        # projection_2D.fit([matrices_nD_LSAE[0]], matrices_2D_LSFE)
        # do projection 2D for all nD matrices of the last slice
        matrices_2D = projection_2D.fit_transform(matrices_nD_LSAE)
        if dump:
            self.dump_matrices(natures, matrices_2D, dir_guided_2D,
                               key=self.key2)

        return matrices_2D


class AlignedKNeighborsProjection(AlignedInterpolatedProjection):

    def __init__(self, key="aligned_kneighbors", n_neighbors=15, min_dist=0.1,
                 metric="euclidean", n_epochs=None, learning_rate=1.0,
                 init=INIT, low_memory=True, alignment_regularisation=1.0e-2,
                 alignment_window_size=3, random_state=None,
                 target_metric='categorical', verbose=False):

        super().__init__(
            n_neighbors=n_neighbors, min_dist=min_dist,
            metric=metric, n_epochs=n_epochs, learning_rate=learning_rate,
            init=init, low_memory=low_memory,
            alignment_regularisation=alignment_regularisation,
            alignment_window_size=alignment_window_size,
            random_state=random_state, target_metric=target_metric,
            verbose=verbose
        )
        self.key2 = key

    def execute(self, matrices_nD_AS, scores_AS, dir_reducer, dir_guided_2D,
                natures=None, dirs_2D=None, current_version=None,
                prev_version=None, dump=False):

        matrices_nD_LSAE, matrices_2D_LSFE = self._aligned_execute(
            matrices_nD_AS, scores_AS, dir_reducer, dir_guided_2D,
            natures=natures, dirs_2D=dirs_2D, current_version=current_version,
            prev_version=prev_version, dump=dump
        )

        interpolator = KNeighborsInterpolator()
        interpolator.fit([matrices_nD_LSAE[0]], matrices_2D_LSFE)
        matrices_2D = interpolator.predict(matrices_nD_LSAE)

        if dump:
            self.dump_matrices(natures, matrices_2D, dir_guided_2D,
                               key=self.key2)

        return matrices_2D
