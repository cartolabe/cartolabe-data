from unittest import TestCase
from pandas.testing import assert_series_equal

import pandas as pd
from pathlib import Path

from cartodata.operations import (
    load_matrices_from_dumps, load_scores
)
from cartodata.pipeline.clustering import KMeansClustering


TOP_DIR = Path.cwd() / "cartodata" / "tests"
DATASET_DIR = TOP_DIR / "dumps"
SCORE_DIR = TOP_DIR / "scores"

NATURES = [
    'articles', 'authors', 'teams', 'labs', 'words'
]
WORDS_INDEX = 4

HL_INDEX = pd.Index([
        "social, language",
        "learning, physics",
        "computing, query",
        "networking internet architecture, quantitative methods",
        "interaction, cognitive science",
        "logic science, formal",
        "problem, mathematics",
        "optimization, evolutionary"
    ])

hl_cluster_series = pd.Series(
    index=HL_INDEX,
    data=[586, 518, 513, 474, 774, 416, 537, 444]
)

hl_cluster_labels = [
    'social', 'language', 'learning', 'physics', 'computing', 'query',
    'networking internet architecture', 'quantitative methods', 'interaction',
    'cognitive science', 'logic science', 'formal', 'problem', 'mathematics',
    'optimization', 'evolutionary'
]

hl_cluster_eval_pos = pd.Series(
    index=HL_INDEX,
    data=[0.500000, 0.814672, 0.555556, 0.337553,
          0.801034, 0.545673, 0.713222, 0.738739]
)

hl_cluster_eval_neg = pd.Series(
    index=HL_INDEX,
    data=[0.106922, 0.129771, 0.130913, 0.015143,
          0.072077, 0.049943, 0.243625, 0.123422]
)

ML_INDEX = pd.Index([
    'artificial intelligence, semantic',
    'matrix adaptation evolution strategy, multi armed',
    'mathematics, problem', 'design, visualizations',
    'logic science, proof', 'networking internet architecture, protocols',
    'learning', 'optimization, monte carlo search', 'sciences, biological',
    'systems, modeling simulation', 'movement, speech', 'physics',
    'testing, linear systems', 'visualization, image',
    'mobile robots, robots', 'interaction', 'query, quantum',
    'cluster computing, services', 'language', 'social, wikipedia', 'graph',
    'performance, programming languages', 'evolutionary, population',
    'structure, quantitative methods'],
                    dtype='object')

ml_cluster_labels = [
    'artificial intelligence', 'semantic',
    'matrix adaptation evolution strategy', 'multi armed', 'mathematics',
    'problem', 'design', 'visualizations', 'logic science', 'proof',
    'networking internet architecture', 'protocols', 'learning',
    'optimization', 'monte carlo search', 'sciences', 'biological', 'systems',
    'modeling simulation', 'movement', 'speech', 'physics', 'testing',
    'linear systems', 'visualization', 'image', 'mobile robots', 'robots',
    'interaction', 'query', 'quantum', 'cluster computing', 'services',
    'language', 'social', 'wikipedia', 'graph', 'performance',
    'programming languages', 'evolutionary', 'population', 'structure',
    'quantitative methods'
]

RANDOM_STATE = 42


class TestClustering(TestCase):
    def setUp(self):
        self.n_clusters = 5

        self.scores = load_scores(NATURES, DATASET_DIR)
        self.matrices = load_matrices_from_dumps(NATURES, "mat", DATASET_DIR)
        self.matrices_2D = load_matrices_from_dumps(NATURES, "umap",
                                                    DATASET_DIR)
        self.matrices_nD = load_matrices_from_dumps(NATURES, "lsa",
                                                    DATASET_DIR)

    def test_create_kmeans_clusters(self):
        nb_clusters = 8
        clustering = KMeansClustering(n=nb_clusters, base_factor=3,
                                      natures=["hl_clusters"],
                                      random_state=RANDOM_STATE)

        (c_nD, c_2D, c_scores, cluster_labels,
         cluster_eval_pos, cluster_eval_neg) = clustering.create_clusters(
             self.matrices,
             self.matrices_2D,
             self.matrices_nD,
             self.scores,
             WORDS_INDEX
             )

        cluster_label = cluster_labels[0]
        clus_cnt = cluster_label["nb_clusters"]
        c_lbs = cluster_label["c_lbs"]
        c_wscores = cluster_label["c_wscores"]
        initial_labels = cluster_label["initial_labels"]

        assert nb_clusters == clus_cnt
        assert_series_equal(c_scores[0], hl_cluster_series)
        assert_series_equal(cluster_eval_pos[0], hl_cluster_eval_pos,
                            check_exact=False, atol=1e-6)
        assert_series_equal(cluster_eval_neg[0], hl_cluster_eval_neg,
                            check_exact=False, atol=1e-6)
        assert cluster_eval_pos[0].index.equals(HL_INDEX)
        assert cluster_eval_neg[0].index.equals(HL_INDEX)
        assert len(c_lbs) == 4262
        assert c_lbs[0] == 5
        assert c_lbs[-1] == 0
        assert len(c_wscores) == 4832
        self.assertAlmostEqual(c_wscores[0], 0.00374591)
        self.assertAlmostEqual(c_wscores[-1], -0.00261712)
        assert initial_labels == []

        # 24 clusters
        nb_clusters = 24
        clustering = KMeansClustering(n=nb_clusters, base_factor=3,
                                      natures=["ml_clusters"], random_state=42)

        (c_nD, c_2D, c_scores, cluster_labels,
         cluster_eval_pos, cluster_eval_neg) = clustering.create_clusters(
             self.matrices,
             self.matrices_2D,
             self.matrices_nD,
             self.scores,
             WORDS_INDEX
             )

        cluster_label = cluster_labels[0]
        clus_cnt = cluster_label["nb_clusters"]
        c_lbs = cluster_label["c_lbs"]
        c_wscores = cluster_label["c_wscores"]
        initial_labels = cluster_label["initial_labels"]

        assert nb_clusters == clus_cnt
        assert c_scores[0].index.equals(ML_INDEX)
        assert cluster_eval_pos[0].index.equals(ML_INDEX)
        assert cluster_eval_neg[0].index.equals(ML_INDEX)
        assert len(c_lbs) == 4262
        assert c_lbs[0] == 4
        assert c_lbs[-1] == 0
        assert len(c_wscores) == 4832
        self.assertAlmostEqual(c_wscores[0], -0.00385078)
        self.assertAlmostEqual(c_wscores[-1], -0.00236855)
        assert initial_labels == []
