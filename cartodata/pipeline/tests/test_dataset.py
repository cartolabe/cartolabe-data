import pytest
from unittest.mock import patch
from unittest import TestCase

from pathlib import Path

import pandas as pd

from cartodata.pipeline.columns import Columns
from cartodata.pipeline.datasets import (
    Dataset, CSVDataset, SliceDataset, CSVSliceDataset
)

DATASET_NAME = "test"
VERSION = "1.0.0"
TOP_DIR = "dumps"
INPUT_DIR = "datas"


class TestDataset(TestCase):

    @patch("cartodata.pipeline.columns.IdentityColumn")
    @patch("cartodata.pipeline.columns.CSColumn")
    @patch("cartodata.pipeline.columns.CorpusColumn")
    def _init_columns(self, identity_column, cs_column, corpus_column):
        identity_column.type = Columns.IDENTITY
        identity_column.nature = "article"
        identity_column.load.return_value = None, None

        cs_column.type = Columns.CS
        cs_column.nature = "author"
        cs_column.load.return_value = None, None

        corpus_column.type = Columns.CORPUS
        corpus_column.nature = "word"
        corpus_column.load.return_value = None, None
        corpus_column.get_corpus.return_value = self.df[
            ["authFullName_s", "en_abstract_s", "en_title_s"]
        ].apply(lambda row: ' . '.join(row.values.astype(str)), axis=1)

        return identity_column, cs_column, corpus_column

    def setUp(self):

        self.df = pd.DataFrame(
            {
                "authFullName_s": [
                    ("Philippe Caillou,Samir Aknine,Suzanne Pinson, "
                     "Samuel Thiriot"),
                    "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                    "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                    "Zach Lewkovicz,Samuel Thiriot"
                ],
                "en_abstract_s": [
                    ("Many empirical studies emphasize the role of social "
                     "networks in job search. The social network implicated "
                     "in this process"),
                    ("In this short note, we investigate. Responsibility of "
                     "Pseudoknotted"),
                    ("Exactly Solvable Stochastic Processes for Traffic "
                     "Modelling"),
                    ("In the ground case, the procedure terminates and"
                     " provides a decision algorithm for the word problem.")
                ],
                "en_title_s": [
                    "Multi-prover verification of floating-point programs",
                    "Hardware-independent proofs of numerical programs",
                    "Viewing a World of Annotations through AnnoVIP",
                    "Combinatorial identification problems and graph powers"
                ]
            }
        )

        identity_column, cs_column, corpus_column = self._init_columns()
        self.dataset_no_df = Dataset(DATASET_NAME, VERSION, None, None,
                                     TOP_DIR, INPUT_DIR)

        self.dataset_no_column = Dataset(DATASET_NAME, VERSION, self.df,
                                         None, TOP_DIR, INPUT_DIR)
        self.dataset_id_column = Dataset(DATASET_NAME, VERSION, self.df,
                                         [identity_column], TOP_DIR, INPUT_DIR)
        self.dataset_all_column = Dataset(
            DATASET_NAME, VERSION, self.df,
            [identity_column, cs_column, corpus_column],
            TOP_DIR, INPUT_DIR
        )

    @patch("cartodata.pipeline.columns.CSColumn")
    def test_const(self, cs_column):
        """Tests Dataset constructor for valid values."""

        dataset = self.dataset_no_df
        assert dataset.key == "mat"
        assert dataset.name == DATASET_NAME
        assert dataset.df is None
        assert dataset.version == VERSION
        assert dataset.columns == []
        assert dataset.natures == []
        assert dataset.main_index == -1
        assert dataset.corpus_index == -1
        assert dataset.corpus is None
        assert dataset.working_dir == (
            Path(TOP_DIR) / DATASET_NAME / VERSION
        )

        dataset = self.dataset_no_column

        assert dataset.key == "mat"
        assert dataset.name == DATASET_NAME
        assert dataset.df.equals(self.df)
        assert dataset.version == VERSION
        assert dataset.columns == []
        assert dataset.natures == []
        assert dataset.main_index == -1
        assert dataset.corpus_index == -1
        assert dataset.corpus is None
        assert dataset.working_dir == (
            Path(TOP_DIR) / DATASET_NAME / VERSION
        )

        dataset = self.dataset_id_column

        assert dataset.key == "mat"
        assert dataset.name == DATASET_NAME
        assert dataset.df.equals(self.df)
        assert dataset.version == VERSION
        assert len(dataset.columns) == 1
        assert dataset.main_index == 0
        assert dataset.corpus_index == -1
        assert dataset.corpus is None
        assert dataset.working_dir == (
            Path(TOP_DIR) / DATASET_NAME / VERSION
        )

        cols = dataset.columns
        cols.append(cs_column)

        assert len(dataset.columns) == 1

        dataset = self.dataset_all_column

        assert dataset.key == "mat"
        assert dataset.name == DATASET_NAME
        assert dataset.df.equals(self.df)
        assert dataset.version == VERSION
        assert len(dataset.columns) == 3
        assert dataset.main_index == 0
        assert dataset.corpus_index == 2
        assert dataset.corpus is not None
        assert dataset.working_dir == (
            Path(TOP_DIR) / DATASET_NAME / VERSION
        )

    def test_const_invalid(self):
        """Tests Dataset constructor for invalid values."""

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset name!"):
            Dataset("", VERSION, self.df, None, TOP_DIR, INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset name!"):
            Dataset(None, VERSION, self.df, None, TOP_DIR, INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset version!"):
            Dataset(DATASET_NAME, None, self.df, None, TOP_DIR, INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset version!"):
            Dataset(DATASET_NAME, "", self.df, None, TOP_DIR, INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid Dataframe!"):
            Dataset(DATASET_NAME, VERSION, pd.DataFrame({}), None, TOP_DIR,
                    INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid top directory!"):
            Dataset(DATASET_NAME, VERSION, self.df, None, None, INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid top directory!"):
            Dataset(DATASET_NAME, VERSION, self.df, None, "", INPUT_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify a valid input directory!"):
            Dataset(DATASET_NAME, VERSION, self.df, None, TOP_DIR, None)

        with pytest.raises(AssertionError,
                           match="Please specify a valid input directory!"):
            Dataset(DATASET_NAME, VERSION, self.df, None, TOP_DIR, "")

    def test_df(self):
        """Tests Dataset.df property."""

        assert self.dataset_no_column.df.shape == (4, 3)

    @patch("cartodata.pipeline.columns.CSColumn")
    def test_columns(self, cs_column):
        """Tests Dataset.columns property."""

        assert self.dataset_no_column.columns == []

        assert len(self.dataset_id_column.columns) == 1

        # this should return a shallow copy
        columns = self.dataset_id_column.columns
        columns.append(cs_column)
        assert len(self.dataset_id_column.columns) == 1

    @patch("cartodata.pipeline.columns.IdentityColumn")
    @patch("cartodata.pipeline.columns.CSColumn")
    @patch("cartodata.pipeline.columns.CorpusColumn")
    def test_set_columns(self, identity_column, cs_column,
                         corpus_column):
        """Tests Dataset.set_columns method."""

        dataset = self.dataset_no_column
        assert dataset.columns == []

        identity_column.type = Columns.IDENTITY
        corpus_column.type = Columns.CORPUS

        dataset.set_columns([identity_column, cs_column])

        assert len(dataset.columns) == 2
        assert len(dataset.natures) == 2
        assert dataset.main_index == 0
        assert dataset.corpus_index == -1

        # set to empty list
        dataset.set_columns([])
        assert len(dataset.columns) == 0
        assert len(dataset.natures) == 0
        assert dataset.corpus_index == -1
        assert dataset.main_index == -1

        dataset.set_columns(None)
        assert len(dataset.columns) == 0
        assert len(dataset.natures) == 0
        assert dataset.corpus_index == -1
        assert dataset.main_index == -1

        dataset.set_columns([identity_column, cs_column,
                             corpus_column])
        assert len(dataset.columns) == 3
        assert len(dataset.natures) == 3
        assert dataset.main_index == 0
        assert dataset.corpus_index == 2

    @patch("cartodata.pipeline.columns.IdentityColumn")
    def test_set_columns_error(self, identity_column):
        """Tests Dataset.set_columns method for error."""

        with pytest.raises(AssertionError,
                           match="Please specify a list for columns."
                           ):
            self.dataset_no_column.set_columns(identity_column)

    def test_natures(self):
        """Tests Dataset.natures property."""

        assert self.dataset_no_column.natures == []

        assert self.dataset_all_column.natures == ["article", "author", "word"]

    def test_create_matrices(self):
        """Tests Dataset.create_matrices_and_scores method."""

        matrices, scores = self.dataset_all_column.create_matrices_and_scores()

        assert len(matrices) == 3
        assert len(scores) == 3

    def test_create_matrices_error(self):
        """Tests Dataset.create_matrices_and_scores method for error."""

        with pytest.raises(AssertionError,
                           match="There are no columns defined for the "
                           "dataset. Please set columns and retry!"
                           ):
            self.dataset_no_column.create_matrices_and_scores()


class TestCSVDataset(TestDataset):

    def setUp(self):

        self.df = pd.read_csv("./datas/lri_2000_2019.csv")

        identity_column, cs_column, corpus_column = self._init_columns()

        self.dataset_no_column = CSVDataset(
            DATASET_NAME, VERSION, filename="lri_2000_2019.csv",
            top_dir=TOP_DIR, input_dir=INPUT_DIR
        )
        self.dataset_id_column = CSVDataset(
            DATASET_NAME, VERSION, filename="lri_2000_2019.csv",
            columns=[identity_column], top_dir=TOP_DIR,
            input_dir=INPUT_DIR
        )
        self.dataset_all_column = CSVDataset(
            DATASET_NAME, VERSION, filename="lri_2000_2019.csv",
            columns=[identity_column, cs_column, corpus_column],
            top_dir=TOP_DIR, input_dir=INPUT_DIR
        )

    def test_const(self):
        """Tests CSVDataset constructor for valid values."""

        dataset = self.dataset_no_column

        assert dataset.name == DATASET_NAME
        assert dataset.version == VERSION
        assert dataset.filename == "lri_2000_2019.csv"
        assert dataset.fileurl is None
        assert dataset.working_dir == Path(
            f"{TOP_DIR}/{DATASET_NAME}/{VERSION}")
        assert dataset.working_dir.exists()
        assert dataset.input_dir == Path(INPUT_DIR).absolute()
        assert dataset.columns == []
        assert dataset.natures == []
        assert dataset.kwargs == {}

        assert self.dataset_all_column.version == VERSION

    def test_const_invalid(self):
        """Tests CSVDataset constructor for invalid values."""

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset name."):
            CSVDataset("", VERSION, top_dir=TOP_DIR,
                       filename="lri_2000_2019.csv")

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset name."):
            CSVDataset(None, VERSION, top_dir=TOP_DIR,
                       filename="lri_2000_2019.csv")

        with pytest.raises(AssertionError,
                           match=("Either filename or fileurl should be "
                                  "specified!")
                           ):
            CSVDataset(DATASET_NAME, VERSION, top_dir=TOP_DIR)

        with pytest.raises(AssertionError,
                           match="Please specify filename for the *"):
            CSVDataset(DATASET_NAME, VERSION, top_dir=TOP_DIR,
                       fileurl="https://")

        dataset = CSVDataset(DATASET_NAME, VERSION, top_dir=TOP_DIR,
                             filename="filename")

        with pytest.raises(AssertionError):
            dataset.df

    def test_df(self):
        """Tests CSVDataset.df property."""

        assert self.dataset_no_column.df.shape == (2988, 11)


class TestSliceDataset(TestDataset):

    def setUp(self):

        self.df = pd.DataFrame(
            {
                "authFullName_s": [
                    ("Philippe Caillou,Samir Aknine,Suzanne Pinson, "
                     "Samuel Thiriot"),
                    "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                    "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                    "Zach Lewkovicz,Samuel Thiriot"
                ],
                "en_abstract_s": [
                    ("Many empirical studies emphasize the role of social "
                     "networks in job search. The social network implicated "
                     "in this process"),
                    ("In this short note, we investigate. Responsibility of "
                     "Pseudoknotted"),
                    ("Exactly Solvable Stochastic Processes for Traffic "
                     "Modelling"),
                    ("In the ground case, the procedure terminates and"
                     " provides a decision algorithm for the word problem.")
                ],
                "en_title_s": [
                    "Multi-prover verification of floating-point programs",
                    "Hardware-independent proofs of numerical programs",
                    "Viewing a World of Annotations through AnnoVIP",
                    "Combinatorial identification problems and graph powers"
                ]
            }
        )

        identity_column, cs_column, corpus_column = self._init_columns()

        self.df1 = self.df.sample(2)

        self.dataset_no_column = SliceDataset(DATASET_NAME, VERSION,
                                              df=self.df)
        self.dataset_id_column = SliceDataset(DATASET_NAME, VERSION,
                                              df=self.df,
                                              columns=[identity_column])
        self.dataset_all_column = SliceDataset(
            DATASET_NAME, df=self.df, dfs=[self.df1, self.df],
            columns=[identity_column, cs_column, corpus_column],
            version="1.0.0"
        )

    def test_const(self):
        """Tests SliceDataset constructor for valid values."""

        dataset = self.dataset_no_column

        assert dataset.key == "mat"
        assert dataset.name == DATASET_NAME
        assert dataset.df.equals(self.df.head(2))
        assert dataset.slice_count == 2
        assert dataset.overlap == 0
        assert dataset.slice_type == "cumulative"
        assert dataset.version == VERSION
        assert dataset.columns == []
        assert dataset.natures == []
        assert dataset.main_index == -1
        assert dataset.corpus_index == -1
        assert dataset.corpus is None

        assert self.dataset_all_column.version == "1.0.0"
        assert self.dataset_all_column.df.equals(self.df1)

    def test_const_error(self):
        """Tests SliceDataset constructor for invalid values."""

        with pytest.raises(AssertionError,
                           match=("Please specify a slice_count greater than "
                                  "0!")
                           ):
            SliceDataset(DATASET_NAME, VERSION, df=self.df, slice_count=0)

        with pytest.raises(AssertionError,
                           match="Please specify a valid slice_type!"):
            SliceDataset(DATASET_NAME, VERSION, df=self.df, slice_type="")

        with pytest.raises(AssertionError,
                           match="Overlap value should be greater*"):
            SliceDataset(DATASET_NAME, VERSION, df=self.df, overlap=-1)

    def test_df(self):
        """Tests SliceDataset.df property."""

        assert self.dataset_no_column.df.equals(self.df.head(2))
        assert self.dataset_all_column.df.equals(self.df1)

        self.dataset_all_column.index = 1
        assert self.dataset_all_column.df.equals(self.df)

    def test_slice_count(self):
        """Tests SliceDataset.slice_count property."""

        assert self.dataset_no_column.slice_count == 2
        assert self.dataset_all_column.slice_count == 2

    def test_index(self):
        """Tests SliceDataset.index property."""

        self.dataset_all_column.index = 1
        assert self.dataset_all_column.df.equals(self.df)

    def test_index_error(self):
        """Tests SliceDataset.index property for invalid values."""

        with pytest.raises(AssertionError,
                           match="Invalid index value*"):
            self.dataset_no_column.index = 2

    def test_create_slices(self):
        """Tests SliceDataset.create_slices method."""

        dataset = self.dataset_all_column
        assert dataset.slice_count == 2
        dataset._load()

        dataset.create_slices(3, slice_type="repetitive")

        assert dataset.slice_count == 3
        for i in range(3):
            dataset.index = i
            assert dataset.df.equals(self.df)

        dataset.create_slices(4, slice_type="discrete")
        assert dataset.slice_count == 4

        slice_size = len(self.df) // 4

        for i in range(4):
            dataset.index = i
            assert dataset.df.shape[0] == slice_size

        dataset.create_slices(4, slice_type="cumulative")
        assert dataset.slice_count == 4

        for i in range(4):
            dataset.index = i
            assert dataset.df.shape[0] == (i * slice_size + slice_size)

    def test_create_slices_discrete_overlap(self):
        """Tests SliceDataset.create_slices method with discrete slice_type and
        overlap value != 0."""

        dataset = self.dataset_all_column
        dataset._load()

        slice_count = 2
        dataset.create_slices(slice_count, slice_type="discrete", overlap=1)
        assert dataset.slice_count == slice_count

        for i in range(slice_count):
            dataset.index = i
            assert dataset.df.shape[0] == 3


class TestCSVSliceDataset(TestSliceDataset):

    def setUp(self):

        self.df = pd.read_csv("./datas/lri_2000_2019.csv")

        identity_column, cs_column, corpus_column = self._init_columns()

        self.dataset_no_column = CSVSliceDataset(
            DATASET_NAME, VERSION, input_dir="datas",
            filename="lri_2000_2019.csv"
        )
        self.dataset_id_column = CSVSliceDataset(
            DATASET_NAME, VERSION, input_dir="datas",
            filename="lri_2000_2019.csv", columns=[identity_column]
        )
        self.dataset_all_column = CSVSliceDataset(
            DATASET_NAME, VERSION, input_dir="datas",
            filename="lri_2000_2019.csv",
            columns=[identity_column, cs_column, corpus_column]
        )

    def test_const(self):
        """Tests CSVSliceDataset constructor for valid values."""

        dataset = self.dataset_no_column

        assert dataset.key == "mat"
        assert dataset.name == DATASET_NAME
        assert dataset.df.equals(self.df.head(len(self.df) // 2))
        assert dataset.version == VERSION
        assert dataset.columns == []
        assert dataset.natures == []
        assert dataset.main_index == -1
        assert dataset.corpus_index == -1
        assert dataset.corpus is None
        assert dataset.slice_count == 2

    def test_df(self):
        """Tests CSVSliceDataset.df property."""

        self.dataset_all_column.index = 1
        assert self.dataset_all_column.df.equals(self.df)

    def test_create_slices_discrete_overlap(self):
        """Tests SliceDataset.create_slices method with discrete slice_type and
        overlap value != 0."""

        dataset = self.dataset_all_column
        dataset._load()

        slice_count = 3
        dataset.create_slices(slice_count, slice_type="discrete", overlap=50)
        assert dataset.slice_count == slice_count

        dataset.index = 0
        assert dataset.df.shape[0] == 1046

        dataset.index = 1
        assert dataset.df.shape[0] == 1096

        dataset.index = 2
        assert dataset.df.shape[0] == 1046

        slice_count = 5
        dataset.create_slices(slice_count, slice_type="discrete", overlap=50)
        assert dataset.slice_count == slice_count

        dataset.index = 0
        assert dataset.df.shape[0] == 647

        dataset.index = 1
        assert dataset.df.shape[0] == 697

        dataset.index = 2
        assert dataset.df.shape[0] == 697

        dataset.index = 3
        assert dataset.df.shape[0] == 697

        dataset.index = 4
        assert dataset.df.shape[0] == 650
