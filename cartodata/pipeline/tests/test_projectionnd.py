import pytest
from unittest import TestCase
from unittest.mock import patch

import pandas as pd

from cartodata.loading import (
    load_comma_separated_column, load_text_column, load_identity_column
)
from cartodata.pipeline.projectionnd import (
    LSAProjection, LDAProjection, Doc2VecProjection
)
from cartodata.tests.conftest import DATASET_DIR


class TestProjectionND(TestCase):

    def setUp(self):

        self.df = pd.DataFrame(
            {
                "auth": [
                    ("Philippe Caillou,Samir Aknine,Suzanne Pinson, "
                     "Samuel Thiriot"),
                    "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                    "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                    "Zach Lewkovicz,Samuel Thiriot"
                ],
                "text": [
                    ("Many empirical studies emphasize the role of social "
                     "networks in job search. The social network implicated "
                     "in this process"),
                    ("In this short note, we investigate. Responsibility of "
                     "Pseudoknotted"),
                    ("Exactly Solvable Stochastic Processes for Traffic "
                     "Modelling"),
                    ("In the ground case, the procedure terminates and"
                     " provides a decision algorithm for the word problem.")
                ],
                "title": [
                    "Multi-prover verification of floating-point programs",
                    "Hardware-independent proofs of numerical programs",
                    "Viewing a World of Annotations through AnnoVIP",
                    "Combinatorial identification problems and graph powers"
                ]
            }
        )

        self.authors_tab, self.authors_scores = load_comma_separated_column(
            self.df, "auth")

        self.words_tab, self.words_scores = load_text_column(
            self.df['text'], 4, 1, 1)

        self.articles_tab, self.articles_scores = load_identity_column(
            self.df, "title")


class TestProjectionLSA(TestProjectionND):

    def setUp(self):
        super().setUp()

        self.proj = LSAProjection(2, True)

    def test_cons(self):
        """Tests LSAProjection constructor."""
        assert self.proj.key == "lsa"
        assert self.proj.num_dims == 2
        assert self.proj.normalize

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_execute(self, dataset):
        """Tests LSAProjection.execute method."""

        matrices = [self.articles_tab, self.authors_tab, self.words_tab]

        dataset.corpus_index = 2
        dataset.natures = ["articles", "authors", "words"]
        projected = self.proj.execute(
            matrices, dataset, dir_nD="dumps"
        )

        self.assertEqual(len(projected), 3)
        self.assertEqual(projected[0].shape,
                         (2, len(self.articles_scores)))
        self.assertEqual(projected[1].shape,
                         (2, len(self.authors_scores)))
        self.assertEqual(projected[2].shape, (2, len(self.words_scores)))

    def test_matrices_exist(self):
        natures = ["articles", "authors", "labs", "teams"]
        assert self.proj.matrices_exist(natures, DATASET_DIR)

        natures_non_existing = natures + ["not_existing"]
        assert not self.proj.matrices_exist(natures_non_existing, DATASET_DIR)

        assert not self.proj.matrices_exist(natures, "trial")

    def test_matrices_exist_error(self):
        natures = ["articles", "authors", "labs", "teams"]

        with pytest.raises(ValueError):
            self.proj.matrices_exist(None, DATASET_DIR)

        with pytest.raises(ValueError):
            self.proj.matrices_exist([], DATASET_DIR)

        with pytest.raises(TypeError):
            self.proj.matrices_exist(natures, None)

    def test_load_matrices(self):
        natures = ["articles", "authors", "labs", "teams"]
        matrices = self.proj.load_matrices(natures, DATASET_DIR)

        assert len(natures) == len(matrices)

    def test_load_matrices_error(self):
        natures = ["articles", "authors", "labs", "teams"]

        with pytest.raises(ValueError):
            self.proj.load_matrices(None, DATASET_DIR)

        with pytest.raises(ValueError):
            self.proj.load_matrices([], DATASET_DIR)

        with pytest.raises(FileNotFoundError):
            self.proj.load_matrices(natures, "trial")

    def test_dump_matrices(self):
        pass


class TestProjectionLDA(TestProjectionND):

    def setUp(self):
        super().setUp()

        self.proj = LDAProjection(2)

    def test_cons(self):
        """Tests LDAProjection constructor."""

        assert self.proj.key == "lda"
        assert self.proj.num_dims == 2
        assert self.proj.normalize
        assert self.proj.update_every == 0
        assert self.proj.passes == 20

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_execute(self, dataset):
        """Tests Doc2VecProjection.execute method."""

        matrices = [self.articles_tab, self.authors_tab, self.words_tab]
        dataset.corpus_index = 2
        dataset.natures = ["articles", "authors", "words"]

        projected = self.proj.execute(matrices, dataset, "dumps")

        self.assertEqual(len(projected), 3)
        self.assertEqual(projected[0].shape,
                         (2, len(self.articles_scores)))
        self.assertEqual(projected[1].shape,
                         (2, len(self.authors_scores)))
        self.assertEqual(projected[2].shape, (2, len(self.words_scores)))


class TestProjectionDoc2Vec(TestProjectionND):

    def setUp(self):
        super().setUp()

        self.proj = Doc2VecProjection(2)

    def test_cons(self):
        """Tests Doc2VecProjection constructor."""
        assert self.proj.key == "doc2vec"
        assert self.proj.num_dims == 2
        assert self.proj.normalize
        assert self.proj.workers == 3
        assert self.proj.epochs == 5
        assert self.proj.batch_words == 10000
        assert self.proj.trim_rule is None
        assert self.proj.alpha == 0.025
        assert self.proj.window == 5
        assert self.proj.seed == 1
        assert self.proj.hs == 0
        assert self.proj.negative == 5
        assert self.proj.ns_exponent == 0.75
        assert self.proj.cbow_mean == 1
        assert self.proj.min_alpha == 0.0001
        assert not self.proj.compute_loss
        assert self.proj.dm_mean is None
        assert self.proj.dm == 1
        assert self.proj.dbow_words == 0
        assert self.proj.dm_concat == 0
        assert self.proj.dm_tag_count == 1

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_execute(self, dataset):
        """Tests Doc2VecProjection.execute method."""

        matrices = [self.articles_tab, self.authors_tab, self.words_tab]

        dataset.natures = ["articles", "authors", "words"]
        dataset.corpus = self.df[["auth", "text", "title"]].apply(
            lambda row: ' . '.join(row.values.astype(str)), axis=1)
        dataset.corpus_index = 2

        projected_doc2vec = self.proj.execute(matrices, dataset, "dumps")

        self.assertEqual(len(projected_doc2vec), 3)
        self.assertEqual(projected_doc2vec[0].shape,
                         (2, len(self.articles_scores)))
        self.assertEqual(projected_doc2vec[1].shape,
                         (2, len(self.authors_scores)))
        self.assertEqual(projected_doc2vec[2].shape,
                         (2, len(self.words_scores)))
