import pandas as pd
from pathlib import Path
from unittest import TestCase

from cartodata.loading import (
    load_identity_column, load_comma_separated_column, load_text_column
)

from cartodata.pipeline.projection2d import (
    UMAPProjection, IndirectUMAPProjection, GuidedUMAPProjection,
    TSNEProjection, AlignedUMAPProjection, AlignedGuidedUMAPProjection,
    AlignedKNeighborsProjection
)


class BaseProjection():

    def setUp(self):
        df = pd.DataFrame(
            {"authFullName_s": [
                "Philippe Caillou,Samir Aknine,Suzanne Pinson, Samuel Thiriot",
                "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                "Zach Lewkovicz,Samuel Thiriot"
            ]
            }
        )

        self.authors_tab, self.authors_scores = load_comma_separated_column(
            df, "authFullName_s")

        df = pd.DataFrame(
            {"text": [
                ("Many empirical studies emphasize the role of social "
                 "networks in job search. The social network implicated "
                 "in this process"),
                ("In this short note, we investigate. Responsibility of "
                 "Pseudoknotted"),
                "Exactly Solvable Stochastic Processes for Traffic Modelling",
                ("In the ground case, the procedure terminates and provides a "
                 "decision algorithm for the word problem.")]})

        self.df = df
        self.words_tab, self.words_scores = load_text_column(
            df['text'], 4, 1, 1)

        df = pd.DataFrame(
            {"en_title_s": [
                "Multi-prover verification of floating-point programs",
                "Hardware-independent proofs of numerical programs",
                "Viewing a World of Annotations through AnnoVIP",
                "Combinatorial identification problems and graph powers"]
             }
        )

        self.articles_tab, self.articles_scores = load_identity_column(
            df, "en_title_s")

        self.natures = ["articles", "authors", "words"]
        self.matrices = [self.articles_tab.toarray(),
                         self.authors_tab.toarray(),
                         self.words_tab.toarray()]

    def test_execute(self):
        projected_matrices = self.projection.execute(
            self.matrices, natures=self.natures
        )

        self.assertEqual(len(projected_matrices), 3)
        self.assertEqual(
            projected_matrices[0].shape, (2, len(self.articles_scores)))
        self.assertEqual(
            projected_matrices[1].shape, (2, len(self.authors_scores)))
        self.assertEqual(
            projected_matrices[2].shape, (2, len(self.words_scores)))


class TestUMAPProjection(BaseProjection, TestCase):

    def setUp(self):
        super().setUp()
        self.projection = UMAPProjection(random_state=42)


class TestIndirectUMAPProjection(BaseProjection, TestCase):

    def setUp(self):
        super().setUp()
        self.projection = IndirectUMAPProjection(random_state=42)


class TestGuidedUMAPProjection(BaseProjection, TestCase):

    def setUp(self):
        super().setUp()
        self.projection = GuidedUMAPProjection(random_state=42)


class TestTSNEProjection(BaseProjection, TestCase):

    def setUp(self):
        super().setUp()
        self.projection = TSNEProjection(random_state=42)


class BaseAlignedProjection():

    def setUp(self, i):
        df = pd.DataFrame(
            {"authFullName_s": [
                "Philippe Caillou,Samir Aknine,Suzanne Pinson, Samuel Thiriot",
                "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                "Zach Lewkovicz,Samuel Thiriot",
                "Hélène Paugam-Moisy,Samuel Thiriot"
            ]
            }
        )

        self.authors_tab1, self.authors_scores1 = load_comma_separated_column(
            df.loc[:i], "authFullName_s")
        self.authors_tab2, self.authors_scores2 = load_comma_separated_column(
            df, "authFullName_s")

        df = pd.DataFrame(
            {"text": [
                ("Many empirical studies emphasize the role of social "
                 "networks in job search. The social network implicated "
                 "in this process"),
                ("In this short note, we investigate. Responsibility of "
                 "Pseudoknotted"),
                ("AD algorithms score the data instances and produce a "
                 "ranked list of candidate anomalies."),
                "Exactly Solvable Stochastic Processes for Traffic Modelling",
                ("In the ground case, the procedure terminates and provides a "
                 "decision algorithm for the word problem.")]})

        self.df = df
        self.words_tab1, self.words_scores1 = load_text_column(
            df.loc[:i]['text'], 4, 1, 1)
        self.words_tab2, self.words_scores2 = load_text_column(
            df['text'], 4, 1, 1)

        df = pd.DataFrame(
            {"en_title_s": [
                "Multi-prover verification of floating-point programs",
                "Hardware-independent proofs of numerical programs",
                "Viewing a World of Annotations through AnnoVIP",
                "Effectiveness of Tree-based Ensembles for Anomaly "
                "Discovery: Insights, Batch and Streaming Active Learning",
                "Combinatorial identification problems and graph powers"]
             }
        )

        self.articles_tab1, self.articles_scores1 = load_identity_column(
            df.loc[:i], "en_title_s")
        self.articles_tab2, self.articles_scores2 = load_identity_column(
            df, "en_title_s")

        self.natures = ["articles", "authors", "words"]
        self.matrices_all = [[self.articles_tab1.toarray(),
                              self.authors_tab1.toarray(),
                              self.words_tab1.toarray()],
                             [self.articles_tab2.toarray(),
                              self.authors_tab2.toarray(),
                              self.words_tab2.toarray()]]
        self.scores_all = [[self.articles_scores1,
                            self.authors_scores1,
                            self.words_scores1],
                           [self.articles_scores2,
                            self.authors_scores2,
                            self.words_scores2]]


class TestAlignedUmapProjection(BaseAlignedProjection, TestCase):

    def setUp(self):
        super().setUp(1)
        self.projection = AlignedUMAPProjection(random_state=42)

    def test_execute(self):
        tmp_dir = Path(__file__).parent
        dirs_2D = [tmp_dir, tmp_dir]
        projected_matrices_all, scores = self.projection.execute(
            self.matrices_all, self.scores_all, dir_reducer=tmp_dir,
            dirs_2D=dirs_2D, natures=self.natures
        )

        self.assertEqual(len(projected_matrices_all), 2)

        projected_matrices1 = projected_matrices_all[0]
        self.assertEqual(len(projected_matrices1), 3)
        self.assertEqual(
            projected_matrices1[0].shape, (2, len(self.articles_scores1)))
        self.assertEqual(
            projected_matrices1[1].shape, (2, len(self.authors_scores1)))
        self.assertEqual(
            projected_matrices1[2].shape, (2, len(self.words_scores1)))

        projected_matrices2 = projected_matrices_all[1]
        self.assertEqual(len(projected_matrices2), 3)
        self.assertEqual(
            projected_matrices2[0].shape, (2, len(self.articles_scores2)))
        self.assertEqual(
            projected_matrices2[1].shape, (2, len(self.authors_scores2)))
        self.assertEqual(
            projected_matrices2[2].shape, (2, len(self.words_scores2)))

        self.assertEqual(len(scores), 3)
        self.assertEqual(len(scores[0]), len(self.articles_scores2))
        self.assertEqual(len(scores[1]), len(self.authors_scores2))
        self.assertEqual(len(scores[2]), len(self.words_scores2))


class BaseAlignedInterpolatedProjection(BaseAlignedProjection):

    def setUp(self):
        super().setUp(4)

    def test_execute(self):
        tmp_dir = Path(__file__).parent
        dirs_2D = [tmp_dir, tmp_dir]
        projected_matrices = self.projection.execute(
            self.matrices_all, self.scores_all, dir_reducer=tmp_dir,
            dir_guided_2D=tmp_dir, natures=self.natures, dirs_2D=dirs_2D
        )

        self.assertEqual(len(projected_matrices), 3)
        self.assertEqual(
            projected_matrices[0].shape, (2, len(self.articles_scores2)))
        self.assertEqual(
            projected_matrices[1].shape, (2, len(self.authors_scores2)))
        self.assertEqual(
            projected_matrices[2].shape, (2, len(self.words_scores2)))


class TestAlignedGuidedUMAPProjection(BaseAlignedInterpolatedProjection,
                                      TestCase):

    def setUp(self):
        super().setUp()
        self.projection = AlignedGuidedUMAPProjection(random_state=42)


class TestAlignedKNeighborsProjection(BaseAlignedInterpolatedProjection,
                                      TestCase):

    def setUp(self):
        super().setUp()
        self.projection = AlignedKNeighborsProjection(random_state=42)
