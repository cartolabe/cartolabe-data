from unittest import TestCase

from cartodata.phases import (
    PhaseProjectionND, PhaseProjection2D, PhaseClustering
)
from cartodata.pipeline.projectionnd import BertProjection
from cartodata.pipeline.projection2d import TSNEProjection
from cartodata.pipeline.clustering import KMeansClustering


class TestPhaseProjectionND(TestCase):

    def test_all(self):

        run_params = {"n_neighbors": 10}

        assert PhaseProjectionND.name() == (
            f"{PhaseProjectionND.INDEX}_{PhaseProjectionND.SHORT}"
        )
        assert PhaseProjectionND.long_name() == (
             f"{PhaseProjectionND.INDEX}_{PhaseProjectionND.NAME}"
        )
        assert PhaseProjectionND.get_key({}) == "lsa"
        assert PhaseProjectionND.prefix(run_params) == {
            f"{PhaseProjectionND.name()}__n_neighbors": 10
        }

        next_params = {
            "projection_nD": {"key": "bert", "family": "all-MiniLM-L6-v2"}
            }
        assert PhaseProjectionND.get_key(
            next_params[PhaseProjectionND.NAME]
        ) == "bert"

        executor = PhaseProjectionND.get_executor(next_params)
        assert type(executor) == BertProjection
        assert executor.family == "all-MiniLM-L6-v2"


class TestPhaseProjection2D(TestCase):

    def test_all(self):

        next_params = {
            "projection_2D": {"key": "tsne", "perplexity": 50}
        }

        assert PhaseProjection2D.name() == (
            f"{PhaseProjection2D.INDEX}_{PhaseProjection2D.SHORT}"
            )
        assert PhaseProjection2D.long_name() == (
             f"{PhaseProjection2D.INDEX}_{PhaseProjection2D.NAME}"
            )
        assert PhaseProjection2D.get_key(
            next_params[PhaseProjection2D.NAME]
        ) == "tsne"
        assert PhaseProjection2D.get_key({}) == "umap"

        executor = PhaseProjection2D.get_executor(next_params)

        assert type(executor) == TSNEProjection
        assert executor.reducer.perplexity == 50

        run_params = {"perplexity": 50}
        assert PhaseProjection2D.prefix(run_params) == {
            f"{PhaseProjection2D.name()}__perplexity": 50
        }


class TestPhaseClustering(TestCase):

    def test_all(self):

        next_params = {
            "clustering": {"key": "kmeans", "base_factor": 3}
        }

        assert PhaseClustering.name() == (
            f"{PhaseClustering.INDEX}_{PhaseClustering.SHORT}"
        )
        assert PhaseClustering.long_name() == (
             f"{PhaseClustering.INDEX}_{PhaseClustering.NAME}"
        )
        assert PhaseClustering.get_key(
            next_params[PhaseClustering.NAME]
        ) == "kmeans"
        assert PhaseClustering.get_key({}) == "hdbscan"

        executor = PhaseClustering.get_executor(next_params)

        assert type(executor) == KMeansClustering
        assert executor.base_factor == 3
