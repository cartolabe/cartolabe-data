import json
import os

import pandas as pd
import numpy as np
from pyfakefs import fake_filesystem_unittest
from ..loading import load_comma_separated_column
from ..operations import filter_min_score, export_to_json, create_elements_list


class TestOperations(fake_filesystem_unittest.TestCase):

    def setUp(self):
        self.setUpPyfakefs()

    def test_filter_min_score(self):
        df = pd.DataFrame(
            {"authFullName_s": [
                "Philippe Caillou,Samir Aknine,Suzanne Pinson, Samuel Thiriot",
                "Bruno Cessac,Hélène Paugam-Moisy,Thierry Viéville",
                "Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant",
                "Zach Lewkovicz,Samuel Thiriot"
            ]
            }
        )

        authors_tab, authors_scores = load_comma_separated_column(
            df, "authFullName_s")
        filtered_authors, filtered_authors_scores = filter_min_score(
            authors_tab, authors_scores, 1)

        np.testing.assert_equal(filtered_authors_scores.values, [2, 2, 2])
        self.assertTrue(
            (filtered_authors_scores.index == [
                "Philippe Caillou", "Samuel Thiriot", "Zach Lewkovicz"
            ]).all()
        )
        np.testing.assert_allclose(filtered_authors.A,
                                   [[1, 1, 0], [0, 0, 0],
                                    [1, 0, 1], [0, 1, 1]])

    def test_create_elements_list(self):
        authors_scores = pd.Series(
            [2, 1, 1, 2, 1, 1, 1, 2, 1],
            index=["philippe caillou", "samir aknine", "suzanne pinson",
                   "samuel thiriot", "bruno cessac", "hlne paugammoisy",
                   "thierry viville", "zach lewkovicz", "jeandaniel kant"]
        )
        positions = np.array([[2182.234, 3525.129, 3565.5313, 1886.508,
                               2903.6265, 2938.4333,
                               2849.0598, 1759.4761, 2986.9456],
                              [2186.0632, 3523.1338, 3563.5352, 1888.107,
                               2904.62, 2940.4446, 2850.128,
                               1768.3367, 2987.2205]])
        total = 0
        elements = create_elements_list(
            "authors", positions, authors_scores, total)

        self.assertEqual(len(elements), 9)
        for x in range(len(elements)):
            self.assertEqual(elements[x]["nature"], "authors")
            self.assertEqual(elements[x]["label"], authors_scores.index[x])
            self.assertEqual(elements[x]["score"], authors_scores.iloc[x])
            self.assertEqual(elements[x]["position"],
                             positions[:, x].tolist())
            self.assertEqual(elements[x]["rank"], x)

    def test_export_to_json(self):
        natures = ["articles", "authors"]
        positions = [np.array([[2182.234, 3525.129, 3565.5313],
                               [2186.0632, 3523.1338, 3563.5352]]),
                     np.array([[2903.6265, 2938.4333],
                               [1768.3367, 2987.2205]])]
        scores = [pd.Series([2, 1, 1],
                            index=["article 1", "article 2", "article 3"]),
                  pd.Series([5, 0], index=["jean", "marc-antoine"])]
        filename = "export.json"

        export_to_json(natures, positions, scores, filename)

        self.assertTrue(os.path.exists(filename))

        with open(filename, 'r') as f:
            json_dump = json.load(f)

        self.assertEqual(json_dump[0],
                         {'label': 'article 1', 'nature': 'articles',
                          'score': 2.0, 'rank': 0,
                          'position': [2182.234, 2186.0632]})
        self.assertEqual(json_dump[1],
                         {'label': 'article 2', 'nature': 'articles',
                          'score': 1.0, 'rank': 1,
                          'position': [3525.129, 3523.1338]})
        self.assertEqual(json_dump[2],
                         {'label': 'article 3', 'nature': 'articles',
                          'score': 1.0, 'rank': 2,
                          'position': [3565.5313, 3563.5352]})
        self.assertEqual(json_dump[3],
                         {'label': 'jean', 'nature': 'authors', 'score': 5.0,
                          'rank': 0, 'position': [2903.6265, 1768.3367]})
        self.assertEqual(json_dump[4],
                         {'label': 'marc-antoine', 'nature': 'authors',
                          'score': 0.0, 'rank': 1,
                          'position': [2938.4333, 2987.2205]})
