"""
Tests for Cartolabe clustering methods
Shamelessly based on (i.e. ripped off from) the HDBSCAN test code,
itself borrowed from the DBSCAN test code.
"""
from unittest import TestCase
from pandas.testing import assert_series_equal

from sklearn.datasets import make_blobs
from sklearn.utils import shuffle
from sklearn.preprocessing import StandardScaler

import pandas as pd
import numpy as np
from pathlib import Path

from cartodata.clustering import (
    create_kmeans_clusters, best_hdbscan_parameters
    )

from cartodata.operations import (
    load_matrices_from_dumps, load_scores
)

dir_path = Path(__file__).parent
DATASET_DIR = dir_path / "dumps"
SCORE_DIR = dir_path / "scores"

NATURES = [
    'articles', 'authors', 'teams', 'labs', 'words'
]
WORDS_INDEX = 4


HL_INDEX = pd.Index([
        "social, language",
        "learning, physics",
        "computing, query",
        "networking internet architecture, quantitative methods",
        "interaction, cognitive science",
        "logic science, formal",
        "problem, mathematics",
        "optimization, evolutionary"
    ])

hl_cluster_series = pd.Series(
    index=HL_INDEX,
    data=[586, 518, 513, 474, 774, 416, 537, 444]
)

hl_cluster_eval_pos = pd.Series(
    index=HL_INDEX,
    data=[0.500000, 0.814672, 0.555556, 0.337553,
          0.801034, 0.545673, 0.713222, 0.738739]
)

hl_cluster_eval_neg = pd.Series(
    index=HL_INDEX,
    data=[0.106922, 0.129771, 0.130913, 0.015143,
          0.072077, 0.049943, 0.243625, 0.123422]
)

hl_cluster_labels = [
    'social', 'language', 'learning', 'physics', 'computing', 'query',
    'networking internet architecture', 'quantitative methods', 'interaction',
    'cognitive science', 'logic science', 'formal', 'problem', 'mathematics',
    'optimization', 'evolutionary'
]

ML_INDEX = pd.Index([
    'artificial intelligence, semantic',
    'matrix adaptation evolution strategy, multi armed',
    'mathematics, problem', 'design, visualizations',
    'logic science, proof', 'networking internet architecture, protocols',
    'learning', 'optimization, monte carlo search', 'sciences, biological',
    'systems, modeling simulation', 'movement, speech', 'physics',
    'testing, linear systems', 'visualization, image',
    'mobile robots, robots', 'interaction', 'query, quantum',
    'cluster computing, services', 'language', 'social, wikipedia', 'graph',
    'performance, programming languages', 'evolutionary, population',
    'structure, quantitative methods'],
                    dtype='object')

ml_cluster_labels = [
    'artificial intelligence', 'semantic',
    'matrix adaptation evolution strategy', 'multi armed', 'mathematics',
    'problem', 'design', 'visualizations', 'logic science', 'proof',
    'networking internet architecture', 'protocols', 'learning',
    'optimization', 'monte carlo search', 'sciences', 'biological', 'systems',
    'modeling simulation', 'movement', 'speech', 'physics', 'testing',
    'linear systems', 'visualization', 'image', 'mobile robots', 'robots',
    'interaction', 'query', 'quantum', 'cluster computing', 'services',
    'language', 'social', 'wikipedia', 'graph', 'performance',
    'programming languages', 'evolutionary', 'population', 'structure',
    'quantitative methods'
]

LL_INDEX = pd.Index([
    'ontology, knowledge', 'multi armed, active learning',
    'complexity, minimum', 'image, segmentation', 'floating point',
    'networks, comparative genomics', 'cluster computing',
    'users, interfaces', 'language, speech',
    'optimization control, evolution strategies',
    'reinforcement learning, models', 'fluid, numerical simulations',
    'semantics, modulo', 'programming languages, synchronous',
    'population protocols, consensus', 'evolutionary',
    'networking internet architecture, energy', 'simulation, systems',
    'movement, composition',
    'covariance matrix adaptation evolution, optimization', 'quantum',
    'quantitative methods, structural',
    'social information networks, social media', 'numerical',
    'visualization', 'query', 'mobile robots', 'interaction', 'monte carlo',
    'sciences, metabolic', 'logic science, verification', 'integer',
    'number searchers', 'graph', 'testing, model checker',
    'challenge, gesture recognition', 'random', 'mathematics, automata',
    'scientific workflows, series', 'performance',
    'secondary structure, structures', 'stabilizing, deterministic',
    'natural language, language models', 'model, rankings',
    'bitcoin, mining', 'visual analytics, visual', 'semantic, entity',
    'fault', 'belief propagation, traffic', 'algorithm',
    'statistics, information theoretic', 'cloud, services',
    'neural networks, neural network', 'displays, reality', 'protein',
    'cognitive science, exploration', 'register allocation, loops',
    'testbed', 'inference, network model',
    'multi objective, hypervolume indicator', 'design', 'physics', 'social',
    'machine learning', 'selection, gesture', 'machine translation',
    'information, documents', 'processors, architectures',
    'radio, resource', 'problem', 'adaptation', 'visualizations'],
                    dtype='object')

ll_cluster_labels = [
    'ontology', 'knowledge', 'multi armed', 'active learning', 'complexity',
    'minimum', 'image', 'segmentation', 'floating point', 'networks',
    'comparative genomics', 'cluster computing', 'users', 'interfaces',
    'language', 'speech', 'optimization control', 'evolution strategies',
    'reinforcement learning', 'models', 'fluid', 'numerical simulations',
    'semantics', 'modulo', 'programming languages', 'synchronous',
    'population protocols', 'consensus', 'evolutionary',
    'networking internet architecture', 'energy', 'simulation', 'systems',
    'movement', 'composition', 'covariance matrix adaptation evolution',
    'optimization', 'quantum', 'quantitative methods', 'structural',
    'social information networks', 'social media', 'numerical',
    'visualization', 'query', 'mobile robots', 'interaction', 'monte carlo',
    'sciences', 'metabolic', 'logic science', 'verification', 'integer',
    'number searchers', 'graph', 'testing', 'model checker', 'challenge',
    'gesture recognition', 'random', 'mathematics', 'automata',
    'scientific workflows', 'series', 'performance', 'secondary structure',
    'structures', 'stabilizing', 'deterministic', 'natural language',
    'language models', 'model', 'rankings', 'bitcoin', 'mining',
    'visual analytics', 'visual', 'semantic', 'entity', 'fault',
    'belief propagation', 'traffic', 'algorithm', 'statistics',
    'information theoretic', 'cloud', 'services', 'neural networks',
    'neural network', 'displays', 'reality', 'protein', 'cognitive science',
    'exploration', 'register allocation', 'loops', 'testbed', 'inference',
    'network model', 'multi objective', 'hypervolume indicator', 'design',
    'physics', 'social', 'machine learning', 'selection', 'gesture',
    'machine translation', 'information', 'documents', 'processors',
    'architectures', 'radio', 'resource', 'problem', 'adaptation',
    'visualizations']


class TestClustering(TestCase):
    def setUp(self):
        self.n_clusters = 5
        X, y = make_blobs(
            n_samples=200,
            centers=self.n_clusters,
            shuffle=True,
            random_state=10
        )
        X, y = shuffle(X, y, random_state=7)
        self.data = StandardScaler().fit_transform(X)

        self.scores = load_scores(NATURES, DATASET_DIR)
        self.matrices = load_matrices_from_dumps(NATURES, "mat", DATASET_DIR)
        self.matrices_2D = load_matrices_from_dumps(NATURES, "umap",
                                                    DATASET_DIR)
        self.matrices_nD = load_matrices_from_dumps(NATURES, "lsa",
                                                    DATASET_DIR)

    def test_kmeans_clusters(self):
        # 8 clusters
        nb_clusters = 8
        cluster_labels = []
        RANDOM_STATE = np.random.RandomState(42)

        (c_nD, c_2D, c_scores, c_lbs, c_wscores, cluster_eval_pos,
         cluster_eval_neg) = create_kmeans_clusters(
             nb_clusters,
             clustering_table=self.matrices_2D[0],
             naming_table=self.matrices_2D[WORDS_INDEX],
             natural_space_naming_table=self.matrices[WORDS_INDEX],
             naming_scores=self.scores[WORDS_INDEX],
             previous_cluster_labels=cluster_labels,
             naming_profile_table=self.matrices_nD[WORDS_INDEX],
             random_state=RANDOM_STATE
             )

        assert_series_equal(c_scores, hl_cluster_series)
        assert_series_equal(cluster_eval_pos, hl_cluster_eval_pos,
                            check_exact=False, atol=1e-6)
        assert_series_equal(cluster_eval_neg, hl_cluster_eval_neg,
                            check_exact=False, atol=1e-6)
        assert c_lbs.shape[0] == 4262
        assert c_lbs[0] == 5
        assert c_lbs[-1] == 0
        assert c_wscores.size == 4832
        self.assertAlmostEqual(c_wscores[0], 0.00374591)
        self.assertAlmostEqual(c_wscores[-1], -0.00261712)
        assert cluster_labels == hl_cluster_labels

        # 24 clusters
        nb_clusters = 24
        cluster_labels = []
        RANDOM_STATE = np.random.RandomState(42)

        (c_nD, c_2D, c_scores, c_lbs, c_wscores, cluster_eval_pos,
         cluster_eval_neg) = create_kmeans_clusters(
             nb_clusters, self.matrices_2D[0],
             self.matrices_2D[WORDS_INDEX],
             self.matrices[WORDS_INDEX],
             self.scores[WORDS_INDEX],
             cluster_labels,
             self.matrices_nD[WORDS_INDEX],
             random_state=RANDOM_STATE
             )

        assert cluster_eval_pos.index.equals(ML_INDEX)
        assert cluster_eval_neg.index.equals(ML_INDEX)
        assert c_scores.index.equals(ML_INDEX)
        assert c_lbs.shape[0] == 4262
        assert c_lbs[0] == 4
        assert c_lbs[-1] == 0
        assert c_wscores.size == 4832
        self.assertAlmostEqual(c_wscores[0], -0.00385078)
        self.assertAlmostEqual(c_wscores[-1], -0.00236855)
        assert cluster_labels == ml_cluster_labels

        # 72 cluster
        nb_clusters = 72
        cluster_labels = []
        RANDOM_STATE = np.random.RandomState(42)

        (c_nD, c_2D, c_scores, c_lbs, c_wscores, cluster_eval_pos,
         cluster_eval_neg) = create_kmeans_clusters(
             nb_clusters, self.matrices_2D[0],
             self.matrices_2D[WORDS_INDEX],
             self.matrices[WORDS_INDEX],
             self.scores[WORDS_INDEX],
             cluster_labels,
             self.matrices_nD[WORDS_INDEX],
             random_state=RANDOM_STATE
             )

        assert cluster_eval_pos.index.equals(LL_INDEX)
        assert cluster_eval_neg.index.equals(LL_INDEX)
        assert c_scores.index.equals(LL_INDEX)
        assert c_lbs.shape[0] == 4262
        assert c_lbs[0] == 12
        assert c_lbs[-1] == 46
        assert c_wscores.size == 4832
        self.assertAlmostEqual(c_wscores[0], 0.00845882)
        self.assertAlmostEqual(c_wscores[-1], 0.02308704)
        assert cluster_labels == ll_cluster_labels

    def test_hdbscan_parameters(self):
        cache = []
        with self.assertRaises(ValueError):
            nb, hdb = best_hdbscan_parameters(self.data, 3.5)
        nb, hdb = best_hdbscan_parameters(self.data, 5)
        self.assertEqual(nb, 5)
        nb, hdb = best_hdbscan_parameters(self.data, 5, cache=cache)
        self.assertEqual(nb, 5)
        nb, hdb = best_hdbscan_parameters(self.data, 3, cache=cache)
        self.assertEqual(nb, 5)
        nb, hdb = best_hdbscan_parameters(self.data, 2, cache=cache)
        self.assertEqual(nb, 2)
        nb, hdb = best_hdbscan_parameters(self.data, 4, cache=cache)
        self.assertEqual(nb, 5)
        nb, hdb = best_hdbscan_parameters(self.data, 6, cache=cache)
        self.assertEqual(nb, 5)

    def test_hdbscan_clusters(self):
        # TODO
        self.assertTrue(True)
