from pathlib import Path

import json
import pandas as pd

from cartodata.phases import PhasePost


def load_scores_from_file(phase, scores, dumps_dir, filename):
    result = None
    parent_dir = Path(dumps_dir)
    scores_file = parent_dir / filename

    if scores_file.exists():
        result = Result()
        scores_df = pd.read_csv(scores_file, index_col=None)

        for row in scores_df.to_numpy():
            name, score = row

            without_phase_name = phase.remove_prefix(name)
            if without_phase_name in scores:
                result.add_score(without_phase_name, score)
    return result


def load_desc_scores_from_file(result, phase, scores, parent_dir,
                               load_params=True):

    for score_name in scores:
        scores_det_file = (
            parent_dir / f"scores_{phase.name()}__{score_name}_det.csv"
            )
        if scores_det_file.exists():
            scores_det_df = pd.read_csv(scores_det_file, header=None)

            rows = scores_det_df.to_numpy()

            value = float(rows[2][0].replace("VALUE: ", ""))
            scores = rows[3:]
            scores = pd.DataFrame(scores)
            result.add_desc_score(f"{score_name}_det", scores[0], value)

            if load_params:
                run_params = rows[0][0]
                run_params = run_params.replace("run params: ", "")
                run_params = run_params.replace("'", "\"")
                run_params = run_params.replace("True", "true")
                run_params = run_params.replace("False", "false")
                run_params = run_params.replace("None", "null")
                run_params = json.loads(run_params)
                new_run_params = {}

                for param, value in run_params.items():
                    if phase.name() in param:
                        sans_prefix = phase.remove_prefix(param, "r")
                        new_run_params[sans_prefix] = value
                result.append_run_params(new_run_params)

                score_params = rows[1][0]
                score_params = score_params.replace("score params: ", "")
                score_params = score_params.replace("'", "\"")
                score_params = score_params.replace("True", "true")
                score_params = score_params.replace("False", "false")
                score_params = score_params.replace("None", "null")
                score_params = json.loads(score_params)
                new_score_params = {}

                for param, value in score_params.items():
                    if phase.name() in param:
                        sans_prefix = phase.remove_prefix(param, "s")
                        new_score_params[sans_prefix] = value

                result.append_score_params(new_score_params)


def load_raw_scores_from_file(result, phase, scores, parent_dir):

    for score_name in scores:
        scores_raw_file = (
            parent_dir / f"scores_{phase.name()}__{score_name}_raw.csv"
            )
        if scores_raw_file.exists():
            scores_raw_df = pd.read_csv(scores_raw_file, index_col=0)
            if scores_raw_df.shape[1] == 1:
                scores_raw_df = scores_raw_df.squeeze()

            result.add_raw_score(score_name, scores_raw_df)


class Result():
    """A class to keep record of score values and the run and scoring
    parameters used while caclulating the score.

    A result has 3 types of scores:

    - scores: a dictionary of the form
        key: value
    - desc_scores : a dictionary of the form
        key: (list, value)
    - raw_scores
        key: list  or
        key: value

    It has 2 types of parameters:

    - run_params: dictionary of parameters used to generate artifacts used in
    - calculating scores
    - score_params: dictionary of parameters used to calculate scores
    """

    def __init__(self):
        """Initilizes a new result."""
        self.run_params = {}
        self.score_params = {}

        self.scores = {}
        self.desc_scores = {}
        self.raw_scores = {}

    def reset(self):
        """Resets all values stored in this result."""
        self.run_params = {}
        self.score_params = {}

        self.scores = {}
        self.desc_scores = {}
        self.raw_scores = {}

    def prefix(self, phase):
        if phase is not None:
            self.scores = phase.prefix(self.scores)
            self.desc_scores = phase.prefix(self.desc_scores)
            self.raw_scores = phase.prefix(self.raw_scores)

            self.run_params = phase.prefix(self.run_params, "r")
            self.score_params = phase.prefix(self.score_params, "s")

    def add_score(self, name, score):
        """Adds the specified ``score`` with ``name`` to the score list.

        Parameters
        ----------
        name: str
            name of the score
        score: float
            value of the score
        """
        self.scores[name] = score

    def add_desc_score(self, name, scores, value):
        """Adds the specified ``scores`` and ``value`` with ``name`` to the
        descriptive scores list.

        Parameters
        ----------
        name: str
            name of the score
        scores: pandas.Series
            list of values associated to this score as pandas.Series
        value: float
            an aggregated value obtained from ``scores``
        """
        self.desc_scores[name] = (scores, value)

    def add_raw_score(self, name, value):
        """Adds the specified ``value`` with ``name`` to raw scores list.

        Parameters
        ----------
        name: str
            name of the score
        value: float or list
            the values associated to the score with ``name``
        """
        self.raw_scores[name] = value

    def has_score(self, score_name):
        """Checks if a score with `score_name` exists in the score list of
        this result.

        Parameters
        ----------
        score_name: str
            name of the score to check.

        Returns
        -------
        bool
            True if a score with ``score_name`` exists; False otherwise
        """
        return score_name in self.scores.keys()

    def append_score_params(self, score_params):
        """Appends the specified ``score_params`` to the list of score
        parameters of this result.

        Parameters
        ----------
        score_params: dict
            dictionary of scoring parameters
        """
        self.score_params.update(score_params)

    def append_run_params(self, run_params):
        """Appends the specified ``run_params`` to the list of run parameters
        of this result.

        Parameters
        ----------
        run_params: dict
            dictionary of run parameters
        """
        self.run_params.update(run_params)

    def append(self, result):
        """Appends scores, descriptive scores, raw scores, run parameters and
        scoring parameters of the specified ``result`` to this one.

        Parameters
        ----------
        result: Result
            the result to append to this result
        """
        self.scores.update(result.scores)
        self.desc_scores.update(result.desc_scores)
        self.raw_scores.update(result.raw_scores)

        self.run_params.update(result.run_params)
        self.score_params.update(result.score_params)

    def print(self):
        self.print_params()
        self.print_scores()
        self.print_desc_scores()
        self.print_raw_scores()

    def print_params(self):
        print("\n================================\n")
        print(f"Run params : {self.run_params}\n")
        print("\n--------------------------------\n")
        print(f"Scoring params : {self.score_params}\n")
        print("================================\n")

    def print_scores(self):
        print("\n----------- Scores -----------")
        for key, value in self.scores.items():
            print(f"{key} : {value:.4f}")

    def print_desc_scores(self):
        print("\n--------- Desc Scores ---------")
        for key, item in self.desc_scores.items():
            scores, num_value = item
            print(f"{key}\n")
            print(scores.reset_index(drop=True))
            print(f"\nVALUE : {num_value:.4f}")

    def print_raw_scores(self):
        print("\n--------- Raw Scores ---------")
        for key, value in self.raw_scores.items():
            print(f"{key}\n")
            print(value)

    @property
    def id(self):
        if self.params is None:
            return None
        else:
            return self.params["id"]

    def persist_scores(self, parent_dir, filename="scores_all.csv"):
        """Persists the scores of this result to the specified file with
        ``filename`` under ``parent_dir``.

        Parameters
        ----------
        parent_dir: str, Path
            the path of the parent directory to save the file
        filename: struct
            name of the scores file
        """
        parent_dir = Path(parent_dir)
        parent_dir.mkdir(parents=True, exist_ok=True)
        scores_df = pd.DataFrame.from_dict(self.scores, orient="index",
                                           columns=["value"])
        scores_df.to_csv(parent_dir / filename)

    def persist_desc_scores(self, parent_dir):
        """Persists descriptive scores of this result together with the run and
        scoring parameters to a file with name ``scores_{desc_score_name}.csv``
        under the specified ``parent_dir``.

        Parameters
        ----------
        parent_dir: str, Path
            the path of the parent directory to save the file
        """
        parent_dir = Path(parent_dir)
        parent_dir.mkdir(parents=True, exist_ok=True)

        for key, item in self.desc_scores.items():
            scores, num_value = item
            det = pd.concat(
                [pd.Series(f"run params: {self.run_params}"),
                 pd.Series(f"score params: {self.score_params}"),
                 pd.Series(f"VALUE: {num_value}"),
                 scores]
            )
            det.to_csv(
                parent_dir / f"scores_{key}.csv",
                index=False, header=False
            )

    def persist_raw_scores(self, parent_dir):
        parent_dir = Path(parent_dir)
        parent_dir.mkdir(parents=True, exist_ok=True)

        for key, item in self.raw_scores.items():
            item.to_csv(parent_dir / f"scores_{key}_raw.csv")


class Results():
    """A class to store multiple results."""

    def __init__(self, score_dir, final_result_file="final_results.csv",
                 header=0, index_col=0):
        """Initializes Results instance with the ``score_dir`` to save the
        the results from all runs.

        Parameters
        ----------
        score_dir: str, Path
            the directory to save the result files
        final_result_file: str
            the name of the final result file
        header: int
            header row index to use while loading if there exists already a
        final results file
        index_col: int
            index column to use while loading if there exists already a final
        results file
        """
        score_dir = Path(score_dir)
        score_dir.mkdir(parents=True, exist_ok=True)
        self.score_dir = score_dir

        # create final_results file under scores_dir
        self.final_result_file = self.score_dir / final_result_file

        base_columns = ["0_agscore"]
        try:
            self.df_final_results = pd.read_csv(
                self.final_result_file, header=header, index_col=index_col
            )
            assert self.df_final_results[base_columns].shape[1] == 1, (
                f"{final_result_file} file should contain columns:\n"
                f"{base_columns}."
            )
        except FileNotFoundError:
            self.df_final_results = pd.DataFrame(columns=base_columns)

        self.runs_ = []

    def read_or_init_file_to_df(self, filepath, index_col=0):
        """Reads the file specified with ``filepath`` to a dataframe if it
        already exists and returns the dataframe. Creates an empty dataframe
        otherwise.

        Parameters
        ----------
        filepath: str, Path
            The path of the file to be read to a dataframe.

        Returns
        -------
        pandas.DataFrame
            A dataframe that contains the contents of the file ``filepath`` if
        it exists; an empty dataframe otherwise.
        """
        filepath = Path(filepath)
        try:
            df = pd.read_csv(filepath, index_col=index_col)
        except FileNotFoundError:
            df = pd.DataFrame()

        return df

    def print_best(self, n=5, field="0_agscore", ascending=False):
        """Prints the best ``n`` results sorted by the specified ``field``.

        Parameters
        ----------
        n: int
            the number of results to show
        field: str
            the name of the field to sort the results
        ascending: bool
            specify to sort in ascending or descending order
        """
        print("------------------------\n")
        print(f"\nBest {n} results: \n")
        print("------------------------\n")
        print(self.df_final_results.sort_values(
            field, ascending=ascending).iloc[0:n]
              )

    def append(self, result, dumps_dir):
        """Appends the specified result to the list of results and persists the
        scores under the scores directory.

        The scores of the result are appended to the final results file.
        The descriptive scores are appended to the corresponding descriptive
        score file.

        Parameters
        ----------
        result: Result
            the result to append to the list of results
        dumps_dir: str, Path
            the path of the directory where artifacts corresponding to the
        specified ``result`` is saved
        """
        self.runs_.append(result)
        params = result.run_params.copy()
        params.update(result.score_params)

        self.append_to_final_results(params, result.scores, dumps_dir)

        # add each score in the result to corresponding file under scores
        # directory
        for score_name, value in result.scores.items():
            score_file = self.score_dir / f"{score_name}.csv"
            score_df = self.read_or_init_file_to_df(score_file)

            # new_row = pd.DataFrame([[value, params]],
            #                        columns=[score_name, "params"],
            #                        index=[result.id])
            new_row = pd.DataFrame([[value]],
                                   columns=[score_name],
                                   index=[params])
            score_df = pd.concat([score_df, new_row])
            score_df.to_csv(score_file)

        # add each desc_score in the result to corresponding file under scores
        # directory
        for score_name, value in result.desc_scores.items():
            desc_score_file = self.score_dir / f"{score_name}.csv"
            desc_score_df = self.read_or_init_file_to_df(desc_score_file)

            scores = value[0].to_list()
            num_value = value[1]

            new_row = pd.DataFrame(
                [[num_value] + scores], index=[params],
                columns=["value"] + [str(i) for i in range(len(scores))]
            )
            desc_score_df = pd.concat([desc_score_df, new_row])
            desc_score_df.to_csv(desc_score_file)

    def append_to_final_results(self, params, scores, dumps_dir):
        """ Appends the specified ``scores`` together with ``params``,
        ``dumps_dir`` to the final scores file.

        Adds an additional ``agscore`` field. If no value is specified with
        ``5_pst__final_score`` column, ``agscore`` is set to 0.

        Parameters
        ----------
        params: dict
            complete set of parameters (run and scoring) used to calculate the
        specified ``scores``
        scores: dict
            scores to be added to the final results file
        dumps_dir: str, Path
            the path of the directory where artifacts corresponding to the
        specified ``result`` is saved
        """
        idsc = len(self.df_final_results)

        agscore = scores.get(f"{PhasePost.name()}__final_score", 0)

        cols = (
            ["0_agscore"] + list(params.keys()) + list(scores.keys()) +
            ["dump", "active"]
        )
        values = (
            [agscore] + list(params.values()) + list(scores.values()) +
            [dumps_dir, True]
        )
        new_result = pd.DataFrame([values], columns=cols, index=[idsc])

        if idsc == 0:
            self.df_final_results = new_result
        else:
            # new_hor_value.columns = self.df_final_results.columns
            # self.df_final_results.loc[idsc] = new_hor_value.loc[idsc]
            self.df_final_results = pd.concat([self.df_final_results,
                                               new_result])

        self.df_final_results = self.df_final_results.sort_values(
            ["0_agscore"], ascending=False
        )

        self.df_final_results.to_csv(self.final_result_file,
                                     index_label="0_rank")
