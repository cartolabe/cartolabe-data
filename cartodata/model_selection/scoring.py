import logging
from pathlib import Path

import gzip
import numpy as np
import pandas as pd
from scipy.spatial import distance
from sklearn import metrics
from sklearn.manifold import trustworthiness
from umap.validation import trustworthiness_vector

from cartodata.clustering import create_kmeans_clusters
from cartodata.model_selection.utils import (
    Result, load_scores_from_file, load_desc_scores_from_file,
    load_raw_scores_from_file

)
from cartodata.neighbors import get_neighbors, get_neighbors_path
from cartodata.operations import (
    load_scores, load_labels, load_matrices_from_dumps
)

logger = logging.getLogger(__name__)


class ScoringBase():
    """A base class for scoring."""

    DEFAULTS = {}

    @classmethod
    def get_kwargs(cls, params):
        kwargs = cls.DEFAULTS
        prefix = f"{cls.KEY}__"

        for key in params.keys():
            if key.startswith(prefix):
                sub_key = key.replace(prefix, "")
                kwargs[sub_key] = params[key]
        return kwargs

    @classmethod
    def update_defaults(cls, kwargs):
        defaults = cls.DEFAULTS.copy()
        defaults.update(kwargs)
        kwargs = defaults

        return kwargs


class Neighbors(ScoringBase):

    KEY = "neighbors"
    DEFAULTS = {
        "min_score": 20,
        "recompute": False,
        "sample_size": None,
        "n_neighbors": 10,
        "random_state": 42
    }

    @classmethod
    def load_scores_from_file(cls, phase, nature, source, dumps_dir,
                              filename="scores_all.csv"):

        name = f"{cls.KEY}_{nature}_{source}"
        score_names = [name]

        try:
            # load scores
            result = load_scores_from_file(phase, score_names, dumps_dir,
                                           filename)
            assert result and len(result.scores) == len(score_names)

            # load descriptive scores
            load_desc_scores_from_file(result, phase, [name], dumps_dir,
                                       load_params=True)
            assert len(result.desc_scores) == 1
            assert len(result.run_params) > 0
            assert len(result.score_params) > 0

            # load raw scores
            load_raw_scores_from_file(result, phase, [name], dumps_dir)
            assert len(result.raw_scores) == 1
        except AssertionError:
            return None

        return result

    @classmethod
    def get_score_keys(cls, nature, source):
        return f"{cls.KEY}_{nature}_{source}"

    @classmethod
    def load_and_evaluate(cls, nature, source, key_xD, dir_mat, dir_xD,
                          **kwargs):
        """Calculates scores depending on neighbors for x-dimensional
        projection.

        Parameters
        ----------
        evaluator_key: str
            the key for the evaluator to be used.
        nature: str
            the base nature of calculation.
        source: str
            the source nature to calculate the score for.
        dir_mat: str, Path
            the path of the directory that contains the entity matrices.
        dumps_dir: Path
            the directory that contains x-dimensional projection matrices.
        min_score: int, default=20
            the minimum score for source scores to be considered for score
        calculation.
        recompute: bool, default=False
            specifies if the neighbors should be recomputed.

        Returns
        -------
        evals: Evaluation
            Returns the calculated scores and descriptive scores.
        """
        kwargs = cls.update_defaults(kwargs)
        recompute = kwargs["recompute"]

        dir_mat = Path(dir_mat)
        dir_xD = Path(dir_xD)

        scores_source = load_scores([source], dir_mat)[0]
        matrix_source = load_matrices_from_dumps([source], "mat", dir_mat)[0]

        neighbors_filename = get_neighbors_path(nature, nature, dir_xD)

        if not recompute and not neighbors_filename.exists():
            logger.info(
                f"{neighbors_filename} does not exist. "
                "Will recompute neighbors."
            )
            recompute = True

        if recompute:
            scores_nature = load_scores([nature], dir_mat)[0]
            matrix_nature_xD = load_matrices_from_dumps(
                [nature], key_xD, dir_xD
            )[0]
        else:
            scores_nature = None
            matrix_nature_xD = None

        kwargs["recompute"] = recompute
        result = cls.evaluate(
            nature, source, matrix_source, scores_source,
            neighbors_filename=neighbors_filename, dir_xD=dir_xD,
            scores_nature=scores_nature, matrix_nature_xD=matrix_nature_xD,
            **kwargs
        )

        return result

    @classmethod
    def sample(cls, source_transpose, scores_source, use, sample_size,
               min_score, random_state=42):
        sub_scores = scores_source.loc[use.squeeze()]

        if len(sub_scores) > sample_size:
            sub_scores = sub_scores.sample(sample_size,
                                           random_state=random_state)
            diff = use.index.difference(sub_scores.index)
            use.loc[diff] = False
        source_transpose = source_transpose[use]
        scores_source = scores_source[use]
        use = scores_source >= min_score

        return source_transpose, scores_source, use

    @classmethod
    def evaluate(cls, nature, source, matrix_source, scores_source,
                 neighbors_filename=None, dir_xD=None,
                 scores_nature=None, matrix_nature_xD=None,
                 **kwargs):

        defaults = cls.DEFAULTS.copy()
        defaults.update(kwargs)
        kwargs = defaults

        min_score = kwargs["min_score"]
        recompute = kwargs["recompute"]
        sample_size = kwargs["sample_size"]
        n_neighbors = kwargs["n_neighbors"]
        random_state = kwargs["random_state"]

        dir_xD = Path(dir_xD)

        logger.info(f"Calculating scores for {cls.KEY}_{nature}_{source}.")

        result = Result()

        if neighbors_filename is None:
            neighbors_filename = get_neighbors_path(nature, nature, dir_xD)

        if recompute:
            assert (dir_xD is not None and scores_nature is not None and
                    matrix_nature_xD is not None)
            logger.info("Computing neighbors...")
            weight = [0.0]
            matrix_nn = get_neighbors(
                matrix_nature_xD, scores_nature, [matrix_nature_xD], weight,
                neighbors_nature=nature, natures=[nature], dump_dir=dir_xD,
                n_neighbors=n_neighbors
            )[0]
            logger.debug("Finished computing neighbors.")
        else:
            logger.info(f"Loading neighbors from {neighbors_filename}.")
            with gzip.GzipFile(neighbors_filename, 'r') as file:
                matrix_nn = np.load(file)

            logger.debug("Finished loading neighbors from file.")

        use = scores_source >= min_score
        source_transpose = matrix_source.T

        if sample_size is not None:
            logger.debug(
                f"Calculating scores for {source} with a sample of "
                f"{sample_size}."
            )

            source_transpose, scores_source, use = Neighbors.sample(
                source_transpose, scores_source, use, sample_size, min_score,
                random_state
            )
        nature_source = source_transpose.tolil().rows

        nntransp = matrix_nn.T
        numb_nn = nntransp.shape[1]

        isin = [np.sum([
            nature in nature_source[source_index] for nature in nntransp[
                nature_source[source_index], :].flatten()
        ]) for source_index in range(len(scores_source))]

        maxsum = [
            scores_source.iloc[
                source_index
            ] * numb_nn for source_index in range(len(scores_source))
        ]

        rate = np.divide(isin, maxsum)
        filt = use * rate
        mean = np.sum(filt) / np.sum(use)

        scores = filt[use]

        # desc scores
        # creates a Series preserving the index of the Series scores
        # and modifying each `value` of the Series scores with `index : value`
        desc_scores = scores.sort_values(ascending=False).index.astype(
            str) + " : " + scores.sort_values(ascending=False).map(
                lambda x: '{0:.2f}'.format(x)).astype(str)

        name = f"{cls.KEY}_{nature}_{source}"
        desc_name = f"{name}_det"

        result.add_score(name, mean)
        result.add_raw_score(name, scores)
        result.add_desc_score(desc_name, desc_scores, mean)

        return result


class NeighborsND(Neighbors):

    KEY = "neighbors"


class Neighbors2D(Neighbors):
    KEY = "neighbors"


class Comparative(ScoringBase):

    KEY = "comparative"
    DEFAULTS = {}

    @classmethod
    def score_base_name(cls, key1, key2):
        phase1 = key1.split("__")[0]
        phase2 = key2.split("__")[0]
        score_name = key2.split("__")[1]
        name = f"{phase1}_{phase2}_{score_name}_dim"
        return name

    @classmethod
    def get_score_keys(cls, key1, key2):
        name = cls.score_base_name(key1, key2)
        return [f"{name}_mean", f"{name}_median"]

    @classmethod
    def load_scores_from_file(cls, phase, key1, key2, dumps_dir,
                              filename="scores_all.csv"):

        score_base_name = cls.score_base_name(key1, key2)
        score_names = cls.get_score_keys(key1, key2)

        try:
            # load scores
            result = load_scores_from_file(phase, score_names, dumps_dir,
                                           filename)
            assert result and len(result.scores) == len(score_names)

            # load descriptive scores
            load_desc_scores_from_file(result, phase, [score_base_name],
                                       dumps_dir, load_params=False)
            assert len(result.desc_scores) == 1
            assert len(result.run_params) == 0
            assert len(result.score_params) == 0

            # load raw scores
            load_raw_scores_from_file(result, phase, [score_base_name],
                                      dumps_dir)
            assert len(result.raw_scores) == 1
            assert result.raw_scores[score_base_name].shape[1] == 3
        except AssertionError:
            return None

        return result

    @classmethod
    def evaluate(cls, key1, key2, scores1, scores2):
        logger.info(f"Calculating scores for {cls.KEY}.")

        result = Result()
        name = cls.score_base_name(key1, key2)

        decsc = pd.DataFrame({key1: scores1, key2: scores2})
        decsc["scdec"] = decsc[key2] - decsc[key1]

        dim_mean = np.mean(decsc["scdec"].dropna())
        dim_median = np.median(decsc["scdec"].dropna())
        scores = decsc["scdec"]

        desc_scores = scores.sort_values(ascending=True).index.astype(
            str) + " : " + scores.sort_values(ascending=True).map(
                lambda x: '{0:.2f}'.format(x)).astype(str)

        result.add_score(f"{name}_mean", dim_mean)
        result.add_score(f"{name}_median", dim_median)
        result.add_desc_score(f"{name}_det", desc_scores, dim_mean)
        result.add_raw_score(name, decsc)

        return result


class Trustworthiness(ScoringBase):

    DEFAULTS = {
        "metric": "euclidean"
    }

    @classmethod
    def load_and_evaluate(cls, natures, key_nD, key_2D, dir_nD, dir_2D,
                          n_neighbors=5, **kwargs):

        kwargs = cls.update_defaults(kwargs)
        metric = kwargs["metric"]

        dir_nD = Path(dir_nD)
        dir_2D = Path(dir_2D)

        matrices_nD = load_matrices_from_dumps(natures, key_nD, dir_nD)
        matrices_2D = load_matrices_from_dumps(natures, key_2D, dir_2D)

        global_matrix_nD = np.asarray(np.hstack(matrices_nD).T)
        global_matrix_2D = np.asarray(np.hstack(matrices_2D).T)

        result = cls.evaluate(global_matrix_nD, global_matrix_2D,
                              tn_neighbors=n_neighbors, metric=metric)

        return result


class TrustworthinessSklearn(Trustworthiness):

    KEY = "trustworthiness_sklearn"
    DEFAULTS = {
        "metric": "euclidean",
        "n_neighbors": 5
    }

    @classmethod
    def load_scores_from_file(cls, phase, dumps_dir,
                              filename="scores_all.csv"):

        score_name = f"{cls.KEY}"
        score_names = [score_name]

        try:
            # load scores
            result = load_scores_from_file(phase, score_names, dumps_dir,
                                           filename)
            assert result and len(result.scores) == len(score_names)

        except AssertionError:
            return None

        return result

    @classmethod
    def evaluate(cls, X, X_embedded, **kwargs):

        logger.info(f"Calculating scores for {cls.KEY}.")

        kwargs = cls.update_defaults(kwargs)
        metric = kwargs["metric"]
        n_neighbors = kwargs["n_neighbors"]

        result = Result()

        name = f"{cls.KEY}"

        score = trustworthiness(X=X, X_embedded=X_embedded,
                                n_neighbors=n_neighbors, metric=metric)

        result.add_score(name, score)

        return result


class TrustworthinessUmap(Trustworthiness):

    KEY = "trustworthiness_umap"
    DEFAULTS = {
        "metric": "euclidean",
        "n_neighbors": 5
    }

    @classmethod
    def load_scores_from_file(cls, phase, dumps_dir,
                              filename="scores_all.csv"):

        score_name = f"{cls.KEY}"
        score_names = [score_name]

        try:
            # load scores
            result = load_scores_from_file(phase, score_names, dumps_dir,
                                           filename)
            assert result and len(result.scores) == len(score_names)

            # load raw scores
            load_raw_scores_from_file(result, phase, score_names,
                                      dumps_dir)
            assert len(result.raw_scores) == len(score_names)

        except AssertionError:
            return None

        return result

    @classmethod
    def evaluate(cls, X, X_embedded, **kwargs):
        logger.info(f"Calculating scores for {cls.KEY}.")

        kwargs = cls.update_defaults(kwargs)
        metric = kwargs["metric"]
        n_neighbors = kwargs["n_neighbors"]

        result = Result()

        name = f"{cls.KEY}"

        scores = trustworthiness_vector(source=X, embedding=X_embedded,
                                        max_k=n_neighbors, metric=metric)

        result.add_score(name, scores[-1])
        result.add_raw_score(name, pd.Series(scores))

        return result


class Distance(ScoringBase):

    KEY = "distance"
    DEFAULTS = {}

    @classmethod
    def evaluate(cls, matrices1, matrices2):

        name = "distance"
        result = Result()

        matrices1_stacked = np.hstack(matrices1).T
        matrices2_stacked = np.hstack(matrices2).T
        distances = [
            distance.euclidean(
                tuple(matrices1_stacked[i]), tuple(matrices2_stacked[i])
            )
            for i in range(matrices1_stacked.shape[0])
        ]

        result.add_score(f"{name}_mean",  np.mean(distances))
        result.add_score(f"{name}_median",  np.median(distances))

        return result


class Clustering(ScoringBase):

    KEY = "clustering"
    DEFAULTS = {
        "iter_stab": 2,
        "remove_stab": [0, .01, .03, .1, .25],
        "metric": "euclidean",
        "random_state": None
    }

    @classmethod
    def get_score_keys(cls, levels):
        level_score_names = [
            "nb_clust",
            "silhouette",
            "avg_word_couv",
            "med_word_couv",
            "avg_word_couv_minus",
            "big_small_ratio",
            "stab_clus",
        ]
        desc_scores = "clus_eval_pos"
        score_names = []

        for i in range(levels):
            for score in level_score_names:
                score_names.append(f"{score}_{i}")
        score_names.extend(["avg_stab_avg", "avg_couv_avg", "clu_score"])
        return score_names, [f"{desc_scores}_{i}" for i in range(levels)]

    @classmethod
    def load_scores_from_file(cls, phase, cluster_natures, dumps_dir,
                              filename="scores_all.csv"):

        score_names, desc_score_names = cls.get_score_keys(
            len(cluster_natures)
        )
        try:
            # load scores
            result = load_scores_from_file(phase, score_names, dumps_dir,
                                           filename)
            assert result and len(result.scores) == len(score_names)

            # load descriptive scores
            load_desc_scores_from_file(result, phase, desc_score_names,
                                       dumps_dir, load_params=True)
            assert len(result.desc_scores) == len(desc_score_names)
            assert len(result.run_params) > 0
            assert len(result.score_params) > 0

        except AssertionError:
            return None

        return result

    @classmethod
    def load_and_evaluate(cls, natures, cluster_natures,
                          key_nD, key_2D, dir_mat, dir_nD, dir_2D, words_index,
                          dir_clus, **kwargs):
        dir_mat = Path(dir_mat)
        dir_nD = Path(dir_nD)
        dir_2D = Path(dir_2D)
        dir_clus = Path(dir_clus)

        kwargs = cls.update_defaults(kwargs)
        iter_stab = kwargs["iter_stab"]
        remove_stab = kwargs["remove_stab"]
        metric = kwargs["metric"]
        random_state = kwargs["random_state"]

        # load necessary matrices and scores
        matrices = load_matrices_from_dumps(natures, "mat", dir_mat)
        scores = load_scores(natures, dir_mat)

        matrices_nD = load_matrices_from_dumps(natures, key_nD, dir_nD)
        matrices_2D = load_matrices_from_dumps(natures, key_2D, dir_2D)

        clus_scores = load_scores(cluster_natures, dir_clus)
        clus_eval_pos = load_scores(cluster_natures, dir_clus,
                                    suffix="eval_pos")
        clus_eval_neg = load_scores(cluster_natures, dir_clus,
                                    suffix="eval_neg")
        cluster_labels = load_labels(cluster_natures, dir_clus)

        result = cls.evaluate(
            cluster_labels, clustering_table=matrices_2D[0],
            naming_table=matrices_2D[words_index],
            natural_space_naming_table=matrices[words_index],
            naming_scores=scores[words_index],
            naming_profile_table=matrices_nD[words_index],
            c_scores=clus_scores,
            cluster_eval_pos=clus_eval_pos,
            cluster_eval_neg=clus_eval_neg, iter_stab=iter_stab,
            remove_stab=remove_stab, metric=metric,
            random_state=random_state
        )

        return result

    @classmethod
    def evaluate(cls, cluster_labels, clustering_table, naming_table,
                 natural_space_naming_table, naming_scores,
                 naming_profile_table, c_scores, cluster_eval_pos,
                 cluster_eval_neg, **kwargs):

        logger.info(f"Calculating scores for {cls.KEY}.")

        kwargs = cls.update_defaults(kwargs)
        iter_stab = kwargs["iter_stab"]
        remove_stab = kwargs["remove_stab"]
        metric = kwargs["metric"]
        random_state = kwargs["random_state"]

        result = Result()

        avg_stab = 0.0
        avg_couv = 0.0

        for idx, label_idx in enumerate(cluster_labels):
            nb_clusters = label_idx["nb_clusters"]
            c_lbs_idx = label_idx["c_lbs"]
            previous_cluster_labels = label_idx["initial_labels"]
            c_scores_idx = c_scores[idx]
            cluster_eval_pos_idx = cluster_eval_pos[idx]
            cluster_eval_neg_idx = cluster_eval_neg[idx]

            silhouette = metrics.silhouette_score(clustering_table.T,
                                                  c_lbs_idx,
                                                  metric=metric)
            avg_word_couv = np.mean(cluster_eval_pos_idx.values)
            med_word_couv = np.median(cluster_eval_pos_idx.values)
            avg_word_couv_minus = (
                np.mean(cluster_eval_pos_idx.values) - np.mean(
                    cluster_eval_neg_idx.values
                )
            )
            labels, counts = np.unique(c_lbs_idx, return_counts=True)
            big_small_ratio = np.max(counts) / np.min(counts)

            result.add_score(f"nb_clust_{idx}", nb_clusters)
            result.add_score(f"silhouette_{idx}", silhouette)
            result.add_score(f"avg_word_couv_{idx}", avg_word_couv)
            result.add_score(f"med_word_couv_{idx}", med_word_couv)
            result.add_score(f"avg_word_couv_minus_{idx}", avg_word_couv_minus)
            result.add_score(f"big_small_ratio_{idx}", big_small_ratio)

            stab_total = 0
            stab_detail = cluster_scores_stab = None

            # TODO consider the case where iter_stab <=0
            if iter_stab > 0:
                (stab_total, stab_detail,
                 cluster_scores_stab) = cls._calculate_stability(
                     nb_clusters, clustering_table, naming_table,
                     natural_space_naming_table, previous_cluster_labels,
                     naming_profile_table, naming_scores, c_scores_idx,
                     iter_stab, remove_stab, random_state
                 )

                c_scores_sort = c_scores_idx.sort_values(ascending=False)
                all_clu = pd.DataFrame({"size": c_scores_sort,
                                        "pos_detail": cluster_eval_pos_idx,
                                        "neg_detail": cluster_eval_neg_idx,
                                        "stability": cluster_scores_stab})
                all_clu = all_clu.sort_values(["size"], ascending=False)
                descclus = all_clu.index.astype(
                    str) + " : s " + all_clu["size"].values.astype(
                        str) + " stb " + all_clu["stability"].map(
                            lambda x: '{0:.2f}'.format(x)
                        ).astype(str) + " + " + all_clu["pos_detail"].map(
                            lambda x: '{0:.2f}'.format(x)
                        ).astype(str) + " - " + all_clu["neg_detail"].map(
                            lambda x: '{0:.2f}'.format(x)
                        ).astype(str)

                result.add_score(f"stab_clus_{idx}", stab_total)

                result.add_desc_score(f"clus_eval_pos_{idx}_det", descclus,
                                      avg_word_couv)

            avg_stab = avg_stab + stab_total
            avg_couv = avg_couv + avg_word_couv

        avg_stab = avg_stab / len(cluster_labels)
        avg_couv = avg_couv / len(cluster_labels)

        result.add_score("avg_stab_avg", avg_stab)
        result.add_score("avg_couv_avg", avg_couv)
        result.add_score("clu_score", (avg_couv + avg_stab)/2)

        return result

    @classmethod
    def _calculate_stability(cls, nb_clusters, clustering_table, naming_table,
                             natural_space_naming_table,
                             previous_cluster_labels, naming_profile_table,
                             naming_scores, c_scores, iter_stab, remove_stab,
                             random_state):
        initial_labels = previous_cluster_labels.copy()
        cluster_scores_stab = pd.Series(dtype=np.float64)
        stab_detail = {}

        for i in c_scores.index.values:
            cluster_scores_stab[i] = 0.0

        nbtot = iter_stab * len(remove_stab)
        lentot = len(c_scores.index.values) * iter_stab

        for samp in remove_stab:
            stab_detail[samp] = 0.0

            for iterstab in range(iter_stab):
                subsamp = clustering_table
                natural_sample = natural_space_naming_table

                if samp > 0:
                    select = np.random.default_rng(
                        seed=random_state
                    ).choice(
                        clustering_table.shape[1],
                        (int)(clustering_table.shape[1] * (1.0 - samp)),
                        replace=False
                    )
                    subsamp = clustering_table[:, select]
                    natural_sample = natural_space_naming_table[select, :]

                (cs_lsa, cs_umap, cs_scores, cs_km,
                 cs_wscores, cscluster_eval_pos,
                 cscluster_eval_neg) = create_kmeans_clusters(
                     nb_clusters, subsamp, naming_table, natural_sample,
                     naming_scores, initial_labels.copy(),
                     naming_profile_table, random_state=random_state)

                names = cs_scores.index.values

                for nn in names:
                    if nn in c_scores.index:
                        cluster_scores_stab[nn] = (cluster_scores_stab[nn]
                                                   + (1 / nbtot))
                        stab_detail[samp] = stab_detail[samp] + (1 / lentot)

        stab_total = np.mean(list(stab_detail.values()))

        return stab_total, stab_detail, cluster_scores_stab


class FinalScore(ScoringBase):

    KEY = "final_score"
    DEFAULTS = {
        "name_list": None
    }

    @classmethod
    def load_scores_from_file(cls, phase, dumps_dir,
                              filename="scores_all.csv"):

        score_name = f"{cls.KEY}"
        score_names = [score_name]

        try:
            # load scores
            result = load_scores_from_file(phase, score_names, dumps_dir,
                                           filename)
            assert result and len(result.scores) == len(score_names)

            # load descriptive scores
            load_desc_scores_from_file(result, phase, score_names,
                                       dumps_dir, load_params=True)
            assert len(result.desc_scores) == len(score_names)
            # assert len(result.run_params) > 0
            # assert len(result.score_params) > 0

        except AssertionError:
            return None

        return result

    @classmethod
    def evaluate(cls, scores, **kwargs):
        logger.debug("Calculating final scores.")

        kwargs = cls.update_defaults(kwargs)
        name_list = kwargs["name_list"]
        print(name_list)
        result = Result()

        if name_list is not None or name_list != []:
            scores = {
                a: scores[a] for a in name_list if a in scores
            }

        scores_df = pd.DataFrame.from_dict(scores, orient="index",
                                           columns=["value"])

        final_score = scores_df["value"].mean()
        # convert dataframe to series
        if len(scores_df) == 1:
            scores = scores_df.squeeze("rows")
        else:
            scores = scores_df.squeeze()

        desc_scores = scores.sort_values(ascending=False).index.astype(
            str) + " : " + scores.sort_values(ascending=False).map(
                lambda x: '{0:.2f}'.format(x)).astype(str)

        score_name = f"{cls.KEY}"

        result.add_score(score_name, final_score)
        result.add_desc_score(f"{score_name}_det", desc_scores, final_score)

        return result
